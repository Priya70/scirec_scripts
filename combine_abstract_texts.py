#! /usr/local/python/
##how to get abstract data from pubmed with all the abstracts for a single pmid merged.
import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
from datetime import datetime 
import h5py

last6mon_q="select pp.pmid,pp.DateCreated,pa.pmid,pa.AbstractText,pa.AbstractOrder from pubmed_pubmed as pp inner join pubmed_abstract as pa where(pp.DateCreated between '2013-12-01' and '2014-05-31') group by pmid"

user_data="

mar=pd.read_csv('mar_2014_data.csv')
uniq_pmids=mar.pmid.unique()
mar_df=mar.drop_duplicates(cols=['pmid'])
pmids_list=list(mar.pmid)
full_abs=[]
for i in uniq_pmids:
        order_cnt=max(mar.AbstractOrder[mar.pmid==i])
        if order_cnt>1:
            tmp_abs=''
            for k in range(1,order_cnt+1):
                tmp_abs= tmp_abs+str(unique(mar.AbstractText[(mar.pmid==i) & (mar.AbstractOrder==k)])[0])
            full_abs.append(tmp_abs)
        else:
            full_abs.append(mar_df.AbstractText[mar_df.pmid==i])
        
        