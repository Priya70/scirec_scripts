###script to get mysql data from our database to get monthly counts..


import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
from datetime import datetime,timedelta 


    '''
    '''
  #  q='select p.pmid, p.DateCreated,p.ArticleTitle, p.JournalTitle,p.ISSN,p.ISSNLinking from pubmed_pubmed as p where (p.DateCreated between %s and %s)'    
    import matplotlib.pyplot as plt
    q='select count(p.pmid),date_format(p.DateCreated,"%Y %m") from pubmed_pubmed as p where p.DateCreated between "2012-01-01" and "2014-09-30" group by date_format(p.DateCreated,"%Y %m")'
    mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
    data=psql.frame_query(q,con=mysql_con)
    date=data.ix[:,1]
    data['new']=data.ix[:,0]       
    locs, labels = xticks()
    labels=[date[i*5] for i in xrange(0,7)]  #there are only seven tick marks on the plot...pull out the dates corresponding to them)        
    labels.append('2014-12')        
    plt.plot(data.new)
    xticks(locs, labels)
    plt.show()   
    
    #### or   
    plot(data.new, 'rs')
    locs, labels = xticks()
    labels=[date[i*5] for i in xrange(0,7)]  #there are only seven tick marks on the plot...pull out the dates corresponding to them)        
    labels.append('2014-12')        

    xticks(locs, labels)
    xlabel("year-month")
    ylabel("# of papers")
    
      