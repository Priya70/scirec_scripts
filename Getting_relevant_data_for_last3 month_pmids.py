#! /usr/local/bin/python
import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
from datetime import datetime 
import h5py
#First need to create LDA for all pmids with recent DateCreated /Pubdate whatever is never null...pulling it out from the database>

mysql_con= MySQLdb.connect(host='localhost', user='scird', passwd='scirdpass', db='scireader') #host ='171.65.77.20' if NOT connecting from the server
may_q='select p.pmid, p.DateCreated,p.ArticleTitle, p.JournalTitle,p.ISSN,p.ISSNLinking ,a.AbstractText, a.AbstractOrder from pubmed_pubmed as p inner join  pubmed_abstract as a on p.pmid = a.pmid where (p.DateCreated between "2014-05-01" and "2014-05-31")'
may_data=psql.frame_query(may_q,mysql_con) 
apr_q ='select p.pmid, p.DateCreated, p.ArticleTitle, p.JournalTitle,p.ISSN,p.ISSNLinking ,a.AbstractText, a.AbstractOrder from pubmed_pubmed as p inner join  pubmed_abstract as a on p.pmid = a.pmid where (p.DateCreated between "2014-04-01" and "2014-04-30")'
apr_data=psql.frame_query(apr_q,mysql_con)
mar_q='select p.pmid, p.DateCreated, p.ArticleTitle, p.JournalTitle,p.ISSN,p.ISSNLinking ,a.AbstractText, a.AbstractOrder from pubmed_pubmed as p inner join  pubmed_abstract as a on p.pmid = a.pmid where (p.DateCreated between "2014-03-01" and "2014-03-31")'
mar_data=psql.frame_query(mar_q,mysql_con)
feb_q='select p.pmid, p.DateCreated, p.ArticleTitle, p.JournalTitle,p.ISSN,p.ISSNLinking ,a.AbstractText, a.AbstractOrder from pubmed_pubmed as p inner join  pubmed_abstract as a on p.pmid = a.pmid where (p.DateCreated between "2014-02-01" and "2014-02-31")'
feb_data=psql.frame_query(feb_q,mysql_con)
apr_data.to_csv('apr_2014_data.csv',header=True)
mar_data.to_csv('mar_2014_data.csv',header=True)
feb_data.to_csv('feb_2014_data.csv',header=True)
may_data.to_csv('may_2014_data.csv',header=True)

#need to concatenate the abstracts of pmids that more than one paragarph -they occur as multiple records
#find duplicates:
feb_dupl=feb_data.set_index('pmid').index.get_duplicates()
    
q='select p.pmid, p.DateCreated,p.ArticleTitle, p.JournalTitle,p.ISSN,p.ISSNLinking ,a.AbstractText, a.AbstractOrder from pubmed_pubmed as p inner join  pubmed_abstract as a on p.pmid = a.pmid where (p.DateCreated between %s and %s)'        
mysql_con= MySQLdb.connect(host='localhost', user='scird', passwd='scirdpass', db='scireader') #host ='171.65.77.20' if NOT connecting from the server
startyear=2013
for month in range(1,13,1):        
    fd=str(startyear)+'-'+str(month)+"-1"
    ld=str(startyear)+'-'+str(month)+"-31"
    data=psql.frame_query(q,params=(fd,ld),con=mysql_con)
    tt=(datetime.strptime(fd,"%Y-%m-%d")).strftime("%b")
    data.to_csv(tt+'_'+str(startyear)+'_data.csv')
    
    
    uniq_pmids=data.pmid.unique()
    data_df=data.drop_duplicates(cols=['pmid'])
    pmids_list=list(data.pmid)
    full_abs=[]
    for i in uniq_pmids:
        order_cnt=max(data.AbstractOrder[data.pmid==i])
        if order_cnt>1:
            tmp_abs=''
            for k in range(1,order_cnt+1):
                tmp_abs= tmp_abs+str(unique(data.AbstractText[(data.pmid==i) & (data.AbstractOrder==k)])[0])
            full_abs.append(tmp_abs)
        else:
            full_abs.append(data_df.AbstractText[data_df.pmid==i])
    pd.to_csv(tt+'_'+str(startyear)+'_data.csv')
     
# Need to find the number of pmids that have more than one para in the abstract as they will be duplicated.
counts = apr_data.groupby('pmid').size()
text_df=pd.read_csv('../make_inferencer_20140221093827/all_20140221093827.txt', sep='\t',header=None,names=['pmids','title','abstract','keywords'])
df=pd.read_csv('../make_inferencer_20140221093827/all_20140221093827_150_9603compostion.txt150_journal.txt', sep='\t',header=None,names=['pmids','title','abstract','keywords'])

#if the  file is too big to read do something like:
#from pandas import *

#tp = read_csv('exp4326.csv', iterator=True, chunksize=1000)
#df = concat(tp, ignore_index=True)
#save LDA files by monthly PubDate 
# Get all relevant data:
#can covert the dates in a datetime series to perform operations on them.
comp_date_data.DateCreated =pd.to_datetime(comp_date_data.DateCreated)





******************
#First need to demonstrate proof of concept for similarity scores>See if that is even meaningful.
#Take jonathan list: -which ones have we performed LDA on?
jp_q="select pa.pmid,pp.DateCreated,pp.Pubdate,pp.JournalTitle,pp.ISSN,pp.ISSNLinking from pubmed_pubmed as pp inner join pubmed_author as pa on pp.pmid=pa.pmid where pa.Lastname='Pritchard' and Forename = 'Jonathan K' "
jp_data=psql.frame_query(jp_q,mysql_con) 
jp_data.to_csv("Pritchard_data.csv")

#feed in the pmid list into scireader LDA download

#put these into scireader and you should get the LDA output format.

#read in abstracts, + compdata
compdata=pd.read_csv('/data/priya/Topic_modelling/mallet-2.0.7/for_database/Calculating_Rec_Scores/all_20140221093827_150_9603compostion.txt',usecols=[0,1,2,3], header=None, skiprows=1, sep='\t', names=['ind','pmid','tpcno1','tpcprob1'])
#which one of JP's papers have we run LDA on?


From Yonggan-

+from scireader.settings import DATABASES
+import MySQLdb as mdb
+from pubmed.views import current_path
+
+def run():
+	con = mdb.Connect(DATABASES['default']['HOST'],DATABASES['default']['USER'],DATABASES['default']['PASSWORD'],DATABASES['default']['NAME'])
+	p = current_path()
+	pubmed = p + "ldaoutput/pubmed.txt"
+	abstract = p + "ldaoutput/abstract.txt"
+	keyword = p + "ldaoutput/keyword.txt"
+	with con:
+		cur = con.cursor()
+		cmd = "SELECT PMID,ArticleTitle INTO OUTFILE '" + pubmed + "' FIELDS TERMINATED BY '\t' LINES TERMINATED BY '\n' FROM  `pubmed_pubmed`"
+		cur.execute(cmd)
+		cmd = "SELECT PMID,AbstractText INTO OUTFILE '" + abstract + "' FIELDS TERMINATED BY '\t' LINES TERMINATED BY '\n' FROM  `pubmed_abstract`"
+		cur.execute(cmd)
+		cmd = "SELECT PMID,Keyword INTO OUTFILE '" + keyword + "' FIELDS TERMINATED BY '\t' LINES TERMINATED BY '\n' FROM  `pubmed_keyword`"
+		cur.execute(cmd)
+		print "done"
