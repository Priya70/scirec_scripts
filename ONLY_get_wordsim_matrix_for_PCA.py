#! /user/bin/python

'''
cronjob to update all libraries for all users daily.
'''
import sys
import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
import os
import glob
import shutil
import numpy as np
import itertools
from bibtexparser.bparser import BibTexParser
import subprocess
import cPickle as pickle

## additional imports from me. 
import argparse
import pubRecords_1 as pr # natalie's module
import natalies_modified_working_Priya_module as cpr # priya's module
#import Data_processing_scripts_WORKING as cpr # priya's module

#make sure update_corpus_TermFreqVec() is run first!!
#first move the old rec file to loaded directory.
out = sys.argv[1]
u_id = sys.argv[2]
lib_publist = [i.strip().split()[0] for i in open(sys.argv[3],'r').readlines()]
dbname = sys.argv[4]
lib_list = sys.argv[5:len(sys.argv)]
date = pr.todays_date()
#o = open(out,'a')

corp_pubfile='/home/ubuntu/lda-data/Current_data/Current_basic_dataset_with_IF_and_LDA.csv'
#out_ind_ct=1
print "hello is anyone here"
for lib_id in lib_list:
    print "updating recs for library ", lib_id 
    libname=lib_id     
    outpath='/home/ubuntu/Personalized_Recs/cronjob_testing/Vocab_testing/'+str(lib_id)
    if not os.path.exists(outpath):
        os.makedirs(outpath)    
    lib,lib_publist =cpr.getting_usrpubRecs_andforLDA([int(i) for i in lib_publist],str(lib_id),outpath)    
    tmp_corp = pr.get_corp_info(corp_pubfile,delim=",")
    corp=[paper for paper in tmp_corp if paper.pmid not in lib_publist]
    #corp = corp + lib
    os.chdir(outpath)
    #cpr.run_lda_user(outpath+"/ForLDA_"+str(lib_id)+".txt",str(lib_id)) # Run topic inferencer
    print "done lda for ", lib_id
    inference_results = outpath+'/'+str(lib_id)+"LDA_composition_for_database.txt" # Identify the file name with the topic assignments
    lib = pr.get_tpcs_lib(inference_results,lib)
    print "starting the word similarities"
    # You have to do the cut, but then just print out the word similarities....
    corp = pr.shared_tpc_cutter_3(lib,corp,frac=0.5) # Fraction can be modified by adding a frac=n line; otherwise, default is 0
    print "done making corpus for the word similarities"
    corp = lib + corp
    simmat,updated_corp_list= cpr.create_updated_termFreqVec_AND_calcSimilarity(pr.pmid_list(corp),outpath+"/ForLDA_"+str(lib_id)+".txt") 
    
    sfile = "wordsim_matrix_library_included.txt"
    s = open(sfile,'w')
    s.write("\t"+"\t".join([str(i) for i in pr.pmid_list(corp)])+"\n")
    for i in range(len(simmat[:,0])):
	to_write = str(lib[i].pmid)
	for j in range(len(simmat[i,:])):
	    to_write += "\t"+str(simmat[i,j])
	s.write(to_write+"\n")
    s.close()
    exit() # no need to actually go further this time - NT

 
