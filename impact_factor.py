#! /usr/local/bin/python
import pandas as pd
import pandas.io.sql as psql
import MySQLdb
from datetime import datetime 

mysql_con= MySQLdb.connect(host='localhost', user='scird', passwd='scirdpass', db='scireader') #host ='171.65.77.20' if NOT connecting from the server
    q = "select * from journals_impactfactor"
    jour_if=psql.frame_query(q,mysql_con)
    jour_if.to_csv('Journal_IF.txt',sep='\t') 
   
    p= "select JournalTitle,ISOAbbreviation from journals_journals"
    jour_name_abbr=psql.frame_query(p,mysql_con)
    jour_name_abbr.to_csv('Journal_names_with_ISOAbbr.txt',sep='\t')
    
    
    jour_if.ISOAbbreviation =jour_if.ISOAbbreviation.str.lower()
    jour_name_abbr.ISOAbbreviation= jour_name_abbr.ISOAbbreviation.str.lower()
    
    new=pd.merge(jour_if,jour_name_abbr,how='inner',on='ISOAbbreviation')
    new_sorted=new.sort('IF', ascending=False)
    new_sorted.to_csv('Journals_common_to_pubmed_and_IFlist.txt',cols=['ISOAbbreviation','JournalTitle'], sep='\t')
    
# To pull out the pmids by journal, better to have  topmost topic probabilities  
compdata=pd.read_csv('all_20140221093827_150_9603compostion.txt',usecols=[0,1,2,3], header=None, skiprows=1, sep='\t', names=['ind','pmid','tpcno1','prob1'])
q= "select pubmed_pubmed.pmid,  pubmed_pubmed.DateCreated, pubmed_pubmed.DateCompleted, pubmed_pubmed.DateRevised, pubmed_pubmed.Pubdate, pubmed_pubmed.JournalTitle,pubmed_pubmed.ISSN,pubmed_pubmed.ISSNLinking" 
   

# Get all the info for each pmid from the join of journals_imapactfactor and pubmed_pubmed table joining on  ISSNLinking=ISSN.

q= "select pubmed_pubmed.pmid, pubmed_pubmed.ISSNLinking, pubmed_pubmed.DateCreated, pubmed_pubmed.DateCompleted, pubmed_pubmed.DateRevised, pubmed_pubmed.Pubdate, journals_impactfactor.IF , pubmed_pubmed.JournalTitle from  pubmed_pubmed join journals_impactfactor where (pubmed_pubmed.ISSNLinking = journals_impactfactor.ISSN or pubmed_pubmed.ISSN = journals_impactfactor.ISSN)"
ifdata=psql.frame_query(q,mysql_con)
ifdata.to_csv('Pmid_IF_date_data.txt',sep='\t') 
 
ifdata.columns=['Ind','pmid','ISSNLinking', 'DateCreated ', 'DateCompleted ', 'DateRevised', 'Pubdate','IF',' JournalTitle', 'ISSN ', 'last']
#cant do a join as need to join on pmid NOT index

ifdata1=pd.read_csv('Pmid_IF_date_data1.txt',sep='\t')
ifdata2=pd.read_csv('Pmid_IF_date_data2.txt',sep='\t')
ifdata==pd.merge(ifdata1,ifdata2,how='outer', left_on='pmid', right_on='pmid',sort=False,left_index=False,right_index=False)

all_data=pd.merge(compdata,ifdata,how='left', left_on='pmid', right_on='pmid',sort=False,left_index=False,right_index=False) #seems faster
all_data.drop(['Ind','ISSNLinking','last'],1,inplace=True)
all_data.to_csv('allpmid_data.csv',sep='\t')

alldata[alldata.IF_y.isnull() & alldata.IF_x.isnull()]
aa=alldata.set_index('pmid').index.get_duplicates()

leftover=set(compdata.pmid)- set(all_data.pmid)
list(leftover) # Turns out these pmids match on ISSN not ISSNLinking
all_data[all_data.pmid.isin(leftover)] 
compdata[compdata.pmid.isin(leftover)]
###turns out every rule has an exception for some journals, eg Plos Computaional Biology and Critical Care London; 
##the ifdata.ISSN number is equal to  pubmeddata ISSN # not ISSNLinking this happens for 253701 docs!

all_data2=pd.merge(ifdata2,compdata,how='inner', left_on='pmid', right_on='pmid',sort=False,left_index=False,right_index=False) #seems faster
leftover=set(compdata.pmid)- set(all_data.pmid)
list(leftover)


all_data.columns=[u'Unnamed', u'Ind', u'pmid', u'ISSNLinking', u'DateCreated', u'DateCompleted', u'DateRevised', u'Pubdate', u'IF', u'JournalTitle', u'ind', u'tpcno1', u'prob1', u'tpcno2', u'prob2', u'tpcno3', u'prob3', u'tpcno4', u'prob4']
all_data.drop(['ind','Unnamed'],1,inplace=True)
aa=list(all_data.ix[0,3:7])  #list of all the dates incl pubdate. date created etc
#find most recent date
tt=max(aa)
tt=datetime.strptime(tt,'%Y-%m-%d')
delta=now.date()-tt.date()
delta.days
bestdate=alldata.apply(g,axis=1)


def g(x):
    return max(x[3:7])


def datefunc1(x):
    tt=max(x[4:8])  #make sure this is correct)
    tt=datetime.strptime(tt,'%Y-%m-%d')
    now=datetime.now()
    delta=now.date()-tt.date()
    if delta.days >60:
       datefctr=(.5)**(delta.days-60.0)  
       return datefctr
    else:
       datefctr=2.0
       return datefctr

def datefunc2(x):
    tt=max(x[4:8])  #make sure this is correct)
    tt=datetime.strptime(tt,'%Y-%m-%d')
    now=datetime.now()
    delta=now.date()-tt.date()
    if delta.days >60:
       datefctr=(.5)**(delta.days-60.0)  
       return datefctr
    else:
       datefctr=2-(delta.days/60.0)  
       return datefctr
       
def datefunc3(x):
    tt=max(x[4:8])  #make sure this is correct)
    tt=datetime.strptime(tt,'%Y-%m-%d')
    now=datetime.now()
    delta=now.date()-tt.date()
    if delta.days >60:
       datefctr=(.5)**(delta.days-60.0)  
       return datefctr
    elif 60.0 >= delta.days >=  15:
       datefctr=2-(delta.days/45.0)  
       return datefctr
    else:
       datefctr=2
       return datefctr
       
               
def funcImF1(x):
    ifac=x[9]
    if ifac > 30:
        imfctr =       
       
       
       
datefctr1=alldata.apply(datefunc1,axis=1)
       

bestdate=alldata.apply(g,axis=1)

datefunc2
datefunc3

iffunc1 
iffunc2
iffunc3



#use combine function to get the intersection of a series.
'''
s1 = pd.Series(data=[1,1,1], index=[1,2,3])
s2 = pd.Series(data=[1,2,2,1], index=[1,2,3,4])
s1.combine(s2, min, 0)
or
pd.DataFrame([s1, s2]).min()
'''

mysql_con.close()