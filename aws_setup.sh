#!/bin/bash
#shell script to upload all new data to s3 and stop the instance
cd
aws s3  sync s3://lda-data  lda-data
# aws s3  sync s3://lda-data  lda-data --delete
# aws s3 sync lda-data/ s3://lda-data --delete
aws s3  sync s3://scirec-scripts Scirec_scripts
export EC2_URL=http://ec2.us-west-1.amazonaws.com