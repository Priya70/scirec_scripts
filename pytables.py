##playing with pytables:
# I want to save all the user's LDA output and sparse vocab file in Pytables.
import pandas as pd
from tables import *
import numpy
class lda(IsDescription):
     pmid      = StringCol(8)    # 16-character String
     tpc1      = Int16Col()      # unsigned 16-bit short integer
     prob1     = Float32Col()    # 32 bit float
     tpc2      = Int16Col()      # unsigned 16-bit short integer
     prob2     = Float32Col()    # 32 bit float
     tpc3      = Int16Col()      # unsigned 16-bit short integer
     prob3     = Float32Col()    # 32 bit float
     tpc4      = Int16Col()      # unsigned 16-bit short integer     tpc3      = Int16Col()      # unsigned 16-bit short integer
     prob4     = Float32Col()    # 32 bit float
     tpc5      = Int16Col()      # unsigned 16-bit short integer
     prob5     = Float32Col()    # 32 bit float
     tpc6      = Int16Col()      # unsigned 16-bit short integer
     prob6     = Float32Col()    # 32 bit float
     tpc7      = Int16Col()      # unsigned 16-bit short integer
     prob7     = Float32Col()    # 32 bit float
     tpc8      = Int16Col()      # unsigned 16-bit short integer
     prob8     = Float32Col()    # 32 bit float
     tpc9      = Int16Col()      # unsigned 16-bit short integer
     prob9     = Float32Col()    # 32 bit float
     tpc10      = Int16Col()      # unsigned 16-bit short integer
     prob10     = Float32Col()    # 32 bit float

h5file = open_file("tutorial1.h5", mode = "w", title = "Test file")
group = h5file.create_group("/", 'lib_id_3', 'data for lib_id 3')
table = h5file.create_table(group, 'lda_data', lda, "top 10 topic prob for papers in lib_id 3")
lda=table.row


#reading the users LDA files into pandas and then into pytables: DONT write stores... they are NOT appendables,queryable or 
#lda_data=pd.read_csv('/home/ubuntu/lda-data/2014/ForLDA_Sep_2014_data.LDA_composition_with_pmid.txt',usecols=range(0,21),sep='\t',header=None,names=['pmid','tpc1','prob1','tpc2','prob2','tpc3','prob3','tpc4','prob4','tpc5','prob5','tpc6','prob6','tpc7','prob7','tpc8','prob8','tpc9','prob9','tpc10','prob10'])# check to make sure of the columns.
#store=pd.HDFStore('data_lib3.h5')
#store['lda_data_lib3'] =lda_data

mkdir Library_data
cd Library_data

store = pd.HDFStore('lib3.h5')
lda_data_3=pd.read_csv("/home/ubuntu/Personalized_Recs/cronjob_testing/Vocab_testing/3/3LDA_composition_with_pmid.txt",usecols=range(0,21),sep='\t',header=None,names=['pmid','tpc1','prob1','tpc2','prob2','tpc3','prob3','tpc4','prob4','tpc5','prob5','tpc6','prob6','tpc7','prob7','tpc8','prob8','tpc9','prob9','tpc10','prob10'])# check to make sure of the columns.
store['lda_data']=lda_data_3
with open("/home/ubuntu/lda-data/Current_data/CurrentTermFreqMatrix",'rb') as infile:
     TermFreqMat = pickle.load(infile)
newTermFreqMat=TermFreqMat.tocsr()[1:10,]
tt=newTermFreqMat.toarray()
termFreq_df=pd.DataFrame(tt)
store["Vocab_vec"]=tt

#lda_data_3.to_hdf("Library","lib3_data",mode="w",format="table",title="lda_data")


##example of the term freq Mat for the papers:
with open("/home/ubuntu/lda-data/Current_data/CurrentTermFreqMatrix",'rb') as infile:
     TermFreqMat = pickle.load(infile)
newTermFreqMat=TermFreqMat.tocsr()[1:10,]
tt=newTermFreqMat.toarray()

f = tables.openFile('Library/lib_3/', 'w')
atom = tables.Atom.from_dtype(tt.dtype)
filters = tables.Filters(complib='blosc', complevel=5)
ds = f.createCArray(f.root, 'Vocab_Vec', atom, tt.shape, filters=filters)
ds[:] = tt
f.close()


h5file = open_file("Library", mode = "r")
h5file

import tables as tb
from numpy import array
from scipy import sparse
'''
def store_sparse_mat(m, name, store='store.h5'):
    msg = "This code only works for csr matrices"
    assert(m.__class__ == sparse.csr.csr_matrix), msg
    with tb.openFile(store,'a') as f:  #keep it as append so you can add more tables/groups to the file
        for par in ('data', 'indices', 'indptr', 'shape'):
            full_name = '%s_%s' % (name, par)
            try:
                n = getattr(f.root, full_name)
                n._f_remove()
            except AttributeError:
                pass
            arr = array(getattr(m, par))
            atom = tb.Atom.from_dtype(arr.dtype)
            filters = tb.Filters(complib='blosc', complevel=9)
            ds = f.createCArray(f.root, full_name, atom, arr.shape)
            ds[:] = arr
'''    

def store_sparse_mat2(m, name, store='store.h5'):
    msg = "This code only works for csr matrices"
    assert(m.__class__ == sparse.csr.csr_matrix), msg
   # with tb.openFile(store,'w') as f:
  #  f=tb.open_file(store,"a")
    f= tb.open_file(store,"a")
    grp=f.create_group(f.root,"TMV_data")
    for par in ('data', 'indices', 'indptr', 'shape'):
        full_name = '%s_%s' % (name, par)
        try:
            n = getattr(f.root, full_name)
            n._f_remove()
        except AttributeError:
            pass
        arr = array(getattr(m, par))
        atom = tb.Atom.from_dtype(arr.dtype)
        filters = tb.Filters(complib='blosc', complevel=9)
    #    ds = f.createCArray(grp, full_name, atom, arr.shape)
        ds = f.create_carray(grp, full_name, atom, arr.shape)
        ds[:] = arr
    f.close()


def load_sparse_mat2(name, store='store.h5'):
    with tb.open_file(store,"a") as f:
        pars = []
        for par in ('data', 'indices', 'indptr', 'shape'):
            pars.append(getattr(f.root.TMV_data, '%s_%s' % (name, par)).read())
    m = sparse.csr_matrix(tuple(pars[:3]), shape=pars[3])
    return m





#This works:
tt=pd.read_hdf("lib3.h5","lda_data")


store_sparse_mat2(TermFreqMat,"TFM",store="test.h5")
testTFM=load_sparse_mat2("TFM",store="test.h5")
aa=cpr.load_sparse_mat2("TFM",store="lib_1.h5")

bb= cpr.store_sparse_mat2(aa,"TFM",store="test.h5") wont work....

to only remove  a single node or group: do
f.root.TMV_data._f_remove()

overwrite_oldstore_sparse_mat2(m, name, store='A.h5')

lda_data_1=pd.read_csv("/home/ubuntu/Personalized_Recs/cronjob_testing/Vocab_testing/1/1LDA_composition_with_pmid.txt",usecols=range(0,21),sep='\t',header=None,names=['pmid','tpc1','prob1','tpc2','prob2','tpc3','prob3','tpc4','prob4','tpc5','prob5','tpc6','prob6','tpc7','prob7','tpc8','prob8','tpc9','prob9','tpc10','prob10'])# check to make sure of the columns.
lda_data_1.to_hdf("test.h5","lib1_lda_data",mode="a",format="table",compress="blosc")

----
tmpUniq.to_hdf("test.h5","lib"+str(lib)+"_uniqAll",mode="a",append=True,format="table",compress="blosc",min_itemsize={ "values_block_1": 5000 } )
