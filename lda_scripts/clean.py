#! /usr/local/bin/python
#takes in the input files and spits out a file with all unicode characters removed.
#usage : python clean.py input.txt >> input_for_inferencer.txt

import codecs
import unicodedata
import sys


def main(args):
    input=args[1] 
 #  infile = codecs.open(input, encoding='utf-8')
    infile=open(input).readlines()
    for line in infile:  
        #aa=unicodedata.normalize('NFKD', line).encode('ascii', 'ignore')
        #  print unidecode.unidecode(line)     
        print ''.join(unicode(line,errors='ignore').splitlines())
           
if __name__ == "__main__":
    main(sys.argv) 