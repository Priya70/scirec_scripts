#!/usr/bin/env python
### Composite pipeline for regular recommendation updates. 
## pipeline edited for TEN FOLD VALIDATION scheme, edits marked '10'
##################
##### Imports
##################

## this import section lifted directly from Priya's code -- unsure of what dependencies her functions have so I am taking it all just in case 
'''import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
from datetime import datetime,timedelta 
#import h5py
import subprocess
import os
from dateutil import rrule
import glob
import shutil
import numpy as np
import itertools
from bibtexparser.bparser import BibTexParser'''
import numpy as np
import os
## additional imports from me. 
import argparse
import pubRecordsProduction_WORKING as pr # my module
#import natalie_modified_Priya_module as cpr # Priya's module with my edits
import natalies_modified_working_Priya_module as cpr
#for the 10fold
import random
##################
##### Argument processing and set-up
##################

#### Argument parsing
parser = argparse.ArgumentParser() ## Create an argument parser

# Arguments...
parser.add_argument('--cf', help = "corpus info file",default="/home/ubuntu/lda-data/Current/Current_basic_dataset_with_IF_and_LDA.csv") # config file
parser.add_argument('--u', help = "user ID",default="1") # user ID
parser.add_argument('--l', help = "library ID",default="3") # library ID
parser.add_argument('--pf', help = "PMID file list") # a file with PMIDs in it
# Now parse everything - these lines are required to create a list of args, l_args, to read
args = parser.parse_args()
l_args = vars(args) # Dictionary with argument names -> values 
arglist = l_args.values() # Values of all the arguments, in order
# Assign to variables
config = l_args["cf"] #config file
## If not using the config file, other args -- these args appear in the config file in this exact order
u_id = l_args["u"] #user ID
lib_id = l_args["l"] #library ID
lib_pubfile = l_args["pf"] #PMID list
##################
#####  MAIN  #####
##################

#########
##### File processing 
#########
 
## 1: Assembling complete record information for each of the papers in the library, not including topics
lib_publist = [i.strip() for i in open(lib_pubfile,'r').readlines()]
#outpath='/home/ubuntu/Personalized_Recs/cronjob_testing/Vocab_testing/'+str(lib_id)
outpath='/home/ubuntu/Personalized_Recs/cronjob_testing/Vocab_testing/'+str(lib_id)
lib,lib_publist =  cpr.getting_usrpubRecs_andforLDA([int(i) for i in lib_publist],str(lib_id),outpath)    
os.chdir(outpath)
cpr.run_lda_user(outpath+"/ForLDA_"+str(lib_id)+".txt",str(lib_id)) # Run topic inferencer
