#!/usr/local/bin/python
#using this script to access the scireader database in  python and pull out all the journal names for the pmids I have.
#using this in the server directory:data/priya/Topic_modelling/mallet-2.0.7/for_database/make_inferencer_20140221093827

import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
from sklearn.metrics import pairwise_distances
from scipy.spatial.distance import cosine
from hcluster import pdist, linkage, complete, dendrogram
from scipy.linalg import norm
from scipy.spatial.distance import euclidean
import scipy.cluster.hierarchy
import scipy

data=pd.read_table('all_20140221093827_150_9603compostion.txt',header=None,skiprows=1) #important to skip 1 row to avoid the doc prop topic line and header=None so you can call a data column by column # 
mysql_con= MySQLdb.connect(host='localhost', user='scird', passwd='scirdpass', db='scireader') #host ='171.65.77.20' if NOT connecting from the server

# q="""select pmid,JournalTitle from pubmed_pubmed where pmid>24566100   """
# df3=psql.frame_query(q,mysql_con)
    q = "select pmid, JournalTitle from pubmed_pubmed"
    j_name=psql.frame_query(q,mysql_con)
    j_name.to_csv('Jour_names.txt', sep='\t')

data=open('all_20140221093827_150_9603compostion.txt').readlines()
out=open('all_20140221093827_150_9603compostion_with_journals.txt', 'w')

for line in data:
    pmid=line.split()[1]
    # print pmid
    if pmid in j_name.pmid.values:
         ind=where(j_name.pmid.values == pmid)
         out.write(line+'\t' +'\t'+j_name.JournalTitle.values[ind[0][0]])
    
## creating tpc_vectors from  the wr_tpc file;
##First separate wrd_file into wrds > 15 occurances and << 15occurances  
n_tpcs=150    
wrd_file=open('wrd_tpcall_20140221093827_150_9603.txt').readlines()
discard=open('Word_wLess_than_15_occ.txt','w')
keep=open('Words_wMore_than_15_occ.txt','w')
tpcs=[[0 for y in xrange(len(wrd_file))] for x in xrange(n_tpcs)]
wrd_vocab=[line.split()[1] for line in wrd_file]

ct=0
discard_ind=[]
for txt in wrd_file:
     aa=txt.split()[2:]
     print aa
     wrd_ct=0
     for i in xrange(len(aa)):
        tpcs[int(aa[i].split(':')[0])][ct]=int(aa[i].split(':')[1]) 
        wrd_ct=wrd_ct+int(aa[i].split(':')[1])
        print wrd_ct
     if wrd_ct>15:
        keep.write(txt.split()[1]+'\t'+str(wrd_ct)+'\n')     
     else:
        discard.write(txt.split()[1]+'\t'+str(wrd_ct)+'\n') 
        discard_ind.append(ct)   
     ct =ct+1
     print ct
    
keep.close()
discard.close()    


tpcs_array=np.array(tpcs)
tpcs_norm= [[] for x in xrange(n_tpcs)] 
for i in range(n_tpcs):
    tp_sum=sum([float(kk) for kk in tpcs[i]])
    tpcs_norm[i]=[(float(kk)/tp_sum) for kk in tpcs[i]]
tpcs_norm_array=np.array(tpcs_norm)    
#need to discard all the entries for words whose count <15
new_tpcs_arr=np.delete(tpcs_array,discard_ind,1)


def tf(tpc_vec,n_tpcs):
    tf=[[] for x in xrange(n_tpcs)]
    for i in range(n_tpcs):        
        tf[i]=tpc_vec[i]/float(sum(tpc_vec[i]))  
    idf=np.zeros(len(tpc_vec[1]))
    tf=np.asarray(tf)
    for i in xrange(len(tpc_vec[1])):        
        idf[i]=math.log10( float(n_tpcs)/sum(1 for a in tpc_vec[:,i] if a>0) )
    tf_idf=multiply(tf,idf) ##or tf*idf        
    return tf_idf
    
def create_CosineSim_matrix(tpcs_norm_arr):
    from sklearn.metrics import pairwise_distances
    from scipy.spatial.distance import cosine
    #shape(tpcs_norm_array)
    dist_out = 1- pairwise_distances(tpcs_norm_arr, metric="cosine")  
   # dist_out = pairwise_distances(tpcs_norm_arr, metric="cosine")
    return dist_out    
    
import scipy.cluster.hierarchy
from scipy.cluster.hierarchy import linkage, dendrogram  
Z=scipy.cluster.hierarchy.linkage(dist_tf_idf_cos,method='average', metric='euclidean')
dendrogram(Z,leaf_font_size=8,leaf_rotation=90)
       # dendrogram(linkage(distanceMatrix, method='complete')
      
      

tf_idf=tf(new_tpcs_arr,n_tpcs)    
tf_idf2=tf(tpcs_array,n_tpcs)
dist_tf_idf_cos=create_CosineSim_matrix(tf_idf)
dist_tf_idf_cos_long=create_CosineSim_matrix(tf_idf2)
dist_cos_long=create_CosineSim_matrix(tpcs_array)
np.savetxt('dist_tf_idf_cosine.csv',dist_tf_idf_cos, delimiter=',') 
Z=scipy.cluster.hierarchy.linkage(dist_tf_idf_cos,method='average', metric='euclidean')
dendrogram(Z,leaf_font_size=8,leaf_rotation=90)


_SQRT2 = np.sqrt(2)     # sqrt(2) with default precision np.float64 

def hellinger1(p, q):
    return norm(np.sqrt(p) - np.sqrt(q)) / _SQRT2
 
def hellinger2(p, q):
    return euclidean(np.sqrt(p), np.sqrt(q)) / _SQRT2
 
def hellinger3(p, q):
    return np.sqrt(np.sum((np.sqrt(p) - np.sqrt(q)) ** 2)) / _SQRT2

def create_hell_matrix(tpcs_norm_array,n_tpcs):
    _SQRT2 = np.sqrt(2)  
    hell_arr=np.zeros((n_tpcs,n_tpcs))    
    for i in range(n_tpcs):
        for j in range(n_tpcs):
            hell_arr[i,j]=hellinger3(tpcs_norm_array[i],tpcs_norm_array[j])
    return hell_arr

hell_dist=create_hell_matrix(tpcs_norm_array,n_tpcs)

Z=scipy.cluster.hierarchy.linkage(hell_dist,method='average', metric='euclidean')
dendrogram(Z,leaf_font_size=8,leaf_rotation=90)

     
