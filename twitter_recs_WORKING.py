'''
cronjob to update all libraries for all users daily.
'''
import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
import os
import glob
import shutil
import numpy as np
import itertools
from bibtexparser.bparser import BibTexParser
import subprocess
import cPickle as pickle
import immediateRecs as ir
## additional imports from me. 
import argparse
import pubRecordsProduction as pr # natalie's module
import Data_processing_scripts as cpr # priya's module
import random
import logging
date = pr.todays_date()
logging.basicConfig(filename='/home/ubuntu/Logging_info/'+str(date)+'twitter_recs.log',level=logging.DEBUG, filemode='w')


if glob.glob('/home/ubuntu/Personalized_Recs/cronjob_testing/new/*'):
    shutil.copy2(glob.glob('/home/ubuntu/Personalized_Recs/cronjob_testing/new/*')[0],'/home/ubuntu/Personalized_Recs/cronjob_testing/loaded/')
    os.remove(glob.glob('/home/ubuntu/Personalized_Recs/cronjob_testing/new/*')[0])


### Know which index you're outputting at...
out_ind_ct=0

topicfile = "/home/ubuntu/lda-data/Current_data/Current_basic_dataset_with_IF_and_LDA.csv"
# Grab the existing papers from twitter
##### 1. Query for the existing papers.
q = "SELECT dbid,count FROM twitter_tweetspapers;"
mysql_con = MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
c=mysql_con.cursor()
c.execute(q)
# Obtain your queried information
tweetpaps = c.fetchall()
tweetpaps = np.asarray(tweetpaps) # who doesn't love NParrays

# Sort it by score into a hashmap
gen_tweets_list = [] # remember, you'll need to output a general tweets list
tweet_scores = dict() # make a map of the scores to papers....
for i in range(len(tweetpaps[:,0])):
	# Append to the general list...
	gen_tweets_list.append(pr.pubRecords(p=int(tweetpaps[i,0])))
        try: # Try to make the map, but if you fail, just 
                tweet_scores[tweetpaps[i,1]].append(pr.pubRecords(p=int(tweetpaps[i,0])))
        except: # start the list
                tweet_scores[tweetpaps[i,1]] = [pr.pubRecords(p=int(tweetpaps[i,0]))]

# Then get all of the topics of interest
q = "SELECT userid_id,subcatid_id FROM feeds_mytopics;" # Select all of the topics
mysql_con = MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
c=mysql_con.cursor()
c.execute(q)
# Obtain your queried information
topicdata = c.fetchall()
topicdata = np.asarray(topicdata) # who doesn't love NParrays

##### 2. Find the top tweeted papers for these topics.
# For each user, identify which topics are relevant, and get papers for those.
user_topics = dict() # This dictionary will hold user topics and papers with those topics
for i in range(len(topicdata[:,0])):
        # Try to add these papers into a dictionary that already exists
        try:
                user_topics[topicdata[i,0]].values() # is there a dictionary here?
                # If so, go ahead and try to update it. 
                try:
                        user_topics[topicdata[i,0]][topicdata[i,1]].append(ir.find_papers_with_topic_dateweight(topicfile,[topicdata[i,1]-1]))
                # If that doesn't work out, then start from scratch:
                except:
                        user_topics[topicdata[i,0]][topicdata[i,1]] = (ir.find_papers_with_topic_dateweight(topicfile,[topicdata[i,1]-1]))
        # If there isn't a dictionary in the first place, create it and add
        except:
                user_topics[topicdata[i,0]] = dict()
                user_topics[topicdata[i,0]][topicdata[i,1]] = (ir.find_papers_with_topic_dateweight(topicfile,[topicdata[i,1]-1]))

# Now, for each paper, calculate the top-tweeted list of all the papers, and mix them in at the top. 
# Remember to output a general list....
pr.output_rec_by_lib(gen_tweets_list,libname="-2",dbname="pubmed",user=str("NULL"),outf="/home/ubuntu/Personalized_Recs/cronjob_testing/new/"+str(pr.todays_date())+"_twitter_recs.txt",index=out_ind_ct)
for n,user in enumerate(user_topics.keys()):
    try:
        # Get the top tweeted papers from those topics
        # prepare a list of paper objects...
        papers = []
        for lib in user_topics[user].values():
                for p in lib:
                        papers.append(p)
        # Re-weight the papers, but only with the tweet scores (use = 1,0,0)
        scores =  pr.metadata_weighter(papers,[1 for i in range(len(papers))],dbgvar=0,use = [1,0,0])
        # Choose the first papers
        recs = pr.choose_best_from_corp(papers,scores,reclen=500)
        # Mix them in preferentially at the top -
        
        best_tweeted = random.sample(recs[0:20],10) # get 10 of the best 20 for the top
   #     less_tweeted = random.sample(recs[20:200],90) # grab 90 of the next best 180 for the top
        less_tweeted = random.sample(recs[20:200],90)
        # Choose the top papers to mix in...
        scores_list = tweet_scores.keys()
        scores_list.sort()
        scores_list.reverse()

        # Now, choose from the best thereof to populate the rest...
        best_corp = []
        less_corp = []

        # Make sure and choose nicely matched sets.
        index = 0
        while len(best_corp) < 10:
                for p in tweet_scores[scores_list[index]]:
                        best_corp.append(p)
                index += 1
        index = 0
        # Now add the less tweeted papers in there.
        while len(less_corp) < 90:
                for p in tweet_scores[scores_list[index]]:
                        less_corp.append(p)
                index+=1
        # Now create two shuffled lists.
        best_list = best_corp + best_tweeted
        less_list = less_corp + less_tweeted
        random.shuffle(best_list)
        random.shuffle(less_list)
        final_list = best_list + less_list
        # Make sure your paper sets are unique, and create your final set...
        tweet_mix = []
        final_set = set(final_list)

        # For every paper, add it to the tweet mix if it's unique; if not, choose another paper to put in its stead...
        for p in final_list:
                attempt_counter = 0 # Never get stuck in a loop - maybe you're all tried out
                while p in tweet_mix and attempt_counter <= 10:
                        p = random.sample(recs,1)[0]
                        attempt_counter += 1
                tweet_mix.append(p)
        # Write it out into the rec file. 
        pr.output_rec_by_lib(tweet_mix[0:60],libname="-2",dbname="pubmed",user=str(int(user)),outf="/home/ubuntu/Personalized_Recs/cronjob_testing/new/"+str(pr.todays_date())+"_twitter_recs.txt",index=out_ind_ct)
		#Update the output counter
	    #print out_ind_ct
    	out_ind_ct += len(final_list)
    except:
        use_gen_tweets_list=random.sample(recs[0:60],60)
        pr.output_rec_by_lib(use_gen_tweets_list[0:60],libname="-2",dbname="pubmed",user=str(int(user)),outf="/home/ubuntu/Personalized_Recs/cronjob_testing/new/"+str(pr.todays_date())+"_twitter_recs.txt",index=out_ind_ct)
        out_ind_ct += len(gen_tweets_list[0:60])
        #continue	
