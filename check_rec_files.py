#/usr/bin/python

import pubRecordsProduction  as pr # natalie's module
import Data_processing_scripts as cpr # priya's module
import logging
date = pr.todays_date()
logging.basicConfig(filename='/home/ubuntu/Logging_info/'+str(date)+'personal_recs.log',level=logging.DEBUG, filemode='w')
import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
import os
import glob 
 
 #first read in the library_library data to figure out how many unique users and libraries there are:
mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
q='select * from library_library_papers'
data=psql.frame_query(q,con=mysql_con)
lib_list=list(pd.unique(data.libid_id))
usr_list=list(pd.unique(data.userid_id))
usrs_cnt=len(usr_list)
lib_cnt=len(lib_list)
##read in the newly made recommendation  file and see if anything is missing. 
out='/home/ubuntu/Personalized_Recs/cronjob_testing/new/'
os.chdir(out)
curr_file=glob.glob('*twitter*')[0] 

cur_recs=pd.read_csv(curr_file,header=None,sep='\t')

expected_twit_data=(usrs_cnt+1)*246

twit_data=cur_recs[cur_recs[2]==-2]
curr_twit_data_len=len(twit_data[0])

##which users are missing in the twitter data? 
twit_usr_list=unique(list(twit_data[1]))

