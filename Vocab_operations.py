#! /usr/bin/python
'''
script to perform the cron operations to create vocabulary from the entire corpus of last 6 months
'''
import Data_processing_scripts_WORKING as cpr
import time
startTime = time.time()
cpr.create_vocab_for_corpus()
print "Runtime:",time.time()-startTime