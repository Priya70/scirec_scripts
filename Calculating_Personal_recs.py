#! /usr/local/bin/python
import pandas as pd
import numpy as np
from datetime import datetime,timedelta
import Data_processing_scripts_WORKING as dp
import pubRecords_1 as pr 
import os  
import sys 
import glob
import shutil
from dateutil import rrule

##Easiest Case 
###Using Priya's Filter 1, word-sim: output 50 most similar papers  
dp.create_basic_papers_dataset_for_tpcRec()   #done as part of Update  tpc recs
dp.update_new_papers()   #done as part of Update  tpc recs
dp.create_basic_papers_dataset_with_AllTpcProb()
dp.create_basic_papers_dataset_for_WordSim()
dp.get_users_papers() #get 2 files: (1)the users papers with tpc_prob, (2) abstract/titles text

#For this example: 
#    jonathan's papers with tpc probs: /home/ubuntu/Calculating_Rec_Scores/JP_test_set/JP_papers_LDA_prob.txt
#    jonathan's papers with abstract/title text:  /home/ubuntu/Calculating_Rec_Scores/JP_test_set/pritchard_abstrcts_cleaned.txt

short_list_of_Topically_sim_papers,user_paper_pmids=dp.filter_topically_similar_papers('/home/ubuntu/Calculating_Rec_Scores/JP_test_set/JP_papers_LDA_prob.txt','/home/ubuntu/lda-data/Current_data/Current_AllTpcProb_data.csv')

Eilon1:  short_list_of_Topically_sim_papers,user_paper_pmids=dp.filter_topically_similar_papers('/home/ubuntu/Personalized_Recs/Eilon_noiseLDA_composition_for_database.txt', '/home/ubuntu/lda-data/Current_data/Current_AllTpcProb_data.csv')
dp.vocab_list_Similarity_calculator(short_list_of_Topically_sim_papers,'Uniq_Eilon_noise.txt','Eilon_noise') 
user_file='Uniq_Eilon_noise.txt'
user_name='Eilon_noise'

Eilon_syn:   short_list_of_Topically_sim_papers,user_paper_pmids=dp.filter_topically_similar_papers('/home/ubuntu/Personalized_Recs/Eilon_synPromoterLDA_composition_for_database.txt', '/home/ubuntu/lda-data/Current_data/Current_AllTpcProb_data.csv')
dp.vocab_list_Similarity_calculator(short_list_of_Topically_sim_papers,'Uniq_Eilon_synPromoter.txt','Eilon_synPromoter') 
user_file='Uniq_Eilon_synPromoter.txt
user_name='Eilon_synPromoter'

Anil:   short_list_of_Topically_sim_papers,user_paper_pmids=dp.filter_topically_similar_papers('/home/ubuntu/Personalized_Recs/Anil_file_tpcs_composition_for_database.txt', '/home/ubuntu/lda-data/Current_data/Current_AllTpcProb_data.csv')
dp.vocab_list_Similarity_calculator(short_list_of_Topically_sim_papers,'Uniq_Anil.txt','Anil') 
user_file='Uniq_Anil.txt'
user_name='Anil'




##Case 2: Using Nat's Filter 1 (ie Filtering by top 3 topics)
dp.create_basic_papers_dataset_for_tpcRec()    #done as part of Update  tpc recs
dp.update_new_papers()     #done as part of Update  tpc recs
user_pmids=dp.getting_users_pmids(user_bibtex)
getting_users_papers_for_lda(pmids,user_name)
run_lda_user(file,user_name)
''' #need to create a list of pubRecords of the users papers. 
 ##eg read in 'Uniq_Eilon_synPromoter.txt' into a list of pubRecords where each pubRecord is a class containing all the info of that paper.
user_txt=open('Uniq_Eilon_synPromoter.txt','r').readlines()
lib=[]
for line in user_txt[1:]:
     ll=line.split('\t')
     paper=pr.pubRecords()
     paper.pmid=ll[0]
     paper.date=ll[1]
     paper.title=ll[2]
     paper.issn=ll[3]
     paper.issnl=ll[4]
     paper.abstract=ll[5] 
     lib.append(paper)
'''
##do this for the topic probs list:
user_tpc_txt=open('Eilon_synPromoterLDA_composition_for_database.txt').readlines()
lib=[]
for line in user_tpc_txt:
    ll=line.split('\t')
    paper=pr.pubRecords()
    paper.pmid=ll[0]       
    for i in range(150):
		#print i
		paper.tpcs[i] = ll[i+1].rstrip('\n')
		lib.append(paper)
###################

current_papers=open('/home/ubuntu/lda-data/Current_data/Current_basic_dataset_with_IF_and_LDA.csv').readlines()

corpus=[]
for line in current_papers[1:]:
    ll=line.split(',')
    #print ll
    paper=pr.pubRecords(p = ll[0], tp = [ll[1],ll[3],ll[5]],p_t=[ll[2],ll[4],ll[6]])
    corpus.append(paper)    
'''         
    for i in range(150):
		#print i
		paper.tpcs[i] = ll[i+1].rstrip('\n')
		corpus.append(paper)
'''
##
filt1_publist=pr.shared_tpc_cutter_3(lib,corpus,0.9)
short_list1=pr.pmid_list(filt1_publist)


