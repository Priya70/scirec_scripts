!#/usr/bin/local/python
import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
df=pd.read_csv('/data/priya/Topic_modelling/mallet-2.0.7/for_database/make_inferencer_20140221093827/all_20140221093827_150_9603_fordatabase.txt', sep='\t',header=None)
jp_tpcs=pd.read_csv('/data/priya/Topic_modelling/mallet-2.0.7/for_database/Calculating_Rec_Scores/JP_test_set/JP_papers_LDA_prob.txt', sep='\t',header=None)
jp_tpcs.drop_duplicates(cols=[0],take_last=True, inplace=True)
Nat_set=pd.read_csv('/home/for_file_sharing/top_pmids_3k+_geq0.8.txt')
df['pmid']=df[0]
Nat_set.columns=['pmid']
Nat_df=pd.merge(Nat_set,df,how='left',left_on='pmid',right_on='pmid',sort=False)
New_df=pd.concat([Nat_df,jp_tpcs],join='outer',axis=0,ignore_index=True)
New_df.drop_duplicates(cols=['pmid'],take_last=True,inplace=True)
New_df_pmid=New_df['pmid']
New_df.drop(['pmid'],axis=1,inplace=True)
New_df_arr=np.array(New_df)

from sklearn.metrics import pairwise_distances
from scipy.spatial.distance import cosine
from hcluster import pdist, linkage, complete, dendrogram
from scipy.linalg import norm
from scipy.spatial.distance import *
import scipy.cluster.hierarchy
import scipy
Nat_cos_dist=pairwise_distances(New_df_arr,New_df_arr, metric="cosine")
Nat_Euc_dist=pairwise_distances(New_df_arr,New_df_arr, metric="euclidean")

numpy.savetxt('Nat_Cos_dist.txt',Nat_cos_dist,fmt='%10.5f',delimiter='\t')
numpy.savetxt('Nat_Euc_dist.txt',Nat_Euc_dist,fmt='%10.5f',delimiter='\t')
