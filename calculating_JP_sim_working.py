#! /usr/local/bin/python

# have the abstracts for JP_papers. Need to run LDA on them.

import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
from datetime import datetime 
import h5py
import mysql.connector


JPdata_q='select pp.pmid,pp.DateCreated,pp.ArticleTitle, pp.JournalTitle,pp.ISSN,pp.ISSNLinking,pab.AbstractText,pab.AbstractOrder from pubmed_pubmed as pp inner join pubmed_author as pa on pp.pmid=pa.pmid inner join pubmed_abstract as pab on pa.pmid=pab.pmid where (pa.Lastname="Pritchard" and pa.Forename = "Jonathan K") group by pp.pmid'
mysql_con= MySQLdb.connect(host='localhost', user='scird', passwd='scirdpass', db='scireader') #host ='171.65.77.20' if NOT connecting from the server
mysql_con= MySQLdb.connect(host='aa1aez2w6zqz5dy.ckyekccfx4xz.us-west-1.rds.amazonaws.com',passwd='&s83B7J3JQhSNKJ',port=3306,user='scireader',db='scireader')

JP_data=psql.frame_query(JPdata_q,mysql_con) #This data contains everything that we need: date created, title, journal ISSN no, 
JP_txt=JP_data

df=pd.read_csv('/data/priya/Topic_modelling/mallet-2.0.7/for_database/make_inferencer_20140221093827/all_20140221093827_150_9603_fordatabase.txt', sep='\t',header=None)
last6mon_q='select p.pmid, p.DateCreated, p.DateCompleted, p.DateRevised, p.Pubdate, p.JournalTitle,p.ISSN,p.ISSNLinking from pubmed_pubmed as p where (p.DateCreated between "2013-12-01" and "2014-05-31")'
last6mon_data=psql.frame_query(last6mon_q,mysql_con) 
last6mon_data['pmid']=last6mon_data['pmid'].apply(int)
last6mon_data.drop_duplicates(cols='pmid',inplace=True)
#This is the dataset of all pmids from last 6 months that I have topic probabilities for. This is my "set" from which I will find most similar papers.
df['pmid']=df[0]
test_data_pmid=pd.DataFrame(last6mon_data['pmid'])
test_data_tpcs=pd.merge(test_data_pmid,df,on='pmid',sort=False)
test_data_tpcs.drop(['pmid'],axis=1, inplace =True)
test_data_tpcs.to_csv('mydata_tpc.csv')
last6mon_textData_p= "select pp.pmid,pp.DateCreated,pa.pmid,pa.AbstractText,pa.AbstractOrder from pubmed_pubmed as pp inner join pubmed_abstract as pa on pp.pmid=pa.pmid where(pp.DateCreated between '2013-12-01' and '2014-05-31')"
last6mon_textData=psql.frame_query(last6mon_textData_q,mysql_con) 

text_df=pd.read_csv('/data/priya/Topic_modelling/mallet-2.0.7/for_database/make_inferencer_20140221093827/all_20140221093827.txt', sep='\t',header=None,names=['pmid','title','abstract','keywords'])
test_data_text=pd.merge(test_data_pmid,text_df,on='pmid',sort=False)
test_data_text.to_csv('mydata_text.csv')
###for some reason therebffffffffffff are 2 more pmids in the list for tpcs than for abstarcts {24056711, 24377402} figure out why


jp_tpcs=pd.read_csv('/data/priya/Topic_modelling/mallet-2.0.7/for_database/Calculating_Rec_Scores/JP_test_set/JP_papers_LDA_prob.txt', sep='\t',header=None)
#to drop the duplicates: do.  
jp_tpcs.drop_duplicates(cols=[0],take_last=True, inplace=True)
jp_tpcs_pmid=jp_tpcs[0]
jp_pmid_list=list(jp_tpcs_pmid)
jp_q='select p.pmid, p.DateCreated,p.ArticleTitle, p.JournalTitle,p.ISSN,p.ISSNLinking ,a.AbstractText, a.AbstractOrder from pubmed_pubmed as p inner join  pubmed_abstract as a on p.pmid = a.pmid where p.pmid in ('+','.join(("%s",)*len(jp_pmid_list))+")" '
cur=mysql_con.cursor()
cur.execute(jp_q,tuple(jp_pmid_list))
result=cur.fetchall()    
jp_df=pd.DataFrame(result) #note that this df has all the paragraphs of the abstarcts. Need to run MysqlData_to_OneRecPerLineFmt(filename) on it. Have cut and pasted that here
jp_df.columns=['pmid', 'DateCreated','ArticleTitle', 'JournalTitle','ISSN','ISSNLinking' ,'AbstractText', 'AbstractOrder'] 
uniq_pmids=pd.unique(jp_df.pmid)
data_df=jp_df.drop_duplicates(cols=['pmid'])
pmids_list=list(jp_df.pmid)
full_abs=[]
for i in uniq_pmids:
    order_cnt=max(jp_df.AbstractOrder[jp_df.pmid==i])
    if order_cnt>1:
     #  print "step1"  
        tmp_abs=''
        for k in range(1,order_cnt+1):
            tmp_abs= tmp_abs+str(pd.unique(jp_df.AbstractText[(jp_df.pmid==i) & (jp_df.AbstractOrder==k)])[0])   
        full_abs.append(tmp_abs)                    
    else:
          #  print "step2"
        full_abs.append(data_df.AbstractText[data_df.pmid==i].values[0])
data_df.drop(['AbstractText'],axis=1, inplace=True)  
data_df.drop(['AbstractOrder'],axis=1,inplace=True)
data_df['AbsText']=full_abs
data_df.to_csv('JP_allData.csv',sep='\t',index=False) 


test_data_txt=pd.read_csv('/data/priya/Topic_modelling/mallet-2.0.7/for_database/Calculating_Rec_Scores/JP_test_set/mydata_text.csv', header=None)
test_data_tpcs=pd.read_csv('/data/priya/Topic_modelling/mallet-2.0.7/for_database/Calculating_Rec_Scores/JP_test_set/mydata_tpc.csv', header=None)
test_data_tpcs.drop([0],axis=1,inplace=True) #drop 1st row and 1st column
test_data_tpcs.drop([0],inplace=True)
# drop the pmids that are in tpcs file and not in text file.{24056711, 24377402}
##make sure that you figure out which one is larger and then subtarct!!!
del_list=list(set(test_data_tpcs[0])-set(test_data_text['pmid']))
del_list=[24056711, 24377402]
for i in del_list:
     test_data_tpcs=test_data_tpcs[test_data_tpcs != i]

test_data_tpcs_pmid=test_data_tpcs[1]
jp_tpcs_pmid=jp_tpcs[0]
test_data_tpcs.drop([1],axis=1,inplace=True)
jp_tpcs.drop([0],axis=1,inplace=True)

tpcs_data_array=np.array(test_data_tpcs)
jp_tpcs_data_array=np.array(jp_tpcs)


sample_data=pd.read_csv('/data/priya/LDA_data/2014/ForLDA_Mar_2014_data.LDA_composition.txt',sep='\t',header=False)
tmp1=pd.read_csv('/data/priya/LDA_data/2014/ForLDA_Apr_2014_data.LDA_composition.txt',sep='\t',header=False)
tmp2=pd.read_csv('/data/priya/LDA_data/2014/ForLDA_May_2014_data.LDA_composition.txt',sep='\t',header=False)
np_sample=np.array(sample_data)
np_tmp1=np.array(tmp1)
np_tmp2=np.array(tmp2)
sample=pd.DataFrame(np.concatenate((np_sample,np_tmp1,np_tmp2),axis=0))
sample_pmids=sample[0]
sample.drop([0],axis=1,inplace=True)
sample_arr=np.array(sample)
##Calculate  papers topically similar to JP_lib -

#test_arr=np.concatenate(tpcs_data_array,jp_tpcs_data_array)
#test_data_pmid=pd.concat([test_data_tpcs[1][1:],jp_tpcs_pmid[0]])
#calculating similarity between topics based on their topic distribution. try both cosine distances and hellinger distance.  
from sklearn.metrics import pairwise_distances
from scipy.spatial.distance import cosine
from scipy.linalg import norm
from scipy.spatial.distance import *
import scipy.cluster.hierarchy
import scipy
#In general
cos_sim = 1- pairwise_distances(jp_tpcs_data_array,tpcs_data_array, metric="cosine")
eucl_sim = 1- pairwise_distances(jp_tpcs_data_array,tpcs_data_array, metric="euclidean")

jsd_sim= 1-jsd_dist(jp_tpcs_data_array,tpcs_data_arr)   

tmp=pd.DataFrame(cos_sim)
tmp.columns=jp_pmid_list
tmp.index=jp_pmid_list #setting the index this way
tmp.to_csv('Cos_sim_jp_vs_jp.csv',sep='\t',index=False)
tmp=pd.DataFrame(eucl_sim)
tmp.columns=jp_pmid_list
tmp.index=jp_pmid_list 
tmp.to_csv('Euc_sim_jp_vs_jp.csv',sep='\t',index=False)
jsd_sim=1-jsd_dist(jp_tpcs_data_array,jp_tpcs_data_array) 
tmp=pd.DataFrame(jsd_sim)
tmp.columns=jp_pmid_list
tmp.index=jp_pmid_list 
tmp.to_csv('JSD_sim_jp_vs_jp.csv',sep='\t',index=False)



#define a Cosine Similarity matrix
def create_CosineSim_matrix(jp_tpcs_data_array,test_arr):
    from sklearn.metrics import pairwise_distances
    from scipy.spatial.distance import cosine
    #shape(tpcs_norm_array)   
    cos_sim = 1- pairwise_distances(jp_tpcs_data_array,test_arr, metric="cosine")     
   # dist_out = pairwise_distances(tpcs_norm_arr, metric="cosine")
    return cos_sim
    
    
#various implementations of Jenson Shannon distance:  
#ref:http://pastebin.com/yyf6efXs
def kl(p, q):
    """Kullback-Leibler divergence D(P || Q) for discrete distributions
    Parameters
    ----------
    p, q : array-like, dtype=float, shape=n
        Discrete probability distributions.    """
    p = np.asarray(p, dtype=np.float)
    q = np.asarray(q, dtype=np.float)
    return np.sum(np.where(p != 0, p * np.log(p / q), 0))   

#Jenson Shannon divergence between two vectors p,q
def myjsd(p,q):  
    m=0.5*(p+q)
    return(0.5*(kl(p,m)+kl(q,m)))
   
#to get matrix for pairwise jsd for an array of vectors   
def jsd_dist(jp_tpcs_data_array,test_arr):
    jsd_arr=np.zeros((len(jp_tpcs_data_array),len(test_arr)))    
    for i in range(len(jp_tpcs_data_array)):
        for j in range(len(test_arr)):
            jsd_arr[i,j]=myjsd(jp_tpcs_data_array[i],test_arr[j])            
    return jsd_arr 
    
out=open('JP_vs_JP_Tpc_sim.txt','w')
for i in range(len(jp_pmid_list)): 
   #to get the list of the similar scores in descending order
   # A=cos_sim[i][(-cos_sim[i]).argsort()] 
   #to get the pmids of the papers with most similar scores in descending order
   # A=(-cos_sim[i]).argsort()
   # B=[jp_pmid_list[i] for i in A] 
    print 'Using cos_sim',jp_pmid_list[i],':',[jp_pmid_list[k] for k in (-cos_sim[i]).argsort()]
    print 'Using cos_sim',jp_pmid_list[i],':',[cos_sim[i][k] for k in (-cos_sim[i]).argsort()]    
    print 'Using Eucl_sim',jp_pmid_list[i],':',[jp_pmid_list[k] for k in (-eucl_sim[i]).argsort()]
    print 'Using Eucl_sim',jp_pmid_list[i],':',[eucl_sim[i][k] for k in (-eucl_sim[i]).argsort()]
    print 'Using JS_sim',jp_pmid_list[i],':',[jp_pmid_list[k] for k in (-jsd_sim[i]).argsort()]
    print 'Using JS_sim',jp_pmid_list[i],':',[jsd_sim[i][k] for k in (-jsd_sim[i]).argsort()]
 
 #   out.write('Using Cos_sim '+str(jp_pmid_list[i])+': '+str([jp_pmid_list[i] for i in (-cos_sim[i]).argsort()]))
 #   out.write('Using Eucl_sim '+str(jp_pmid_list[i])+': '+str([jp_pmid_list[i] for i in (-eucl_sim[i]).argsort()]))
 #   out.write('Using JSD_sim '+str(jp_pmid_list[i])+': '+str([jp_pmid_list[i] for i in (-jsd_sim[i]).argsort()]))
out.close() 
'''  **DOESNT seem to work
#another one:http://stats.stackexchange.com/questions/29578/jensen-shannon-divergence-calculation-for-3-prob-distributions-is-this-ok
def entropy(prob_dist, base=math.e):
       return -sum([p * math.log(p,base) for p in prob_dist if p != 0]
def jsd(prob_dists, base=math.e):
    weight = 1/len(prob_dists) #all same weight
    js_left = [0,0,0]
    js_right = 0    
    for pd in prob_dists:
        js_left[0] += pd[0]*weight
        js_left[1] += pd[1]*weight
        js_left[2] += pd[2]*weight
        js_right += weight*entropy(pd,base)
    return entropy(js_left)-js_right

usage: jsd([[1/2,1/2,0],[0,1/10,9/10],[1/3,1/3,1/3]])
'''   
#usage: jsd([[1/2,1/2,0],[0,1/10,9/10],[1/3,1/3,1/3]])   
    
    
    
    
'''    
#define the Hellinger Matrix    
_SQRT2 = np.sqrt(2)     # sqrt(2) with default precision np.float64 

def hellinger1(p, q):
    return norm(np.sqrt(p) - np.sqrt(q)) / _SQRT2
 
def hellinger2(p, q):
    return euclidean(np.sqrt(p), np.sqrt(q)) / _SQRT2
 
def hellinger3(p, q):
    return np.sqrt(np.sum((np.sqrt(p) - np.sqrt(q)) ** 2)) / _SQRT2

def create_hell_dis_matrix(jp_tpcs_data_array,test_arr,_SQRT2):
    _SQRT2 = np.sqrt(2)  
    hell_arr=np.zeros((len(jp_tpcs_data_array),len(test_arr)))    
    for i in range(len(jp_tpcs_data_array)):
        for j in range(len(test_arr)):
            hell_arr[i,j]= 1- hellinger3(jp_tpcs_data_array[i],test_arr[j])
    return hell_arr
 '''           
tpcs_cosine_sim=create_CosineSim_matrix(jp_tpcs_data_array,tpcs_data_array)
test_data_tpcs_pmid.index=range(len(test_data_tpcs_pmid.index))
tmp=pd.DataFrame(tpcs_cosine_sim)
tmp.index=list(jp_tpcs_pmid) #add jp pmids as the index. drop the rows with duplicate indices
tmp["index"]=tmp.index 
tmp.drop_duplicates(cols="index",inplace=True)
jp_tpcs_pmid=tmp.index
tmp.drop(['index'],axis=1,inplace=True)
jp_tpcs_pmid=tmp.index#Note this list dropped from 47 to 35
## to get duplicates: 

Cos_sim_tpcs_df=tmp.T #THis is (221320, 35) dataframe wiht each of JP's papers as one columns -so can use argsort?)
Cos_sim_tpcs_df.index=list(test_data_tpcs_pmid)

#make sure no pmids are duplicated in the list- they are in jp_list!!
#if there are then do the following:
tmp=Cos_sim_tpcs_df[23166502] #or whatever the pmid is 
tmp.columns=[24603674,'to_drp'] 
tmp.drop(['to_drp'],axis=1,inplace=True)

##the hellinger matrix doesnt quite work as expected.-
#sim_Cos_df=pd.DataFrame(sim_Cos_matrix)
#sim_Cos_df.index=jp_tpcs_pmid[0]
#sim_Cos_df.columns=list(test_data_pmid)
saved the list of most similar docs for 2 pmids:23166502,24603674
save as pmids_for_23166502 and pmids_for_24603674 which are dataframes  
text_df=pd.read_csv('/data/priya/Topic_modelling/mallet-2.0.7/for_database/make_inferencer_20140221093827/all_20140221093827.txt', sep='\t',header=None,names=['pmid','title','abstract','keywords'])
new_pmids_for_23166502=pd.read_csv('23166502_TopicSim_pmids.csv',header=False,names=['pmid','cosine_sim'])
pmids_for_23166502_text=pd.merge(new_pmids_for_23166502,text_df,how='left',left_on='pmid',right_on='pmid',left_index=False, right_index=False,sort=False)
pmids_for_mypmid=pd.merge(pmids_for_mypmid,text_df,how='left',left_on='pmid',right_on='pmid',left_index=False, right_index=False,sort=False)


#Now to calculate similarities between abstracts of usr lib paper and pmids topically similar to it(from above)
##Merge abstarcts, titles and keywords. 
pmids_for_23166502_text['all_cols']=pmids_for_23166502_text['title']+pmids_for_23166502_text['abstract']+pmids_for_23166502_text['keywords']
pmids_for_mypmid['all_text']=pmids_for_mypmid['title']+pmids_for_mypmid['abstract']+pmids_for_mypmid['keywords']

text_pmid_list=list(pmids_for_23166502_text.pmid)
text_corpus=list(pmids_for_23166502_text.all_cols)

text_pmid_list=list(pmids_for_mypmid.pmid)
text_corpus=list(pmids_for_mypmid.all_text)

##using Gensim
from gensim import corpora, models, similarities
import gensim
import sys
import csv
import re
import string
word_set=[[re.sub("[\s\*\=\+\.\)\(-\:$\" ]","",i) for i in str(line).lower().split(' ')] for line in text_corpus]
dictionary_out = corpora.Dictionary(word_set)
stp_list1=open('/data/priya/Topic_modelling/mallet-2.0.7/stoplists/en.txt','r').read().split('\n')
stoplist=[x.strip(' ') for x in (stp_list1)] 
stop_ids = [dictionary_out.token2id[stopword] for stopword in stoplist if stopword in dictionary_out.token2id]
once_ids = [tokenid for tokenid, docfreq in dictionary_out.dfs.iteritems() if docfreq == 1]
dictionary_out.filter_tokens(stop_ids+once_ids) # filter out or remove stop words
dictionary_out.compactify()
dict_out=csv.writer(open('text_corpus_23166502_dictionary.csv','w'),delimiter=':')
for key,value in dictionary_out.token2id.items():
    dict_out.writerow([key,value])  

class OutCorpus(object):
    def __iter__(self):
        for line in text_corpus:
        # assume there's one document per line, tokens separated by whitespace
            yield dictionary_out.doc2bow(str(line).lower().split(' '))   

text_23166502_bow=OutCorpus() 
outfile=open('text_2323166502_bow.txt','w')
for doc in text_2323166502_bow:
    outfile.write(str(doc)+'\n')
outfile.close()    


  
##using nltk

from sklearn.feature_extraction.text import CountVectorizer
import scipy as sp
vectorizer=CountVectorizer(min_df=1, decode_error=u'ignore', stop_words=stoplist,strip_accents='ascii')
X=vectorizer.fit_transform(str(line) for line in text_corpus)
X.toarray()
len(X.toarray())
vectorizer.get_feature_names() #lot of random words?? ...but regular words further down the list. Fewer random words Gensim version??)
num_samples,num_features=X.shape

#getting jp abstract data. ACtually should get it all at one go..but for now do one by one.
text_q='select pa.pmid, pp.ArticleTitle, pa.AbstractText from pubmed_pubmed as pp  inner join pubmed_abstract as pa on pp.pmid=pa.pmid  where pp.pmid =23166502;'
text_q='select pa.pmid, pp.ArticleTitle, pa.AbstractText,pa.keywords from pubmed_pubmed as pp  inner join pubmed_abstract as pa on pp.pmid=pa.pmid  where pp.pmid =20220758;'
JP_text_23166502=psql.frame_query(text_q,mysql_con)
JP_text_mypmid=psql.frame_query(text_q,mysql_con)
JP_text_23166502.text=JP_text_23166502.ArticleTitle+JP_text_23166502.AbstractText
JP_text_mypmid.all_text=JP_text_mypmid.ArticleTitle+JP_text_mypmid.AbstractText
new_paper=list(JP_text_23166502.text)
new_paper=list(JP_text_mypmid.all_text)
new_paper_vec=vectorizer.transform(new_paper)
new_paper_vec.toarray()

#getting euclidean distance between normalized vectors v1 and v2,ie not using raw counts.

def dist_norm(v1,v2):
   v1_norm=v1/sp.linalg.norm(v1.toarray())
   v2_norm=v2/sp.linalg.norm(v2.toarray())
   delta=v1_norm-v2_norm
   return sp.linalg.norm(delta.toarray())
   
   
import sys
best_doc=None
best_dist=sys.maxint
best_i=None
for i in range(0,num_samples):
    post=text_corpus[i]
    if post == new_paper:
        continue
    post_vec=X.getrow(i)
    d=dist_norm(post_vec,new_paper_vec)
  #  print "===Post %i with dist%.2f: %s"% (i, d, post)
    if d <best_dist:
        best_dist = d
        best_i = i
        
print ("best paper is %i with dist=%.2f"%(best_i,best_dist)  )      

dist_list=[]              
for i in range(0,num_samples):
     dist_list.append(dist_norm(new_paper_vec,X.getrow(i)))  
     
#need to get both the list in increasing order and the pmids>     
vals = numpy.array(dist_list)
sort_index = numpy.argsort(vals)
OR
sorted(range(len(dist_list)), key=lambda k: dist_list[k])
OR
from operator import itemgetter
indices, dist_sorted = zip(*sorted(enumerate(dist_list), key=itemgetter(1)))
list(dist_sorted)
list(indices)
#for JP 
out=open('papers_similar_to_23166502_using_l2_norm.txt','w')
out.write('Pmid     l2_norm     title')
for i in indices[0:50]:
     #out.write(str(text_pmid_list[i])+'\t'+str(dist_list[i])+'\t'+ pmids_for_23166502_text['title'][i]+'\n')
     out.write(str(text_pmid_list[i])+'\t'+str(dist_list[i])+'\t'+ pmids_for_mypmid['title'][i]+'\n')
     
out.close()   
***********************
#tfidf vectorizer: can Use my own code: from create_tpc_vec.py. But try with nltk :
#X is a scipy.sparse.csc.csc_matrix
#X.toarray() is a numpy array.

from sklearn.feature_extraction.text import TfidfVectorizer
vectorizer=TfidfVectorizer(min_df=1, decode_error=u'ignore', stop_words=stoplist,strip_accents='ascii')
X=vectorizer.fit_transform(str(line) for line in text_corpus)
vectorizer.get_feature_names()
from sklearn.metrics import pairwise_distances
from scipy.spatial.distance import cosine
##I dont want a matrix- I am comparing one abstract against the other 5000

def Cos_dist_norm(v1,v2):
   return cosine(v1.toarray(),v2.toarray())
   
Cos_dist_list=[]
for i in range(0,num_samples):
     Cos_dist_list.append(Cos_dist_norm(new_paper_vec,X.getrow(i)))
     
from operator import itemgetter
cos_indices,cos_dist_sorted = zip(*sorted(enumerate(Cos_dist_list), key=itemgetter(1)))
list(cos_dist_sorted)
list(cos_indices)    
 
out=open('papers_similar_to_23166502_using_tfidfvectors_and cosdist.txt','w')
out.write('Pmid    Cosine_dist     title\n')
for i in cos_indices[0:50]:
     out.write(str(text_pmid_list[i])+'\t'+str(Cos_dist_list[i])+'\t'+ pmids_for_mypmid['title'][i]+'\n')
     
out.close()   



##to get all the files of similar paper from the topically similar list:
#Get all the jp pmids(from the topic prob file):
jp_tpcs=pd.read_csv('/data/priya/Topic_modelling/mallet-2.0.7/for_database/Calculating_Rec_Scores/JP_test_set/JP_papers_LDA_prob.txt', sep='\t',header=None)
jp_tpcs_pmid=jp_tpcs[0]
jp_tpcs_pmid=unique(jp_tpcs_pmid)
text_df=pd.read_csv('/data/priya/Topic_modelling/mallet-2.0.7/for_database/make_inferencer_20140221093827/all_20140221093827.txt', sep='\t',header=None,names=['pmid','title','abstract','keywords'])
from sklearn.feature_extraction.text import CountVectorizer
import scipy as sp
stp_list1=open('/data/priya/Topic_modelling/mallet-2.0.7/stoplists/en.txt','r').read().split('\n')
stoplist=[x.strip(' ') for x in (stp_list1)]

for pmid in jp_tpcs_pmid:
    user_q='select pa.pmid, pp.ArticleTitle, pa.AbstractText,pa.AbstractOrder from pubmed_pubmed as pp  inner join pubmed_abstract as pa on pp.pmid=pa.pmid  where pp.pmid ='+str(pmid)
    JP_text_mypmid=psql.frame_query(user_q,mysql_con)
    P_text_mypmid.all_text=JP_text_mypmid.ArticleTitle+JP_text_mypmid.AbstractText
    new_paper=list(JP_text_mypmid.all_text)
    pmids_for_mypmid=pd.read_csv('22307276_TopicSim_pmids.csv',header=False,names=['pmid','cosine_sim'])
    pmids_for_mypmid=pmids_for_mypmid.head(5000)
    pmids_for_mypmid=pd.merge(pmids_for_mypmid,text_df,how='left',left_on='pmid',right_on='pmid',left_index=False, right_index=False,sort=False) 
    pmids_for_mypmid['all_text']=pmids_for_mypmid['title']+pmids_for_mypmid['abstract']+pmids_for_mypmid['keywords']
    text_pmid_list=list(pmids_for_mypmid.pmid)
    vectorizer=CountVectorizer(min_df=1, decode_error=u'ignore', stop_words=stoplist,strip_accents='ascii')
    text_corpus=list(pmids_for_mypmid.all_text)
    X=vectorizer.fit_transform(str(line) for line in text_corpus)
    X.toarray()
    len(X.toarray())
    num_samples,num_features=X.shape
    
    new_paper=list(JP_text_mypmid.all_text)
    new_paper_vec=vectorizer.transform(new_paper)


#######
This works:
for jpmid in jp_tpcs_pmid:
qq='select distinct(pp.pmid),pp.DateCreated,pa.pmid,pa.AbstractText,pa.AbstractOrder from pubmed_pubmed as pp inner join pubmed_abstract as pa on pp.pmid=pa.pmid  where pp.pmid =%d'%jpmid
data=psql.frame_query(qq,mysql_con) 
shape(data)


###############JP wants Similar papers from all the papers in the last one year.
#get 2014 data
os.chdir('/data/priya/LDA_data/2014/') 


Sample_set

JP wants 