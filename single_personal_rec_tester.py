#!/usr/bin/env python
# To test the  creation of single library personal rec files r
#usage: 
# python ~/Scirec_scripts/single_personal_rec_tester.py 75

import sys
import subprocess

lib_id = sys.argv[1]

%run ~/Scirec_scripts/Single_libraryPersonal_recs_WORKING.py lib_id 
subprocess.call("cut  -f5 *personal*")
