#!/user/bin/python
'''
cronjob to update all libraries for all users daily.
'''
import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
import os
import glob
import shutil
import numpy as np
import itertools
from bibtexparser.bparser import BibTexParser
import subprocess
import cPickle as pickle
import immediateRecs as ir
## additional imports from me. 
import argparse
import pubRecordsProduction as pr # natalie's module
import Data_processing_scripts as cpr # priya's module
import random

### Know which index you're outputting at...
out_ind_ct=0
#### Make recommendations with the usual process for users' liked libraries.
mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
q='SELECT * FROM metrics_likes'
# Store it as a pandas data frame
data=psql.frame_query(q,con=mysql_con)
# Here, the list of libraries is ACTUALLY a list of users. 
lib_list=list(pd.unique(data.userid_id))
print lib_list
date = pr.todays_date()
out = '/home/ubuntu/Personalized_Recs/cronjob_testing/new/'+str(date)+"_liked_recs.txt"
#out = '/home/ubuntu/Personalized_Recs/cronjob_testing/new/'+str(date)+"_recs.txt"
o = open(out,'a')

# Given the libraries organized, use each one in successive recommendations. 
corp_pubfile='/home/ubuntu/lda-data/Current_data/Current_basic_dataset_with_IF_and_LDA.csv'
for lib_id in lib_list:
    print "updating recs for library ", lib_id
    lib_publist = list(data.dbid[data.userid_id == lib_id])
    print lib_publist
    print len(lib_publist)
    u_id= str(list(data.userid_id[data.userid_id == lib_id])[0])
    date = pr.todays_date()
    libname=lib_id
    dbname =str(list(data.dbname[data.userid_id == lib_id])[0])
    outpath='/home/ubuntu/Personalized_Recs/cronjob_testing/Vocab_testing/'+str(lib_id)
    if not os.path.exists(outpath):
        os.makedirs(outpath)
    outpath='/home/ubuntu/Personalized_Recs/cronjob_testing/Vocab_testing/'+str(lib_id)
    lib,lib_publist =cpr.getting_usrpubRecs_andforLDA([int(i) for i in lib_publist],str(lib_id),outpath)
    tmp_corp = pr.get_corp_info(corp_pubfile,delim=",")
    corp=[paper for paper in tmp_corp if paper.pmid not in lib_publist]
    os.chdir(outpath)
    cpr.run_lda_user(outpath+"/ForLDA_"+str(lib_id)+".txt",str(lib_id)) # Run topic inferencer
    print "done lda for ", lib_id
    inference_results = outpath+'/'+str(lib_id)+"LDA_composition_with_pmid.txt" # Identify the file name with the topic assignments
    lib = pr.get_tpcs_lib(inference_results,lib)
    corp = pr.shared_tpc_cutter_3(lib,corp,frac=0.5) # Fraction can be modified by adding a frac=n line; otherwise, default is 0
    simmat,updated_corp_list= cpr.create_updated_termFreqVec_AND_calcSimilarity(pr.pmid_list(corp),outpath+"/ForLDA_"+str(lib_id)+".txt")
 #   simmat = cpr.vocab_list_Similarity_calculator(pr.pmid_list(corp),outpath+"/ForLDA_"+str(lib_id)+".txt") 
 #if any papers have been removed in the Vocab step because they did not have titles/etc make sure you update the corp:   
    new_corp=[paper for paper in corp if paper.pmid in updated_corp_list]
    print "word_sim done for lib ", lib_id
    corp_score_list = pr.word_sim_combined_score(simmat,lib,new_corp)
    corp_score_list = pr.metadata_weighter(new_corp,corp_score_list,use=[.1,1,1],dbgvar=0)
    best_recs = pr.choose_best_from_corp(new_corp,corp_score_list)
    outputerror = 0  #This is a little error flag -- you'll try to write to output, and if it fails, you'll return an error flag in the end 
    for i, pub in enumerate(best_recs): # For every single pub 
           output = str(out_ind_ct)+"\t"+str(libname)+"\t"+str(-1)+"\t"+dbname+"\t"+str(pub.pmid)+"\n"
           out_ind_ct +=1
           #print output
           o.write(output)
