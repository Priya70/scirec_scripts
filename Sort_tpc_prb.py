#!/usr/bin/local/python

def main(args):
    input=args[1]
    data=np.loadtxt(input)
##to sort data array with regards to 3rd column
    arr = data[data[:,3].argsort()]
#To get data in descending order do:
    arr = data[-data[:,3].argsort()]
# or read in a dtype as:
    data=np.loadtxt(input,dtype=[('pmid', '<f8'), ('one', '<f8'), ('two', '<f8'), ('three', '<f8'), ('four', '<f8'), ('five', '<f8'), ('six', '<f8'), ('seven', '<f8'), ('eight', '<f8'), ('nine', '<f8'), ('ten', '<f8'), ('eleven', '<f8'), ('twelve', '<f8'), ('thirteen', '<f8'), ('fourteen', '<f8'), ('fifteen', '<f8'), ('sixteen', '<f8'), ('seventeen', '<f8'), ('eighteeen', '<f8'), ('nineteen', '<f8'), ('twenty', '<f8')])
#and  sort by:
    sort(data,order='one')

if __name__ == "__main__":
    main(sys.argv) 