#! /usr/local/bin/python

import pandas as pd
import numpy as np
from datetime import datetime,timedelta
import Data_processing_scripts as dp
import os  
import sys 
import glob
import shutil
from dateutil import rrule

#update paper data -or should that have already been done by cron job??
dp.update_new_papers_data()
#make my basic paper_set
dp.create_basic_papers_dataset() 
os.chdir('/home/ubuntu/lda-data/Current_data/')
basic_all_data=pd.read_csv('Current_basic_dataset_with_IF_and_LDA.csv')
#This data has cols=pmid,150 tpc probs,pmid,DateCreated,ISSN,ISSNLinking 





#collect all the dates/issn etc info into a table
#oldest paper seems to have a DateCreated of 1946-12-31. 48900 papers have this Date Created. 
#if to_date.year>from_date.year:
    #need to go into two different directories
os.chdir(int(from_year))
filelist=glob.glob("Uniq*") #give correct path name
basic_data=pd.read_csv(filelist[0],usecols=[0,1,5], header=None, skiprows=1, sep='\t', names=['pmid','DateCreated','ISSNLinking'])
for file in filelist[1:]:
    tmp_data=pd.read_csv(file,usecols=[0,1,5], header=None, skiprows=1, sep='\t', names=['pmid','DateCreated','ISSNLinking'])
    basic_data=pd.concat([basic_data,tmp_data],ignore_index=True)
    
'''for dt in rrule.rrule(rrule.MONTHLY, dtstart=from_date, until=to_date):
    mon=dt.strftime("%b")
    filename=glob.glob("Uniq_"+mon+'*')
    filelist.append(glob.glob("Uniq_"+mon+'*'))       
   
   for month in range(int(fromdate.month),int(todate.month)+1,1):        
        tt=(datetime.strptime(fd,"%Y-%m-%d")).strftime("%b")
        data.to_csv(tt+'_'+str(startyear)+'_data.csv',index=False)
''' 
lda_filelist=glob.glob("*composition*") 
basic_lda_data=pd.read_csv(lda_filelist[0], header=None, sep='\t')
for file in lda_filelist[1:]:
    tmp_lda_data=pd.read_csv(file, header=None,sep='\t')
    basic_lda_data=pd.concat([basic_lda_data,tmp_lda_data],ignore_index=True)
#The complete basic papers set is:    
basic_all_data=pd.merge(basic_lda_data,basic_data,how='left', left_on=basic_lda_data[0], right_on='pmid',sort=False)
#drop papers that are too old:
to_date=(datetime.now()).date()
from_date=str((datetime.now()).date() - timedelta(180))
basic_all_data=basic_all_data[basic_all_data.DateCreated > from_date]



#need to get relevant data from the database
mysql_con= MySQLdb.connect(host='localhost', user='scird', passwd='scirdpass', db='scireader') #host ='171.65.77.20' if NOT connecting from the server
q= "select pmid, DateCreated, DateCompleted, DateRevised, Pubdate, JournalTitle,ISSN,ISSNLinking from pubmed_pubmed"
pubmed_data=psql.frame_query(q,mysql_con)
qr= "select pmid, DateCreated, DateCompleted, DateRevised, Pubdate, JournalTitle,ISSN,ISSNLinking from pubmed_recent_pubmed"
pubmed_recent_data=psql.frame_query(qr,mysql_con)
pubmed_data=pd.concat([pubmed_data,pubmed_recent_data])
r="select journals_impactfactor.IF, journals_impactfactor.ISSN from journals_impactfactor" 
if_jourdata=psql.frame_query(r,mysql_con)
compdata=pd.read_csv('/data/priya/Topic_modelling/mallet-2.0.7/for_database/Calculating_Rec_Scores/all_20140221093827_150_9603compostion.txt',usecols=[0,1,2,3], header=None, skiprows=1, sep='\t', names=['ind','pmid','tpcno1','tpcprob1'])
pubmed_data['pmid']=pubmed_data['pmid'].apply(int)
pubmed_data.drop_duplicates(cols='pmid',inplace=True)
comp_date_data=pd.merge(compdata,pubmed_data,how='left', left_on='pmid', right_on='pmid',sort=False)

#mysql_con.close()

def g(x):
    if x[7]==None:
        return x[4]
    else:
        return x[7]     
   
    
comp_date_data['MostRecentDate']=comp_date_data.apply(g,axis=1)
#chk sum(comp_date_data.MostRecentDate.isnull())
comp_date_data.drop(['DateCreated','DateCompleted','Pubdate','DateRevised'],axis=1,inplace=True)
#Turns out that some of the if_jourdata have duplicates. Pick the one with max IF factor. 

aa=if_jourdata.set_index('ISSN').index.get_duplicates()

for ISSN in aa:
      bb=if_jourdata.IF[if_jourdata.ISSN == ISSN]
      if_jourdata.IF[if_jourdata.ISSN == ISSN]=max(bb)
      
#Remove the duplicate rows
if_jourdata.drop_duplicates(cols='ISSN',inplace=True)
ifdata1=pd.merge(comp_date_data,if_jourdata,how='left',left_on='ISSNLinking',right_on='ISSN',sort=False) #This does not have the same order as far as pmids are concerened because you link on ISSN nos.
##There are  many pmids for which there is an ISSN # but NO ISSNLInking #.
#replace the ISSNLinking numbers by the ISSN numbers,

ifdata1.ISSNLinking[ifdata1.ISSNLinking.isnull()]=ifdata1.ISSN_x[ifdata1.ISSNLinking.isnull()]
#then drop  'ISSN_x','ISSN_y'. I do this because for the majority of journals the match is on ISSNLinking.
#chk to see if eLife already has the correct ISSNLining num.
ifdata1.drop(['ISSN_x','ISSN_y'],axis=1,inplace=True)
#manually put in elife's IF=15
ifdata1.IF[ifdata1.JournalTitle == 'eLife'] =15
#There are journals with no IF-convert the IF of these journals into 1.
ifdata1.IF[ifdata1.IF == 0.0]=1.0
#Remove all pmids belonging to 
missingdates=list(ifdata1.index[ifdata1.MostRecentDate.isnull()])
ifdata1.(ifdata1.index[missingdates],inplace=True)

def datefunc1(x):
    now=datetime.now()
    delta=now.date()- (x.MostRecentDate)
    if delta.days>90:
       datefctr=0
       return datefctr   
    if 90>= delta.days >60:
       datefctr=(.5)**(delta.days-60.0)  
       return datefctr
    elif 60.0 >= delta.days >=  15:
       datefctr=2.5-((1.5/45)*delta.days)  
       return datefctr
    else:
       datefctr=2
       return datefctr

ifdata1['datefactor']=ifdata1.apply(datefunc1,axis=1)

def impactfactorfctr(x):
    im=x.IF
    if im >=30.0:
        im_fctr=8.0
        return im_fctr
    if (30.0>im) & (im>=20.0):
        im_fctr=5.0
        return im_fctr
    if (20.0>im) &  (im>=10.0):
        im_fctr=3.0   
        return im_fctr    
    if (10.0>im) & (im>=4.0):  
        im_fctr=1.0   
        return im_fctr 
    else:     
        im_fctr=0.1        
        return im_fctr      

ifdata1['impact_factor_fctr']=ifdata1.apply(impactfactorfctr,axis=1)

ifdata1['Rec_Score1']=ifdata1['tpcprob1']*8.0+ifdata1['datefactor']+(ifdata1['impact_factor_fctr'])*0.5
ifdata1['Rec_Score1'][ifdata1.datefactor == 0]=0

#Making topic lists
for i in xrange(150):
    tmpdf = ifdata1[ifdata1.tpcno1 == i].sort('Rec_Score1',ascending=False)  
    tmpdf=tmpdf.ix[:,['tpcno1','pmid','Rec_Score1']]
    tmpdf=tmpdf.reset_index()
    tmpdf.drop(['index'],axis=1,inplace=True)
    tmpdf.to_csv('/data/priya/For_Yonggan/SciScores/Topic_lists_sorted/'+str(i)+"_sorted_"+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t',index_label='Index')

#making a single list. Note: this file is for the db, hence the numbering must begin from 1. Adding 1 to topic numbers and suing linecnt instead of index."
out=open('/data/priya/For_Yonggan/SciScores/All_tpids_sorted'+datetime.now().strftime('%m_%d_%y')+'.txt','w')
linecnt=1 
#out.write("id\tdbname\tdbid\tsubcatid\tprobability\n")
#for i in xrange(150):
for i in range(150):
    print i
    if i == 0:
         list=[0,41]
         tmp0=pd.read_csv('/data/priya/For_Yonggan/SciScores/Topic_lists_sorted/'+str(list[0])+'_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t')
         tmp1=pd.read_csv('/data/priya/For_Yonggan/SciScores/Topic_lists_sorted/'+str(list[1])+'_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t')
         new= pd.concat([tmp0,tmp1]).sort('Rec_Score1',ascending=False).head(500)
         new.to_csv('/data/priya/For_Yonggan/SciScores/Topic_lists_sorted/'+str(i)+"_edit_sorted_"+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t',index=False)         
         tmp=open('/data/priya/For_Yonggan/SciScores/Topic_lists_sorted/'+str(i)+'_edit_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt').readlines()
         print "i = " + str(i)
         for line in tmp[1:501]:   
          #  out.write(str(linecnt)+'\n')            
            tt=line.split('\t')
           # print  str(linecnt)+"\tpubmed\t"+str(int(i)+1)+'\t'+tt[2]+'\t'+tt[3]
            out.write(str(linecnt)+'\t'+str(int(i)+1)+"\tpubmed\t"+tt[2]+'\t'+tt[3])
            linecnt=linecnt+1 
         print linecnt       
    elif i == 3:
         list=[3,9]
         tmp0=pd.read_csv('/data/priya/For_Yonggan/SciScores/Topic_lists_sorted/'+str(list[0])+'_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t')
         tmp1=pd.read_csv('/data/priya/For_Yonggan/SciScores/Topic_lists_sorted/'+str(list[1])+'_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t')
         new= pd.concat([tmp0,tmp1]).sort('Rec_Score1',ascending=False).head(500)
         new.to_csv('/data/priya/For_Yonggan/SciScores/Topic_lists_sorted/'+str(i)+"_edit_sorted_"+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t',index=False)
         print "i = " + str(i)
         tmp=open('/data/priya/For_Yonggan/SciScores/Topic_lists_sorted/'+str(i)+'_edit_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt').readlines()
         for line in tmp[1:501]:   
          #  out.write(str(linecnt)+'\n')
            tt=line.split('\t')
           # print  str(linecnt)+"\tpubmed\t"+str(int(i)+1)+'\t'+tt[2]+'\t'+tt[3]
            out.write(str(linecnt)+'\t'+str(int(i)+1)+"\tpubmed\t"+tt[2]+'\t'+tt[3])
            linecnt=linecnt+1    
         print linecnt        
    elif i == 8:
         list=[8,146]
         tmp0=pd.read_csv('/data/priya/For_Yonggan/SciScores/Topic_lists_sorted/'+str(list[0])+'_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t')
         tmp1=pd.read_csv('/data/priya/For_Yonggan/SciScores/Topic_lists_sorted/'+str(list[1])+'_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t')
         new= pd.concat([tmp0,tmp1]).sort('Rec_Score1',ascending=False).head(500)
         new.to_csv('/data/priya/For_Yonggan/SciScores/Topic_lists_sorted/'+str(i)+"_edit_sorted_"+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t',index=False)
         print "i = " + str(i)
         tmp=open('/data/priya/For_Yonggan/SciScores/Topic_lists_sorted/'+str(i)+'_edit_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt').readlines()
         for line in tmp[1:501]:              
            tt=line.split('\t')
          #  print  str(linecnt)+"\tpubmed\t"+str(int(i)+1)+'\t'+tt[2]+'\t'+tt[3]
            out.write(str(linecnt)+'\t'+str(int(i)+1)+"\tpubmed\t"+tt[2]+'\t'+tt[3])
            linecnt=linecnt+1    
         print linecnt      
    elif i == 134:
         list=[134,138]
         tmp0=pd.read_csv('/data/priya/For_Yonggan/SciScores/Topic_lists_sorted/'+str(list[0])+'_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t')
         tmp1=pd.read_csv('/data/priya/For_Yonggan/SciScores/Topic_lists_sorted/'+str(list[1])+'_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t')
         new= pd.concat([tmp0,tmp1]).sort('Rec_Score1',ascending=False).head(500)
         new.to_csv('/data/priya/For_Yonggan/SciScores/Topic_lists_sorted/'+str(i)+"_edit_sorted_"+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t',index=False)
         print "i = " + str(i)
         tmp=open('/data/priya/For_Yonggan/SciScores/Topic_lists_sorted/'+str(i)+'_edit_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt').readlines()
         for line in tmp[1:501]:   
            tt=line.split('\t')
            out.write(str(linecnt)+'\t'+str(int(i)+1)+"\tpubmed\t"+tt[2]+'\t'+tt[3])
            linecnt=linecnt+1
         print linecnt          
    else:  
         print "getting into else loop"   
         tmp=open('/data/priya/For_Yonggan/SciScores/Topic_lists_sorted/'+str(i)+'_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt').readlines()
         print "i = " + str(i)
         for line in tmp[1:501]:  
            tt=line.split('\t')
          #  print  str(linecnt)+"\tpubmed\t"+str(int(i)+1)+'\t'+tt[2]+'\t'+tt[3]
            out.write(str(linecnt)+'\t'+str(int(i)+1)+"\tpubmed\t"+tt[2]+'\t'+tt[3])
            linecnt=linecnt+1
         print linecnt      
out.close()      
# Now I need to delete some of the topic ids:
 data=pd.read_csv('/data/priya/For_Yonggan/SciScores/All_tpids_sorted'+datetime.now().strftime('%m_%d_%y')+'.txt',sep='\t',header=None,names=['id','subcatid','dbname','pmid','prob'])     
 #to test if data is ok:
 # for i in range(1,151,1):
#             print shape(data[data.subcatid == i])

 del_list=[10,11,14,21,29,42,48,49,56,60,65,73,103,104,112,113,121,124,127,139,147,149]
 for i in del_list:
    data=data[data.subcatid != i]
 data.drop(['id'],axis=1,inplace=True)   
 aa=range(len(data.subcatid)+1)[1:]   
 data.id=aa
 data.to_csv('/data/priya/For_Yonggan/SciScores/All_tpids_sorted'+datetime.now().strftime('%m_%d_%y')+'.txt',sep='\t',index=False,header=False)

#(Using Y's +1 numbering scheme )the following subtpcids occur twice: 80, 110
            
##some cool code to change order of columns:
#cols = data.columns.tolist()
#cols = [new list]
#data=data[cols]

#Making Journal lists
ISSN_List= pd.unique(list(ifdata1.ISSNLinking))

for issn in ISSN_List:
    tmpdf = ifdata1[ifdata1.ISSNLinking == issn].sort('Rec_Score1',ascending=False) 
    tmpdf=tmpdf.ix[:,['tpcno1','pmid','Rec_Score1']]
    tmpdf=tmpdf.reset_index() 
    tmpdf.drop(['index'],axis=1,inplace=True)
    jour_name=ifdata1[ifdata1.ISSNLinking == issn].iloc[0,4]
    tmpdf.to_csv('/data/priya/For_Yonggan/Journals_sorted/'+issn+"_sorted_df.txt", sep='\t',index_label='Index')
    
   

    