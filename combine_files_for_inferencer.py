#! /usr/local/bin/python
# usage: combine_files_for_inferencer.py pubmed.txt kewyord.txt abstract.txt 
import re
import sys
import codecs
#sys.stdout = codecs.getwriter('utf8')(sys.stdout)


def main(args):
   if len(args) == 1:
    return
   else:
    title_file,keywds_file,abstract_file = args[1:4] 
    #get pmids and abstracts"
    pubmed=open(abstract_file,'r').readlines()
    pubmed_pmid=[]
    pubmed_abstract=[]
    for line in pubmed:
          pubmed_pmid.append(line.split('\t')[0])   
          pubmed_abstract.append(re.sub('[|:\n\r]','',line.split('\t')[1].split('\n')[0]))
    #Get pmids and keywords
    key=open(keywds_file,'r').readlines()
    key_pmid=[]
    key_txt=[] 
    for line in key:
        key_pmid.append(line.split('\t')[0])
        key_txt.append(re.sub('[|:\n\r]','',line.split('\t')[1].split('\n')[0]))
    
     #Get pmids and titles
    title=open(title_file,'r').readlines()
    title_pmid=[]
    title_txt=[] 
    all_text=[]
    out=open('pubmed_for_inferencer.txt','w')
    for line in title:
        xx=line.split('\t')[0] 
        title_pmid.append(xx)
        title_txt.append(line.split('\t')[1].split('\n')[0]) 
        str="|Title\t"+re.sub('[|:\n\r]','',line.split('\t')[1].split('\n')[0])
        if xx in pubmed_pmid:
            jj=[i for i, j in enumerate(pubmed_pmid) if j == xx]
            for ind in jj:
                str=str+pubmed_abstract[ind]+' '
        if  xx in key_pmid:
            yy=[i for i, j in enumerate(key_pmid) if j == xx]
            for indx in yy:
                str=str+key_txt[indx]+','
        all_text.append(str)
        if  len(str.split())>120:
            xx=re.sub('[|:\n\r]','',xx)
            ''.join([x for x in xx if ord(x) < 128 and ord(x) != 26])
            ''.join([x for x in str if ord(x) < 128 and ord(x) != 26])
          #  print '%s \t %s' %(xx,str)
            out.write(xx+'\t'+str+'\n')
    out.close()        
  
                
  
         
if __name__ == "__main__":
    main(sys.argv)                
            

            