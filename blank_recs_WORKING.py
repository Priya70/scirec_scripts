#! /user/bin/python

'''
cronjob to update recommendations for all libraries for all users daily. This is version3 and 
uses the incremental updates model- does not recalculate lda for all users as in the first version. 
'''
import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
import os
import glob
import shutil
import numpy as np
import itertools
from bibtexparser.bparser import BibTexParser
import subprocess
import cPickle as pickle
from boto.s3.connection import S3Connection
from boto.s3.key import Key
import boto
## additional imports from me. 
import argparse
import pubRecordsProduction  as pr # natalie's module
import immediateRecs as ir #natalie's immediate recs module
import Data_processing_scripts as cpr # priya's module
import logging
date = pr.todays_date()
logging.basicConfig(filename='/home/ubuntu/Logging_info/'+str(date)+'blank_recs.log',level=logging.DEBUG, filemode='w')
#make sure update_corpus_TermFreqVec() is run first!!
#make sure  update_pytables_for_libraries() is run next before running this

##### Obtain information about libraries
#Identify all of the papers to go through the usual process -- these will be in library_library_papers.
mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
q='select libid_id from library_library_papers'
data=psql.frame_query(q,con=mysql_con)
lib_list=list(pd.unique(data.libid_id))
# This library list is the list of libraries with papers. 
print lib_list

# Also identify ALL libraries present -- use this for your master list of libraries later. 
q='select id,userid_id from library_libraries'
data=psql.frame_query(q,con=mysql_con)
full_lib_list = list(pd.unique(data.id))
print full_lib_list

# Define some output settings

## Files
out = '/home/ubuntu/Personalized_Recs/cronjob_testing/new/'+str(date)+"_blank_recs.txt"
#out = '/home/ubuntu/Personalized_Recs/cronjob_testing/'+str(date)+"_nolibrary_recs_test.txt" # Test file for developing immediate recs for blank libraries
o = open(out,'a')
corp_pubfile='/home/ubuntu/lda-data/Current_data/Current_basic_dataset_with_IF_and_LDA.csv'
## Output line indexer and date for file name
out_ind_ct=1
date = pr.todays_date()

### Now identify which papers are in the full lib list and NOT in the lib list with papers in it
blank_libs = list(set(full_lib_list) - set(lib_list)) # Set subtraction - everything in the full lib list that ISN'T in the regular lib list
print len(blank_libs), blank_libs
##### Recommendations
### For every library, check whether it's empty or not, then make recs for it.
for lib_id in blank_libs: ## Iterate over EVERY library, even those which don't actually have papers in them...
    try: # ATtempt to make recommendations; this may at some point fail because there are no topics/no user information, ergo the try
        print "no file in s3"
        logging.debug(str(lib_id)+' doesnt have an h5 library yet.' )

	# Define some useful identifiers for output.
	u_id= str(list(data.userid_id[data.id == lib_id])[0])
	date = pr.todays_date()
	libname=lib_id     

	################# WARNING. THIS PARTICULAR DEFAULT ONLY WORKS FOR PAPERS WHICH ARE ALL FROM PUBMED.
	# A challenge -- there can be no dbname since there's no papers known yet. That siad, we recommend FROM pubmed, so I'll
	### Set the default here to PUBMED.
	dbname = "pubmed" ##### THIS IS A DEFAULT. THIS SHOULD BE CHANGED IF WE EVER RECOMMEND NOT FROM PUBMED.

	# Define a set of immediate recs.	
	immediaterecs = ir.blank_library_recs(u_id,corp_pubfile)
	# Then, output those recs.
	for i, pub in enumerate(immediaterecs): # For every single pub
             # output = str(out_ind_ct)+"\t"+"NULL"+"\t"+str(libname)+"\t"+dbname+"\t"+str(pub.pmid)+"\n"
             output = str(out_ind_ct)+"\t"+str(u_id)+"\t"+str(libname)+"\t"+dbname+"\t"+str(pub)+"\n"
             out_ind_ct += 1
             # print output
             o.write(output)
	continue # Now, continue on.
    except:
	print "User", u_id, " did not have enough information for immediate recs"
o.close()
