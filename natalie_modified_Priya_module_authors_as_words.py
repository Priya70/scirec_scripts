#! /usr/local/bin/python
import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
from datetime import datetime,timedelta 
#import h5py
import subprocess
import os
from dateutil import rrule
import glob
import shutil
import numpy as np
import itertools
from bibtexparser.bparser import BibTexParser
import pubRecords_1 as pr


def get_MysqlData(startyear):
    '''
    function that takes a year in YYYY format and pulls out all the relevant data from the mysql database. Note that this function returns a file that may have multiple lines corresponding to the same pmid, but different Abstract Orders, as each AbstractOrder has a separate record. 
    MysqlData_to_OneRecPerLineFmt, used in conjunction with this function will merge them .
    Note- I only pull out ISSNLinking as that is what is used to connect to the IF database
    '''
    q='select p.pmid, p.DateCreated,p.ArticleTitle, p.JournalTitle,p.ISSN,p.ISSNLinking ,a.AbstractText, a.AbstractOrder from pubmed_pubmed as p inner join  pubmed_abstract as a on p.pmid = a.pmid_id where (p.DateCreated between %s and %s)'        
    mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
    startyear=int(startyear)
    for month in range(1,13,1):        
        fd=str(startyear)+'-'+str(month)+"-1"
        ld=str(startyear)+'-'+str(month)+"-31"
        data=psql.frame_query(q,params=(fd,ld),con=mysql_con)
        nrow,ncol=data.shape
        if nrow >5:  
            tt=(datetime.strptime(fd,"%Y-%m-%d")).strftime("%b")
            data.to_csv(tt+'_'+str(startyear)+'_data.csv',index=False)
    # Adding a connection close to every single clause to see if that fixes my connection problem!
    #mysql_con.commit()	
    #mysql_con.close()
    print "get_MysqlData -- connection closed!" #TPL     
def MysqlData_to_OneRecPerLineFmt(filename,newpath):
    '''
    function that takes a csv file containing all relevant data pulled out for selected pmids and merges/appends all the various paragraphs belonging to the same paper, so they are ready to be fed into the LDA machine. The pathname to store the new merged file is te other argument. 
    This function creates two files: one for LDA(For_LDA*) and one contained all the data(Uniq*)
    '''
    data=pd.read_csv(filename)
    nrow,ncol=data.shape
    if nrow >5:  
        uniq_pmids=pd.unique(data.pmid)
        data_df=data.drop_duplicates(cols=['pmid'])
        pmids_list=list(data.pmid)
        full_abs=[]
        #print "test1"
        for i in uniq_pmids:
            order_cnt=max(data.AbstractOrder[data.pmid==i])
            if order_cnt>1:
                #print "test1" 
                tmp_abs=''
                for k in range(1,order_cnt+1):
                    tmp_abs= tmp_abs+str(pd.unique(data.AbstractText[(data.pmid==i) & (data.AbstractOrder==k)])[0])   
                full_abs.append(tmp_abs)                    
            else:
                #print "test2"
                full_abs.append(data_df.AbstractText[data_df.pmid==i].values[0])
        data_df.drop(['AbstractText'],axis=1, inplace=True)  
        data_df.drop(['AbstractOrder'],axis=1,inplace=True)
        data_df['AbsText']=full_abs            
        data_df.to_csv(newpath+'/Uniq_'+filename,sep='\t',index=False)#write out the file in format to store
        #write out another file with just text for LDA.
        print "writing out " + filename +" to compute lda probabilities"
        data_df.to_csv(newpath+'/ForLDA_'+filename,sep='\t',index=False,cols=['pmid','ArticleTitle','AbsText'],header=False)
        print "done lda for", filename
        os.remove(filename)
   
def update_new_papers():
    '''
    This function should do the following:
         - update the papers in lda-data with papers downloaded into the Scireader database since the last download/yesterday? 
         - convert into files for LDA processing and Unique data
    '''    
    #download most recent data from Scireader
    curr_mon=datetime.now().month 
    curr_year=datetime.now().year
    path='/home/ubuntu/lda-data/'+str(curr_year)
    print path
    os.chdir(path)
    fd=str(curr_year)+'-'+str(curr_mon)+"-1"
    ld=str(curr_year)+'-'+str(curr_mon)+"-31"
    mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
    q='select p.pmid, p.DateCreated,p.ArticleTitle, p.JournalTitle,p.ISSN,p.ISSNLinking ,a.AbstractText, a.AbstractOrder from pubmed_pubmed as p inner join  pubmed_abstract as a on p.pmid = a.pmid_id where (p.DateCreated between %s and %s)'        
    data=psql.frame_query(q,params=(fd,ld),con=mysql_con)
    nrow,ncol=data.shape
    if nrow>5:  #dont bother saving if its a Month in the future or a month without much data        
        tt=(datetime.strptime(fd,"%Y-%m-%d")).strftime("%b")
        data.to_csv(tt+'_'+str(curr_year)+'_data.csv',index=False)
        # re-sort the file into format with one record per line as well as make the txt file for LDA    
        MysqlData_to_OneRecPerLineFmt(tt+'_'+str(curr_year)+'_data.csv',path)    
        filelist=glob.glob('ForLDA_'+tt+'*data.csv')
        print "filename is ", filelist 
        for file in filelist:
            if len(open(file).readlines())>5:   
                print "running LDA for ", file 
                subprocess.call(['/home/ubuntu/Scirec_scripts/lda_scripts/run_LDA.sh',file])
                shutil.copyfile('output_file_tpcs_composition_for_database.txt',file.split('csv')[0]+'LDA_composition_for_database.txt')
                shutil.copyfile('output_file_tpcs_composition_with_pmid.txt',file.split('csv')[0]+'LDA_composition_with_pmid.txt')
                os.remove('output_file_tpcs_composition.txt')
                os.remove('output_file_tpcs_composition_for_database.txt') 
                os.remove('output_file_tpcs_composition_with_pmid.txt') 
            else:
                print "file is short. It has length ", len(open(file).readlines())          
    else:
        print "file is short. It has length ", nrow 
    
    
def chk_if_allAbstractOrdersArePresent(filename):    
    data=pd.read_csv(filename)
    uniq_pmids=pd.unique(data.pmid)
    data_df=data.drop_duplicates(cols=['pmid'])
    pmids_list=list(data.pmid)
 #   full_abs=[]
    for i in uniq_pmids:
        order_cnt=max(data.AbstractOrder[data.pmid==i])
        if order_cnt>1:
            #print "test1" 
            tmp_abs=''
            for k in range(1,order_cnt+1):
                try:
                    tt=str(pd.unique(data.AbstractText[(data.pmid==i) & (data.AbstractOrder==k)])[0])   
                except IndexError:
                    failed_pmids.append(i)
                    break 
    print failed_pmids                
    return failed_pmids                                             

def update_corp(curr_basic_dataset_file,user_lib_pmids_list):
    '''
    function to remove all the pmids present in the users library from our sampling corpus so we dont recommend duplicate papers!!  
    Input: the sampling corpus , dataframe of users pmids
    '''
    corpus=pd.read_csv(curr_basic_dataset_file)
    updated_corpus_pmids_df=pd.DataFrame(list(set(list(corpus.pmid)) - set(user_lib_pmids_list)),columns=['pmid'])
    return pd.merge(corpus,updated_corpus_pmids_df,how="inner",on="pmid",sort=False)
    
    

def create_basic_papers_dataset_for_tpcRec(): 
    '''
    This function should find all the papers in the last 6 months (from lda-data) and merge their relevant info into a dataframe. The argument should be todays date.
    Note there are two cases: one when the 6 months straddle two consecutive years, and one where all the 6 months are in the same year.
    This one just takes care of the 2nd case.
    '''
    month_list=['Dummy','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
    today=datetime.now().date()
    fromdate=today-timedelta(180)
    basic_data_filelist=[]
    tpc_data_filelist=[]
    if today.year==fromdate.year:   
        curr_year=datetime.now().year
        path='/home/ubuntu/lda-data/'+str(curr_year)
        os.chdir(path) 
        basic_filelist=glob.glob('Uniq_*.csv')              
        basic_data=pd.read_csv(basic_filelist[0],usecols=[0,1,4,5], header=None, skiprows=1, sep='\t', names=['pmid','DateCreated','ISSN','ISSNLinking'])
        for file in basic_filelist[1:]:
            tmp_data=pd.read_csv(file,usecols=[0,1,4,5], header=None, skiprows=1, sep='\t', names=['pmid','DateCreated','ISSN','ISSNLinking'])
            basic_data=pd.concat([basic_data,tmp_data],ignore_index=True)
        #update the basic_data with the IF for the journals.    
        basic_data.ISSNLinking[basic_data.ISSNLinking.isnull()]=basic_data.ISSN[basic_data.ISSNLinking.isnull()]
        update_jour_IF(basic_data)
        if_data=pd.read_csv("/home/ubuntu/lda-data/Journal_IF_data.csv")        
        basic_data_with_IF=pd.merge(basic_data,if_data,how='left',left_on='ISSNLinking',right_on='ISSN',sort=False)           
        basic_data_with_IF.drop(['ISSN_x','ISSN_y'],axis=1,inplace=True)       
        basic_data_with_IF.drop_duplicates(cols='pmid',inplace=True)
        basic_data_with_IF.to_csv("/home/ubuntu/lda-data/Current_data/Current_basic_dataset_with_IF.csv",index=False)                      
        lda_filelist=glob.glob("*pmid*") 
        basic_lda_data=pd.read_csv(lda_filelist[0], header=None, sep='\t',usecols=[0,1,2,3,4,5,6], skiprows=1, names=['pmid','tpcno1','tpcprob1','tpcno2','tpcprob2','tpcno3','tpcprob3'])
        for file in lda_filelist[1:]:
            tmp_lda_data=pd.read_csv(file, header=None,sep='\t',usecols=[0,1,2,3,4,5,6], names=['pmid','tpcno1','tpcprob1','tpcno2','tpcprob2','tpcno3','tpcprob3'])
            basic_lda_data=pd.concat([basic_lda_data,tmp_lda_data],ignore_index=True)            
        basic_data_with_IF=basic_data_with_IF[basic_data_with_IF.DateCreated >= str(fromdate)]
        basic_data_with_IF.drop_duplicates(cols='pmid',inplace=True)
        basic_data_with_IF.to_csv("/home/ubuntu/lda-data/Current_data/Current_basic_LDA_dataset.csv",index=False)                       
        #The complete basic papers set is:    
        basic_all_data=pd.merge(basic_lda_data,basic_data_with_IF,how='left', left_on='pmid', right_on='pmid',sort=False) 
        #drop papers that are too old:
        basic_all_data=basic_all_data[basic_all_data.DateCreated >= str(fromdate)]
        basic_all_data.drop_duplicates(cols='pmid',inplace=True)
        basic_all_data.to_csv("/home/ubuntu/lda-data/Current_data/Current_basic_dataset_with_IF_and_LDA.csv",index=False)
        

def create_basic_papers_dataset_with_AllTpcProb():
    '''
    function to create the  basic dataset made up of pmids, topicc probability of papers from the last 6 months for calculating topic similarity with the users library papers. 
    This basically combining the "LDA_composition_for_database" files 
    '''
    today=datetime.now().date()
    fromdate=today-timedelta(180)
    basic_AllTpcProb_filelist=[]
    if today.year==fromdate.year:   
        curr_year=datetime.now().year
        path='/home/ubuntu/lda-data/'+str(curr_year)
        os.chdir(path) 
        basic_AllTpcProb_filelist=glob.glob('*for_database.txt')              
        basic_AllTpcProb_data=pd.read_csv(basic_AllTpcProb_filelist[0],header=None, sep='\t')
        for file in basic_AllTpcProb_filelist[1:]:
            tmp_data=pd.read_csv(file, header=None, sep='\t')
            basic_AllTpcProb_data=pd.concat([basic_AllTpcProb_data,tmp_data],ignore_index=True)
            
        basic_AllTpcProb_data.to_csv("/home/ubuntu/lda-data/Current_data/Current_AllTpcProb_data.csv",sep ='\t',index=False)

def create_basic_papers_dataset_for_WordSim():
    '''
    function to create the  basic dataset made up of pmids, titles, abstracts of papers from the last 6 months to use for calculating word similarity. Note that this is pretty much the 
    same thing as combining the ForLDA_* files from the last 6 months. 
    '''
    today=datetime.now().date()
    fromdate=today-timedelta(180)
    basic_filelist=[]
    if today.year==fromdate.year:   
        curr_year=datetime.now().year
        path='/home/ubuntu/lda-data/'+str(curr_year)
        os.chdir(path) 
        basic_filelist=glob.glob('Uniq_*.csv')              
        basic_WordSim_data=pd.read_csv(basic_filelist[0], usecols=[0,2,6],header=None, skiprows=1, sep='\t', names=['pmid','Title','Abstract'])
        for file in basic_filelist[1:]:
            tmp_data=pd.read_csv(file, header=None, usecols=[0,2,6],sep='\t', skiprows=1,names=['pmid','Title','Abstract'])
            basic_WordSim_data=pd.concat([basic_WordSim_data,tmp_data],ignore_index=True)
        basic_WordSim_data.drop_duplicates(cols='pmid',inplace=True)    
        basic_WordSim_data.to_csv("/home/ubuntu/lda-data/Current_data/Current_WordSim_data.csv",sep ='\t',index=False)

def filter_topically_similar_papers(user_tpcs_file,Current_AllTpcProb_file):   
    '''
    This function would take in two files of topic probabilities: Current_papers_tpc_prob.csv and user_paper_tpc_prob.csv and output a shorter list
    of sample papers that are most topically similar to the user's papers. Using cosine similarity between topic vectors to determine degree of similarity    
    '''
    #reading in the relevant foiles into dataframes
    samplePaper_tpcs_df=pd.read_csv(Current_AllTpcProb_file,sep='\t',header=None)  
    user_tpcs_df=pd.read_csv(user_tpcs_file,sep='\t',header=None)    
    #making sure there are no duplicates in either dataframe:
    samplePaper_tpcs_df.drop_duplicates(cols=[0],inplace=True)    
    samplePaper_tpcs_df.dropna(axis=1,how='any',inplace=True)   
    user_tpcs_df.drop_duplicates(cols=[0],inplace=True)  
    #pulling out pmids
    samplePaper_pmids=samplePaper_tpcs_df[0]
    user_pmids=user_tpcs_df[0]   
    #drop the columns containing pmids to prepare for calculating cosine similarity and converting to np arrays   
    samplePaper_tpcs_df.drop([0],axis=1,inplace=True)
    user_tpcs_df.drop([0],axis=1,inplace=True)    
    samplePaper_tpcs_array=np.array(samplePaper_tpcs_df)
    user_tpcs_array=np.array(user_tpcs_df)
    #importing all required modules
    from sklearn.metrics import pairwise_distances
    from scipy.spatial.distance import cosine
    cos_sim = 1- pairwise_distances(user_tpcs_array,samplePaper_tpcs_array, metric="cosine")    
    out=open('/home/ubuntu/Calculating_Rec_Scores/On_AWS/User_vs_Current_Tpc_sim.txt','w')
    topically_sim_papers_dict={}
    topically_sim_papers_pmids_list=[]
    for i in range(len(user_pmids)):
        topically_sim_papers_dict[user_pmids[i]]=[samplePaper_pmids[k] for k in (-cos_sim[i]).argsort()[0:100]] 
        topically_sim_papers_pmids_list=topically_sim_papers_pmids_list+[samplePaper_pmids[k] for k in (-cos_sim[i]).argsort()[0:100]]
        #print user_pmids[i],':',[samplePaper_pmids[k] for k in (-cos_sim[i]).argsort()[0:100]] 
        out.write(str(user_pmids[i])+': '+str([samplePaper_pmids[i] for k in (-cos_sim[i]).argsort()[0:100]]))
    out.close()
    return set(topically_sim_papers_pmids_list),user_pmids
    
def vocab_list_Similarity_calculator(topically_sim_papers_pmids_list,user_file,corpus,user_lib):
    #This function takes in  files containing the Current pmids/text and creates a word vocabulary list and converts each paper in the Current paper list 
    #into a word frequency vector. For each day; we will have ONE word vocabulary vector(??? Is this true?).Am creating the word sim vector using 
    #ONLY the topically similar paper list that I get from Natalie? This function will have as input: a list of the pmids of shortlisted papers from the current corpus, 
    #user_file (eg. ForLDA_1.txt) which has text info about the users papers (pmid,title, text). Natalie wants the output to be the similarity matrix. So output that.
    #**Output is a file with recommendations. Will use nltk to create the TFIDF vector.     
    
    #Note the Word_Sim_data file is the same for every user: it is '/home/ubuntu/lda-data/Current_data/Current_WordSim_data.csv'
    WordSim_data_file='/home/ubuntu/lda-data/Current_data/Current_WordSim_data.csv'
    WordSim_df=pd.read_csv(WordSim_data_file, sep='\t',skiprows=1,header=None,names=['pmid','title','abstract'])
    tmp_topically_sim_papers_df= pd.DataFrame(list(topically_sim_papers_pmids_list),columns=['pmid'])     
    topically_sim_papers_df=pd.merge(tmp_topically_sim_papers_df,WordSim_df,how='left',on='pmid',sort=False)  
    #topically_sim_papers_df['pmid'][topically_sim_papers_df['title'].isnull()])
    topically_sim_papers_df.dropna(subset=['pmid','title','abstract'],how='any', inplace=True)
    #Now to calculate similarities between abstracts of usr lib paper and pmids topically similar to it(from above)
    #First need to combine Titles and Abstracts:    
    topically_sim_papers_df['title_and_abs']= topically_sim_papers_df['title']+ topically_sim_papers_df['abstract']
    topically_sim_papers_pmids_list=list(topically_sim_papers_df['pmid'])
    topically_sim_papers_text=topically_sim_papers_df['title_and_abs']
    text_corpus = []
    for i, row in enumerate(topically_sim_papers_df.iterrow()):
	text_corpus.append(row['title_and_abs'] + " ".join(corpus[i].auths))
    # Now, make sure that you add all of the corpus' text as well ... 
    for paper in corpus:
        if paper.abstract != "" and paper.title != "\"title not found\"":
            text_corpus.append(paper.title + paper.abstract + " ".join(paper.auths))
    print "The current corpus length is", len(text_corpus)
    #print text_corpus
    #create stoplist:
    stp_list1=open('/home/ubuntu/Calculating_Rec_Scores/en.txt','r').read().split('\n')
    stoplist=[x.strip(' ') for x in (stp_list1)] 
    #using the tfidf Vectorizer
    from sklearn.feature_extraction.text import TfidfVectorizer
    import scipy as sp
    vectorizer=TfidfVectorizer(min_df=1, decode_error=u'ignore', stop_words=stoplist,strip_accents='ascii')
    X=vectorizer.fit_transform(str(line) for line in text_corpus)
    
    # User library information collation. WHy doesn't this work exactly how it does for the Wordsim_df? I'm not sure .... 
    user_file_df=pd.read_csv(user_file,sep='\t',header=None,names=['pmid','title','abstract'])
    user_pmids_list = pr.pmid_list(user_lib)
    tmp_user_df= pd.DataFrame([int(i) for i in user_pmids_list],columns=['pmid'])
    tmp_user = pd.merge(tmp_user_df,user_file_df,how='left',on='pmid',sort=False)
    tmp_user.dropna(subset=['pmid','title','abstract'],how='any', inplace=True)
    tmp_user['title_and_abs']=tmp_user['title']+tmp_user['abstract']
    user_file_text= []
    for i, row in enumerate(tmp_user.iterrow()):
        user_file_text.append(row['title_and_abs']+" ".join(lib[i].auths))
    user_corpus=list(user_file_text)
    print "The user library has the length", len(user_corpus) #let's just try this out-- nt
    user_paper_vec=vectorizer.transform(user_corpus)
    user_paper_vec.toarray()
    #vectorizer.get_feature_names()
    #since its a sparse matrix, cannot really use scipy distance metrics     
    from sklearn.metrics.pairwise import linear_kernel  
    num_samples,num_features=X.shape
    num_user_papers,num_features=user_paper_vec.shape
    Cos_dist_array=[[]for i in range(0,num_user_papers)]
    for j in range(0,num_user_papers):
         Cos_dist_array[j] = linear_kernel(user_paper_vec[j:j+1], X).flatten()
    return(np.asarray(Cos_dist_array))
    

def vocab_list_Similarity_calculator_test(topically_sim_papers_pmids_list,user_file):
    '''
    This function takes in  files containing the Current pmids/text and creates a word vocabulary list and converts each paper in the Current paper list 
    into a word frequency vector. For each day; we will have ONE word vocabulary vector(??? Is this true?).Am creating the word sim vector using 
    ONLY the topically similar paper list that I get from Natalie? This function will have as input: a list of the pmids of shortlisted papers from the current corpus, 
    user_file (eg. ForLDA_1.txt) which has text info about the users papers (pmid,title, text). Natalie wants the output to be the similarity matrix. So output that.
    **Output is a file with recommendations. Will use nltk to create the TFIDF vector.     
    ''' 
    #Note the Word_Sim_data file is the same for every user: it is '/home/ubuntu/lda-data/Current_data/Current_WordSim_data.csv'
    WordSim_data_file='/home/ubuntu/lda-data/Current_data/Current_WordSim_data.csv'
    WordSim_df=pd.read_csv(WordSim_data_file, sep='\t',skiprows=1,header=None,names=['pmid','title','abstract'])
    tmp_topically_sim_papers_df= pd.DataFrame(list(topically_sim_papers_pmids_list),columns=['pmid'])     
    topically_sim_papers_df=pd.merge(tmp_topically_sim_papers_df,WordSim_df,how='left',on='pmid',sort=False)  
    #topically_sim_papers_df['pmid'][topically_sim_papers_df['title'].isnull()])
    '''topically_sim_papers_df.dropna(subset=['pmid','title','abstract'],how='any', inplace=True)'''
    #Now to calculate similarities between abstracts of usr lib paper and pmids topically similar to it(from above)
    #First need to combine Titles and Abstracts:    
    topically_sim_papers_df['title_and_abs']= topically_sim_papers_df['title']+ topically_sim_papers_df['abstract']
    topically_sim_papers_pmids_list=list(topically_sim_papers_df['pmid'])
    topically_sim_papers_text=topically_sim_papers_df['title_and_abs']
    text_corpus=list(topically_sim_papers_text)
    #create stoplist:
    stp_list1=open('/home/ubuntu/Calculating_Rec_Scores/en.txt','r').read().split('\n')
    stoplist=[x.strip(' ') for x in (stp_list1)] 
    #using the tfidf Vectorizer
    from sklearn.feature_extraction.text import TfidfVectorizer
    import scipy as sp
    vectorizer=TfidfVectorizer(min_df=1, decode_error=u'ignore', stop_words=stoplist,strip_accents='ascii')
    X=vectorizer.fit_transform(str(line) for line in text_corpus)
    user_file_df=pd.read_csv(user_file,sep='\t',header=None,names=['pmid','title','abstract'])#
    #user_file_df=pd.read_csv(user_file,usecols=[0,2,6],sep='\t',header=None,skiprows=1,names=['pmid','title','abstract'])# check to make sure of the columns.
    user_file_df['title_and_abs']=user_file_df['title']+user_file_df['abstract']
    user_pmids_list=list(user_file_df['pmid'])
    user_file_text=user_file_df['title_and_abs']
    user_corpus=list(user_file_text)
    user_paper_vec=vectorizer.transform(user_corpus)
    user_paper_vec.toarray()
    #vectorizer.get_feature_names()
    #since its a sparse matrix, cannot really use scipy distance metrics     
    from sklearn.metrics.pairwise import linear_kernel  
    num_samples,num_features=X.shape
    num_user_papers,num_features=user_paper_vec.shape
    Cos_dist_array=[[]for i in range(0,num_user_papers)]
    for j in range(0,num_user_papers):
         Cos_dist_array[j] = linear_kernel(user_paper_vec[j:j+1], X).flatten()
    return(np.asarray(Cos_dist_array))
    



'''    
    ##************Perhaps a faster method:
    from sklearn.feature_extraction.text import HashingVectorizer
    hv = HashingVectorizer(decode_error='ignore',stop_words=stoplist,strip_accents='ascii')
   # hv=hv.fit_transform(text_corpus)
    hv=hv.fit_transform(str(line) for line in text_corpus)
    user_file_df=pd.read_csv(user_file,sep='\t',header=None,names=['pmid','title','abstract'])#
    #user_file_df=pd.read_csv(user_file,usecols=[0,2,6],sep='\t',header=None,skiprows=1,names=['pmid','title','abstract'])# check to make sure of the columns.
    user_file_df['title_and_abs']=user_file_df['title']+user_file_df['abstract']
    user_pmids_list=list(user_file_df['pmid'])
    user_file_text=user_file_df['title_and_abs']
    user_corpus=list(user_file_text)
    user_paper_vec=hv.transform(user_corpus)
    from sklearn.metrics.pairwise import linear_kernel             
    num_samples,num_features=X.shape
    num_user_papers,num_features=user_paper_vec.shape
    Cos_dist_array=[[]for i in range(0,num_user_papers)]
    for j in range(0,num_user_papers):
         Cos_dist_array[j] = linear_kernel(user_paper_vec[j:j+1], hv).flatten()
    aa=np.asmatrix(Cos_dist_array)
    
    ####older method:  deprecate now
    from scipy.spatial.distance import cosine
        def Cos_dist_norm(v1,v2):
        return cosine(v1.toarray(),v2.toarray())
    num_samples,num_features=X.shape
    num_user_papers,num_features=user_paper_vec.shape
    Cos_dist_array=[[]for i in range(0,num_user_papers)]
    for j in range(0,num_user_papers):
        Cos_dist_array[j]=[]
        for i in range(0,num_samples):
            Cos_dist_array[j].append(Cos_dist_norm(user_paper_vec[j],X.getrow(i)))
    aa=np.asmatrix(Cos_dist_array)
    

  ##  method to write out the date,titles and abs text
    
    a=np.asarray(Cos_dist_array)
    np.savetxt(user_name+'sim_matrix.txt', a, delimiter=" ") 
    np.sort(a, axis=None)
    idx = np.argsort(a, axis=None)
    rows, cols = np.unravel_index(idx, a.shape)
    a_sorted = a[rows, cols]
    out=open('Similar_papers_for'+user_name,'w')   
    for r, c, v in zip(rows, cols, a_sorted):
         outstr=str(topically_sim_papers_df.pmid[c])+'\t'+topically_sim_papers_df.title[c]+topically_sim_papers_df.abstract[c]+'\n'
        # print outstr
         out.write(outstr)
    out.close()
    '''
    
def create_basic_jour_IF():
    '''
    This converts all the published Journal IF data into a table.Turns out some of the Journal Data has duplicates: for those cases, I 
    pick the one with max IF factor, and drop the others. IF of journal with IF=NULL or 0 is converted to 1. eLife is given an IF =15. 
    ''' 
    mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
    #Can I do this in MySQL
    ##?r="select distinct(j1.ISSN), j1.IF  from journals_impactfactor as j1,journals_impactfactor as j2 where ( j1.ISSN=j2.ISSN and j1.IF>j2.IF) " 
    #Do it in pandas -more straightforward
    r="select journals_impactfactor.ISSN,journals_impactfactor.IF  from journals_impactfactor"
    if_jourdata=psql.frame_query(r,mysql_con)
    aa=if_jourdata.set_index('ISSN').index.get_duplicates()
    for ISSN in aa:
        bb=if_jourdata.IF[if_jourdata.ISSN == ISSN]
        if_jourdata.IF[if_jourdata.ISSN == ISSN]=max(bb)
    #Remove the duplicate rows.
    if_jourdata.drop_duplicates(cols='ISSN',inplace=True)  
    #There are journals with IF=0.0,-convert the IF of these journals into 1.
    if_jourdata.IF[if_jourdata.IF==0.0]=1.00
    #This file contains all the journals that are in the published list
    if_jourdata.to_csv("/home/ubuntu/lda-data/Journal_IF_data.csv",index=False)
    
def update_jour_IF(dataframe):      
    '''
    This function reads in the IF file from "/home/ubuntu/lda-data/Journal_IF_data.csv" and then updates it with any new journals that are in the current dataset that 
    are NOT in the IF file, with an IF =1.0. If journal is eLife give if IF=15    
    '''
    if_jourdata= pd.read_csv("/home/ubuntu/lda-data/Journal_IF_data.csv")
    ISSN_uniq=set(pd.unique(dataframe.ISSNLinking))
    new_ISSN=ISSN_uniq-set(if_jourdata.ISSN)
    tmp_df=pd.DataFrame(list(new_ISSN)) 
    tmp_df.columns=['ISSN']
    tmp_df.dropna(inplace=True)
    tmp_df['IF']=1.00
    if len(tmp_df.ISSN)>0:
              tmp_df.IF[tmp_df.ISSN=='2050-084X']=15.00
    update_ifdata=pd.concat([if_jourdata,tmp_df],ignore_index=True) #Note: concat is a better option than merge as they are disjoint sets
    update_ifdata.to_csv("/home/ubuntu/lda-data/Journal_IF_data.csv", index=False)
    
def getting_users_pmids(bibtex):
    bp = BibTexParser(open(bibtex,'r').read())
    dicts = bp.get_entry_dict()
    pmids=[]
    for key in dicts.keys():
        try:
		    pmids.append((dicts[key]["pmid"]).encode('utf-8'))
        except:
	    	pmids.append(0)	    	
    return pmids

def getting_usrpubRecs_andforLDA(pmids,user_name,pathname):
    q='select p.pmid, p.DateCreated,p.ArticleTitle, p.JournalTitle,p.ISSN,p.ISSNLinking ,a.AbstractText, a.AbstractOrder from pubmed_pubmed as p inner join pubmed_abstract as a on p.pmid = a.pmid_id where p.pmid in (%s)'        
    mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
    str_list=','.join(['%s']*len(pmids))
    q=q % str_list
    user_data=psql.frame_query(q,params = pmids,con=mysql_con)
    #### Can we get this stupid mysqldb has gone away error to ... go away?
    #mysql_con.commit() 
    #mysql_con.close() # connection closes
    print "getting_userpubrecs -- mysql_con closed!" #TPL 
    nrow,ncol=user_data.shape 
    uniq_pmids=pd.unique(user_data.pmid)    
    user_data_df=user_data.drop_duplicates(cols=['pmid'])
    pmids_list=list(user_data.pmid)
    full_abs=[]
    count=0
    for i in uniq_pmids:
        order_cnt=max(user_data.AbstractOrder[user_data.pmid==i])
        if order_cnt>1:
            tmp_abs=''
            for k in range(1,order_cnt+1):
                tmp_abs=tmp_abs+str(pd.unique(user_data.AbstractText[(user_data.pmid==i) & (user_data.AbstractOrder==k)])[0])   
            full_abs.append(tmp_abs) 
        else:
            full_abs.append(user_data_df.AbstractText[user_data_df.pmid==i].values[0])
    user_data_df.drop(['AbstractText'],axis=1, inplace=True)  
    user_data_df.drop(['AbstractOrder'],axis=1,inplace=True)
    user_data_df['AbsText']=full_abs
    pmids_df=pd.DataFrame(pmids,columns=['pmid'])
    new_user_data=pd.merge(pmids_df, user_data_df, how="left",on="pmid",sort=False)                       
    user_data_df.to_csv(pathname+'/Uniq_'+user_name+'.txt',sep='\t',index=False)#write out the file in format to store
    #write out info for each paper into a pubRec.
    lib=[]
    for index, row in  user_data_df.iterrows():   
        paper=pr.pubRecords(p=row['pmid'],t=row['ArticleTitle'],d=row['DateCreated'],abst=row['AbsText'],i=row['ISSN'],il=row['ISSNLinking'])
        #paper.pmid=row['pmid']
        #paper.date=row['DateCreated']
        #paper.title=row['ArticleTitle']
        #paper.issn=row['ISSN']
        #paper.issnl=row['ISSNLinking']
        #paper.abstract=row['AbsText'] 
        lib.append(paper)
    #write out another file with just text for LDA.
    fields = ",".join(["PMID_id","ForeName","LastName","Initials"])
    pmids = ",".join([str(i) for i in pr.pmid_list(lib)])
    # Create your query and evaluate it
    q= "SELECT "+fields+" FROM pubmed_author WHERE PMID_id IN ("+pmids+");"
    mysql_con = MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
    c=mysql_con.cursor()
    c.execute(q)
    # Obtain your queried information
    data = c.fetchall()
    # Can you close this idiotic connection?!?!?! Please say YES!
    #mysql_con.commit() # Trying to close this connection .... 
    #mysql_con.close()
    #c.close()
# For every paper in the corpus, assign the information. 
    for i in range(len(data)):
         for p in lib: # For every paper
         # If the PMID matches, add the author
             if p.pmid == data[i][0]:
                 p.auths.append("".join(data[i][1:3]))
    print "writing out " + user_name +" data  to compute lda probabilities"
    user_data_df.to_csv(pathname+'/ForLDA_'+user_name+'.txt',sep='\t',index=False,cols=['pmid','ArticleTitle','AbsText'],header=False)
    print "done lda for ", user_name     
    return lib,list(user_data_df.pmid)    
       

def getting_users_papers_for_lda(pmids,user_name,pathname):   
    q='select p.pmid, p.DateCreated,p.ArticleTitle, p.JournalTitle,p.ISSN,p.ISSNLinking ,a.AbstractText, a.AbstractOrder from pubmed_pubmed as p inner join pubmed_abstract as a on p.pmid = a.pmid_id where p.pmid in (%s)'        
    mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
    str_list=','.join(['%s']*len(pmids))
    q=q % str_list
    user_data=psql.frame_query(q,params = pmids,con=mysql_con)
    # Let's close the connection, will that fix things?
    #mysql_con.commit()
    #mysql_con.close() # Let me try closing this ....
    print "getting_users_papers_for_lda connection closed!" #TPL
    nrow,ncol=user_data.shape 
    uniq_pmids=pd.unique(user_data.pmid)
    user_data_df=user_data.drop_duplicates(cols=['pmid'])
    pmids_list=list(user_data.pmid)
    full_abs=[]
    count=0
    for i in uniq_pmids:
        order_cnt=max(user_data.AbstractOrder[user_data.pmid==i])
        if order_cnt>1:
            tmp_abs=''
            for k in range(1,order_cnt+1):
                tmp_abs=tmp_abs+str(pd.unique(user_data.AbstractText[(user_data.pmid==i) & (user_data.AbstractOrder==k)])[0])   
            full_abs.append(tmp_abs) 
        else:
            full_abs.append(user_data_df.AbstractText[user_data_df.pmid==i].values[0])
    user_data_df.drop(['AbstractText'],axis=1, inplace=True)  
    user_data_df.drop(['AbstractOrder'],axis=1,inplace=True)
    user_data_df['AbsText']=full_abs            
    user_data_df.to_csv(pathname+'/Uniq_'+user_name+'.txt',sep='\t',index=False)#write out the file in format to store
    #write out another file with just text for LDA.
    print "writing out " + user_name +" data  to compute lda probabilities"
    user_data_df.to_csv(pathname+'/ForLDA_'+user_name+'.txt',sep='\t',index=False,cols=['pmid','ArticleTitle','AbsText'],header=False)
    print "done lda for ", user_name
    

def calculateRunTime(function, *args):
    import time
    '''run a function and return the run time and the result of the function
     if the function requires arguments, those can be passed in too '''
    startTime = time.time()
    result = function(*args)
    return result,time.time() - startTime,
    
    
'''
#### Possible alternative for time consuming table join in above function?

    q_pubmed='select pmid, DateCreated,ArticleTitle, JournalTitle,ISSN,ISSNLinking from pubmed_pubmed where pmid in (%s)'        
    q_pubmed=q_pubmed % str_list
    user_pubmed_data=psql.frame_query(q_pubmed,params = pmids,con=mysql_con)
    q_abstract='select pmid_id, AbstractText,AbstractOrder from pubmed_abstract where pmid_id in (%s)'
    q_abstract=q_abstract % str_list
    user_abstract_data=psql.frame_query(q_abstract,params = pmids,con=mysql_con)
    user_data=pd.merge(user_pubmed_data,user_abstract_data,left_on="pmid",right_on="pmid_id",sort=False)
'''

def run_lda_user(file,user_name):
    subprocess.call(['/home/ubuntu/Scirec_scripts/lda_scripts/run_LDA.sh',file])
    shutil.move('output_file_tpcs_composition_for_database.txt',user_name+'LDA_composition_for_database.txt')
    shutil.move('output_file_tpcs_composition_with_pmid.txt',user_name+'LDA_composition_with_pmid.txt')                
