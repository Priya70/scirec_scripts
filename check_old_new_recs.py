import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np


mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
q='select * from library_library_papers'
data=psql.frame_query(q,con=mysql_con)
lib_list=list(pd.unique(data.libid_id))

newrecs=pd.read_csv('/home/ubuntu/Personalized_Recs/cronjob_testing/10_22_14_recs.txt',sep='\t',header=None, names=['ind','usrid','libid','dbid','pmid'] )
oldrecs=pd.read_csv('/home/ubuntu/Personalized_Recs/cronjob_testing/new/10_22_14_recs.txt',sep='\t',header=None, names=['ind','usrid','libid','dbid','pmid'])
overlap_list=[]
for lib in lib_list:
    new_lib=newrecs.pmid[newrecs.libid == lib]
    old_lib=oldrecs.pmid[oldrecs.libid == lib]
    over=set(new_lib).intersection(set(old_lib))
    overlap_list.append(len(over)/500.)

plot(lib_list,overlap_list,'ro')    