#!/usr/bin/env python
### Composite pipeline for regular recommendation updates. 
## pipeline edited for TEN FOLD VALIDATION scheme, edits marked '10'
##################
##### Imports
##################

## this import section lifted directly from Priya's code -- unsure of what dependencies her functions have so I am taking it all just in case 
'''import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
from datetime import datetime,timedelta 
#import h5py
import subprocess
import os
from dateutil import rrule
import glob
import shutil
import numpy as np
import itertools
from bibtexparser.bparser import BibTexParser'''
import numpy as np
import os
## additional imports from me. 
import argparse
import pubRecords_1 as pr # my module
#import natalie_modified_Priya_module as cpr # Priya's module with my edits
import natalies_modified_working_Priya_module as cpr
#for the 10fold
import random
##################
##### Argument processing and set-up
##################

#### Argument parsing
parser = argparse.ArgumentParser() ## Create an argument parser

# Arguments...
parser.add_argument('--u', help = "user ID") # user ID
parser.add_argument('--l', help = "library ID") # library ID
parser.add_argument('--pf', help = "PMID file list") # a file with PMIDs in it
parser.add_argument('--cf', help = "PMID file for corpus") # a file with PMIDs in it
parser.add_argument('--sm', help = "simmat file")
parser.add_argument('--ct', help = "corpus text")
parser.add_argument('--pt', help = "library text")
# Now parse everything - these lines are required to create a list of args, l_args, to read
args = parser.parse_args()
l_args = vars(args) # Dictionary with argument names -> values 
arglist = l_args.values() # Values of all the arguments, in order
# Assign to variables
u_id = l_args["u"] #user ID
lib_id = l_args["l"] #library ID
lib_pubfile = l_args["pf"] #filename with PMID list stored within
corp_pubfile = l_args["cf"] #filename with PMID list for corpus stored within
simmatfile = l_args["sm"]
user_file = l_args["pt"]
corp_file = l_args["ct"]

##################
#####  MAIN  #####
##################

#########
##### File processing 
#########
lib_publist = np.loadtxt(lib_pubfile)
print lib_publist
print len(lib_publist)
date = pr.todays_date()
outpath='/home/ubuntu/Personalized_Recs/cronjob_testing/Vocab_testing/'+str(lib_id)
lib_cut,lib_publist =cpr.getting_usrpubRecs_andforLDA([int(i) for i in lib_publist],str(lib_id),outpath)
tmp_corp = pr.get_corp_info(corp_pubfile,delim=",")
corp_cut=[paper for paper in tmp_corp if paper.pmid not in lib_publist]

#########
##### Recs
#########
##10 inferencing moved here, so it's only done once....
#cpr.run_lda_user(outpath+"/ForLDA_"+str(u_id)+".txt",u_id) # Run topic inferencer

#Calculate word similarity and return the similarity matrix
simmat = cpr.DYNAMIC_vocab_list_Similarity_calculator(user_file,corp_file,[i for i in lib_publist])
simfile = "/home/ubuntu/Natalie/HHMIFigures/wordsim_matrix_notsquare.txt"
s = open(simfile,'w')
s.write("\t"+"\t".join([str(i) for i in lib_publist])+"\n")
for i in range(len(simmat[:,0])):
        to_write = ""
        for j in range(len(simmat[i,:])):
                to_write += "\t"+str(simmat[i,j])
        s.write(to_write+"\n")
s.close()
#simmat = np.loadtxt(simmatfile)
#simmat = simmat[:,0:1639]
#simmat = np.flipud(np.flipud(np.loadtxt(simmatfile)).T)
#print "word_sim done for lib ", lib_id
corp_score_list = pr.word_sim_combined_score(simmat,lib_cut,corp_cut,alpha=1000,dbgvar=0)
#print "corp_score_list has length ", len(corp_score_list)
# Now do some metadata weighting
print "The length of the corpus is ", len(corp_cut), "and the length of the score list is", len(corp_score_list)
#corp_score_list_meta = pr.metadata_weighter(corp_cut,corp_score_list,dbgvar=1,use=[1,1,1])
corp_score_list_meta = pr.metadata_weighter_tweet_IF_interchange(corp_cut,corp_score_list,dbgvar=1,use=[1,1,1])
#print "corp_score_list has length ", len(corp_score_list)
file = "scores_aggreg.txt"
best_recs = pr.choose_best_from_corp(corp_cut,corp_score_list_meta)

o = open(file,'w')
for i in range(len(corp_score_list)):
	o.write(str(corp_cut[i].pmid)+"\t"+str(corp_score_list[i])+"\t"+str(corp_score_list_meta[i])+"\n")
o.close()

file = "bestpmids.txt"
o = open(file,'w')
for r in best_recs:
	o.write(str(r.pmid)+"\n")
o.close()

#file = "metadata_scores.txt"
#o = open(file,'w')
#for s in corp_score_list:
#	o.write(str(s)+"\n")
#o.close()

#o = open("pmids_of_best.txt",'w')
#for rec in best_recs:
#	o.write(str(rec.pmid)+"\n")
#o.close()

#########
##### Output
#########

### The only thing to output is the actual wordsim matrix...
# 1. Choose the best papers by their scores; we assume the corp_score_list is in the same order as the corpus
#best_recs,best_scores = pr.modified_choose_best_from_corp(corp_cut,corp_score_list,reclen=500)
#best_scores = pr.metadata_weighter(best_recs,best_scores)
#best_recs,best_scores = pr.modified_choose_best_from_corp(corp_cut,corp_score_list,reclen=len(best_recs))
# 2. Output them into Yonggan's format
### Output the test library as well as the test corpus
#pr.modified_output_rec_by_lib(best_recs,[str(1) for i in range(len(best_recs))],libname=lib_id,user=u_id,date = "not-a-real-date")
