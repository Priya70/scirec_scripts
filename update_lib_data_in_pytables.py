#! /user/bin/python

'''
cronjob to create libdata in pytables for all libraries daily.
'''
import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
import os
import glob
import shutil
import numpy as np
import itertools
from bibtexparser.bparser import BibTexParser
import pubRecords_1 as pr
import subprocess
import cPickle as pickle
from boto.s3.connection import S3Connection
from boto.s3.key import Key
import boto
## additional imports from me. 
import argparse
import pubRecords_1 as pr # natalie's module
import Data_processing_scripts_WORKING as cpr # priya's module

## comment this function out as you only need to run it once!
cpr.create_pytables_for_all_libraries()