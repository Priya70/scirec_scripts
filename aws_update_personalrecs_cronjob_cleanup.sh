#!/bin/bash
#shell script to upload all new data to s3 and stop the instance
cd
export EC2_URL=http://ec2.us-west-1.amazonaws.com
aws s3  sync lda-data/ s3://lda-data 
aws s3  sync Scirec_scripts s3://scirec-scripts 
aws s3  sync Final_Tpc_Recs/new/ s3://scireader-data/topics/new/ --delete
aws s3  sync Final_Tpc_Recs/loaded/ s3://scireader-data/topics/loaded/
aws s3  sync Personalized_Recs/cronjob_testing/new/  s3://scireader-data/recommendations/new/
aws s3  sync Personalized_Recs/cronjob_testing/loaded/  s3://scireader-data/recommendations/loaded/

