#! /usr/bin/local/python
import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
from datetime import datetime 
import h5py
import mysql.connector

jp_txt_data=pd.read_csv('JP_allData.csv',sep='\t')
jp_tpcs=pd.read_csv('/data/priya/Topic_modelling/mallet-2.0.7/for_database/Calculating_Rec_Scores/JP_test_set/JP_papers_LDA_prob.txt', sep='\t',header=None)
#to drop the duplicates: do.
jp_tpcs.drop_duplicates(cols=[0],take_last=True, inplace=True)
jp_tpcs_pmid=jp_tpcs[0]
jp_pmid_list=list(jp_tpcs_pmid)
jp_tpcs.drop([0],axis=1,inplace=True)
jp_tpcs_data_array=np.array(jp_tpcs)
from sklearn.metrics import pairwise_distances
from scipy.spatial.distance import cosine
from hcluster import pdist, linkage, complete, dendrogram
from scipy.linalg import norm
from scipy.spatial.distance import *
import scipy.cluster.hierarchy
import scipy
from hcluster import pdist, linkage, complete, dendrogram
def kl(p, q):
        """Kullback-Leibler divergence D(P || Q) for discrete distributions
        Parameters
        ----------
        p, q : array-like, dtype=float, shape=n
            Discrete probability distributions.    """
        p = np.asarray(p, dtype=np.float)
        q = np.asarray(q, dtype=np.float)
        return np.sum(np.where(p != 0, p * np.log(p / q), 0))
#Jenson Shannon divergence between two vectors p,q
def myjsd(p,q):  
        m=0.5*(p+q)
        return(0.5*(kl(p,m)+kl(q,m)))
#to get matrix for pairwise jsd for an array of vectors
def jsd_dist(jp_tpcs_data_array,test_arr):
        jsd_arr=np.zeros((len(jp_tpcs_data_array),len(test_arr)))    
        for i in range(len(jp_tpcs_data_array)):
            for j in range(len(test_arr)):
                jsd_arr[i,j]=myjsd(jp_tpcs_data_array[i],test_arr[j])            
        return jsd_arr

cos_sim = 1- pairwise_distances(jp_tpcs_data_array,jp_tpcs_data_array, metric="cosine")
eucl_sim = 1- pairwise_distances(jp_tpcs_data_array,jp_tpcs_data_array, metric="euclidean")
jsd_sim= 1-jsd_dist(jp_tpcs_data_array,jp_tpcs_data_array)


out=open('JP_vs_JP_Tpc_sim.txt','w')
for i in range(len(jp_pmid_list)): 
   #to get the list of the similar scores in descending order
   # A=cos_sim[i][(-cos_sim[i]).argsort()] 
   #to get the pmids of the papers with most similar scores in descending order
   # A=(-cos_sim[i]).argsort()
   # B=[jp_pmid_list[i] for i in A] 
    print 'Using cos_sim',jp_pmid_list[i],':',[jp_pmid_list[k] for k in (-cos_sim[i]).argsort()]
    print 'Using cos_sim',jp_pmid_list[i],':',[cos_sim[i][k] for k in (-cos_sim[i]).argsort()]    
    print 'Using Eucl_sim',jp_pmid_list[i],':',[jp_pmid_list[k] for k in (-eucl_sim[i]).argsort()]
    print 'Using Eucl_sim',jp_pmid_list[i],':',[eucl_sim[i][k] for k in (-eucl_sim[i]).argsort()]
    print 'Using JS_sim',jp_pmid_list[i],':',[jp_pmid_list[k] for k in (-jsd_sim[i]).argsort()]
    print 'Using JS_sim',jp_pmid_list[i],':',[jsd_sim[i][k] for k in (-jsd_sim[i]).argsort()]
 
 #   out.write('Using Cos_sim '+str(jp_pmid_list[i])+': '+str([jp_pmid_list[i] for i in (-cos_sim[i]).argsort()]))
 #   out.write('Using Eucl_sim '+str(jp_pmid_list[i])+': '+str([jp_pmid_list[i] for i in (-eucl_sim[i]).argsort()]))
 #   out.write('Using JSD_sim '+str(jp_pmid_list[i])+': '+str([jp_pmid_list[i] for i in (-jsd_sim[i]).argsort()]))
out.close() 

***Article similarity based on word/word frequency using L2 norm (euclidean distance)****

jp_txt=pd.read_csv('Jp_allData.csv',sep='\t')
jp_pmid_df=pd.DataFrame(jp_pmid_list,columns=['pmid'])
new=pd.merge(jp_pmid_df,jp_txt,how='left',left_on='pmid',right_on='pmid',sort=False)
new.to_csv('JP_reference_key.txt',sep='\t',header=True)

jp_txt['all_txt']=jp_txt['ArticleTitle']+jp_txt['AbsText']
text_corpus=list(jp_txt.all_txt)
text_pmid_list=list(jp_txt.pmid)

stp_list1=open('/data/priya/Topic_modelling/mallet-2.0.7/stoplists/en.txt','r').read().split('\n')
stoplist=[x.strip(' ') for x in (stp_list1)] 
from sklearn.feature_extraction.text import CountVectorizer
import scipy as sp
vectorizer=CountVectorizer(min_df=1, decode_error=u'ignore', stop_words=stoplist,strip_accents='ascii')
X=vectorizer.fit_transform(str(line) for line in text_corpus)
X.toarray()
len(X.toarray())
vectorizer.get_feature_names() #lot of random words?? ...but regular words further down the list. Fewer random words Gensim version??)
num_samples,num_features=X.shape


def dist_norm(v1,v2):
   v1_norm=v1/sp.linalg.norm(v1.toarray())
   v2_norm=v2/sp.linalg.norm(v2.toarray())
   delta=v1_norm-v2_norm
   return sp.linalg.norm(delta.toarray())
   
import sys
for pmid in text_pmid_list: 
    new_paper=list(jp_txt.all_txt[jp_txt.pmid ==pmid])
    new_paper_vec=vectorizer.transform(new_paper)  
    for i in range(0,num_samples):
        post=text_corpus[i]
        if post == new_paper:
           continue
        post_vec=X.getrow(i)
        d=dist_norm(post_vec,new_paper_vec)            
    dist_list=[]              
    for i in range(0,num_samples):
        dist_list.append(dist_norm(new_paper_vec,X.getrow(i)))  
    from operator import itemgetter
    indices, dist_sorted = zip(*sorted(enumerate(dist_list), key=itemgetter(1)))
    out=open('papers_similar_to_'+str(pmid)+'_using_l2_norm.txt','w')
    out.write('Pmid     l2_norm     title\n')
    for i in indices[0:35]:
        out.write(str(text_pmid_list[i])+'\t'+str(dist_list[i])+'\t'+ jp_txt['ArticleTitle'][i]+'\n')
    out.close()   
    print 'Using l2 norm papers similar to ',pmid,':',[text_pmid_list[i] for  i in indices[0:35]]

    
***Article similarity based on word/word frequency using tf-idf+cosine distance)****
      
from sklearn.feature_extraction.text import TfidfVectorizer
new_vectorizer=TfidfVectorizer(min_df=1, decode_error=u'ignore', stop_words=stoplist,strip_accents='ascii')
X=new_vectorizer.fit_transform(str(line) for line in text_corpus)
new_vectorizer.get_feature_names()
from sklearn.metrics import pairwise_distances
from scipy.spatial.distance import cosine
##I dont want a matrix- I am comparing one abstract against the other 5000

def Cos_dist_norm(v1,v2):
   return cosine(v1.toarray(),v2.toarray())
   
for pmid in text_pmid_list: 
    new_paper=list(jp_txt.all_txt[jp_txt.pmid ==pmid])
    new_paper_vec=new_vectorizer.transform(new_paper)     
    Cos_dist_list=[]
    for i in range(0,num_samples):
        Cos_dist_list.append(Cos_dist_norm(new_paper_vec,X.getrow(i)))     
    cos_indices,cos_dist_sorted = zip(*sorted(enumerate(Cos_dist_list), key=itemgetter(1)))
    out=open('papers_similar_to_'+str(pmid)+'_using_Cosine_TfIdf_norm.txt','w')
    out.write('Pmid    Cosine_dist     title\n')
    for i in cos_indices[0:35]:
        out.write(str(text_pmid_list[i])+'\t'+str(Cos_dist_list[i])+'\t'+ jp_txt['ArticleTitle'][i]+'\n') 
    out.close()   
    print 'Using Cosine/TfIdf norm papers similar to ',pmid,':',[text_pmid_list[i] for  i in cos_indices[0:35]]
   
***Article similarity based on word/word frequency using JSD distance)****
vectorizer=CountVectorizer(min_df=1, decode_error=u'ignore', stop_words=stoplist,strip_accents='ascii')
X=vectorizer.fit_transform(str(line) for line in text_corpus)

