#!/usr/bin/local/python
# usage: create_tpc_vector  'wrd_tpcall_20140221093827_150_9603.txt ' 150 
# usage : create_tpc_vector  infile  num_tpcs 
#  infile, n_tpcs='wrd_tpcall_20140221093827_150_9603.txt' ,'150' 

import sys
import pandas as pd
import numpy as np

def create_tp_vector(infile,n_tpcs):    
    n_tpcs=int(n_tpcs)
    data=open(infile,'r').readlines()
    tpcs=[[0]*len(data) for x in xrange(n_tpcs)]  
    word_vocab=[]
    ct=0
    for line in data:
        word_vocab.append(line.split()[1])   
        aa=line.split()[2:]
        for tt in aa:
            tpcs[int(tt.split(':')[0])][ct]=int(tt.split(':')[1])            
        ct=ct+1   
    tpcs.insert(0,word_vocab)      
    tpcs_df=pd.DataFrame(tpcs)           
    tpcs_df=tpcs_df.T
    return tpcs_df
    
tpcs_df=create_tp_vector('wrd_tpcall_20140221093827_150_9603.txt' ,'150')
        df=pd.DataFrame([['NA','NA','NA']],columns=['topicID','word','wrd_prob'])
    for i in range(150):    
        print "Topic # ",i    
        test=pd.DataFrame(tpcs_df,columns=[0,i+1])
        test.columns=['word','wrd_prob']
        test['wrd_prob']=(test['wrd_prob']/tmpsum)
        tmp=test.sort('wrd_prob',ascending=False).head(50)
        tmp['topicID']=np.zeros(50)+i
        df=pd.concat([df,tmp])
    #    tmp.index=list(xrange(50)+i*50+i)
    df.index =list(xrange(7501))
    df=df[df.word != 'NA']  #dont use df.index == 0, as there are multiple tpcs where 0th wrd has high prob.
    df.to_csv('For_Wordle.txt',sep='\t')
       
        
   
   