#!/usr/bin/env python
__all__ = ['recMerge']

############
## This package contains all of the different merge techniques for 
## creating the merged recommendation list. 
############

###########
## Imports
###########

# Minimal dependencies: MySQLdb, bibtexparser
import MySQLdb
import numpy as np # I love numpy, and it loads files so quickly when they're all-numerical!
import sys
import pubRecordsProduction_WORKING as pr # Use pubRecords for handling the papers
import random # For random sampling from rec lists...
######################
## Merger Functions ##
######################

# The basic merger simply interleaves the various libraries
# The default length of a list is all the paper sin all the libraries
# Pass that as an argument in order to get smaller lists. 
def basic(libs,reclen=0): # If the rec len is 0 or otherwise untenable we assign it more appropriately
	#Calculate total number of recommended papers in order to ensure that reclen is appropriately set
	total = 0
	for lib in libs.keys():
		total += len(libs[lib])

	# If the recommendation length is not set appropriately
	if reclen == None or reclen == 0 or reclen > total: # If reclen sucks
		reclen = 0 # Re-define it as zero
		for lib in libs.keys(): # then create it as the sum length of all the papers in the recommendation lists
			if len(libs[lib]) > 0:
				reclen += len(libs[lib]) # Then define reclen appropriately

	# Now, assemble the personal recommendations aggregate
	recs = dict() # Papers to rankings - necessary to ensure no paper is recommended twice
	lib_index = 0 # Start from the first library
	paper_index = 0 # Start from the first paper in each library
	counter = 1 # start at rank 1
	# Until you've recommended enough, interleave recs from each library
	while len(recs) < reclen: # append more lists from each of the libraries
		#print lib_index, paper_index
		if len(libs[libs.keys()[lib_index]]) > 0:
			try:
				recs[libs[libs.keys()[lib_index]][paper_index]] + 5 # just a simple test -- does this paper have an existing rank?
			except:
				recs[libs[libs.keys()[lib_index]][paper_index]] = counter # set the paper to the current ranking
		lib_index = (lib_index + 1) % len(libs.keys()) # Go on to the next library, modulo library length
		counter = counter + 1
		# Whenever you re-start the library list, move down a paper
		if lib_index == 0:
			paper_index += 1 # Move on to the next paper index 
	# Return the interleaved recommendation list
	return recs 
########
# This slightly more complex merging function takes into account the size of each library
# when aggregating the papers. 
# It's currently under development. 
'''
def by_lib_size(libs,reclen=0): # If the rec len is 0 or otherwise untenable we assign it more appropriately
        #print(reclen)
        #print(libs.keys())
	# First check that the rec length is reasonable
	total_len = sum([len(libs[i]) for i in libs.keys()])
        if reclen == 0 or reclen > len(libs.keys()) * min([len(libs[i]) for i in libs.keys()]): # If reclen sucks
                reclen = len(libs.keys()) * min([len(libs[i]) for i in libs.keys()]) # Then define reclen appropriately

        # Now, assemble the personal recommendations aggregate
	# Identify the 'mix' of papers in the libraries -- e.g. what fraction of all recs come from which library?
	#lib_fracs = dict()
	#for l in lib.keys(): # Get the fraction of total recs represented by that library
	#	lib_fracs[l] = float(len(libs[l])) / total_len

	# Begin making the recommendation list -- make sure it's biased towards the big libraries...
	recs = []
	# Until you've recommended enough papers, 
        while len(recs) < reclen: # append more lists from each of the libraries
                #print lib_index, paper_index
		# Identify the largest library
		big = [i for i in max([len(j) for j in libs.values()])][0]
		print(big)
                recs.append(random.sample(libs[big],int(float(len(libs[big]) / total_len * rec_len)))
		libs[big] = [] # Set the length to 0, so that you never choose it again.

        # Return the recommendation list
        return recs
'''
