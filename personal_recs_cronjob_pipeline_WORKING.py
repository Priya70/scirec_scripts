#!/usr/bin/env python
### Composite pipeline for regular recommendation updates. 
## calling info:
'''
python ~/Scirec_scripts/personal_recs_cronjob_pipeline.py --pf 'pmids_for_testing.txt' --u "1" --l "3" --ci '/home/ubuntu/lda-data/Current_data/Current_basic_dataset_with_IF_and_LDA.csv' --ct '/home/ubuntu/lda-data/Current_data/Current_WordSim_data.csv'
'''
##################
##### Imports
##################

## this import section lifted directly from Priya's code -- unsure of what dependencies her functions have so I am taking it all just in case 
'''import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
from datetime import datetime,timedelta 
#import h5py
import subprocess
import os
from dateutil import rrule
import glob
import shutil
import numpy as np
import itertools
from bibtexparser.bparser import BibTexParser
import numpy as np
import os'''

## additional imports from me. 
import os
import numpy as np
import argparse
import pubRecords_1 as pr # my module
import Data_processing_scripts_WORKING as cpr # priya's module
import subprocess
##################
##### Argument processing and set-up
##################
##add only for testing :

#### Argument parsing
parser = argparse.ArgumentParser() ## Create an argument parser

# Arguments...
parser.add_argument('--cf', help = "config file with all settings") # config file
parser.add_argument('--u', help = "user ID") # user ID
parser.add_argument('--l', help = "library ID") # library ID
parser.add_argument('--pf', help = "PMID file list") # a file with PMIDs in it
parser.add_argument('--ci', help = "Corpus info file") # A file with all of the corpus info
parser.add_argument('--ct', help = "Corpus text info file -- e.g., includes titles and abstracts") # A file with all of the corpus info for word similarity
parser.add_argument('--pl', help = "PMID list", nargs = "+") # list of PMIDs for manual entry -- REDUNDANT with the PMID file list

# Now parse everything - these lines are required to create a list of args, l_args, to read
args = parser.parse_args()
l_args = vars(args) # Dictionary with argument names -> values 
arglist = l_args.values() # Values of all the arguments, in order
# Assign to variables
config = l_args["cf"] #config file
## If not using the config file, other args -- these args appear in the config file in this exact order
u_id = l_args["u"] #user ID
lib_id = l_args["l"] #library ID
lib_publist = l_args["pl"] #PMID list
lib_pubfile = l_args["pf"] #filename with PMID list stored within
corp_pubfile = l_args["ci"] #corpus info for first filtration steps
corp_txt = l_args["ct"] #larger file with corpus 


##################
#####  MAIN  #####
##################
########
##### File processing 
#########
 
## 1: Assembling complete record information for each of the papers in the library, not including topics
'''if lib_publist == None: # If a list of PMIDs hasn't been loaded in
	'''# Then load it yourself
	lib_publist = np.loadtxt(lib_pubfile)

# Now that you have a publist, go ahead and read in the information
#lib = pr.get_lib_db_info([ str(int(i)) for i in lib_publist]) # Recast the numpy.float64s as ints to remove the .0, then as strings to enable joining 

outpath='/home/ubuntu/Personalized_Recs/cronjob_testing/'+str(lib_id)

lib,lib_publist =cpr.getting_usrpubRecs_andforLDA([int(i) for i in lib_publist],lib_id,outpath)

## 2: Assembling complete record information for each of the papers in the corpus, INCLUDING topics
tmp_corp = pr.get_corp_info(corp_pubfile,delim=",")
corp=[paper for paper in tmp_corp if paper.pmid not in lib_publist]

## 3: Assembling topic information for the library, via LDA and inference - Priya, can you fill in this part? :) 
# input: lib
# output: lib, with updated topics (dictionary formatted as: tpc[topic] => p(topic))

# LDA inferencing

os.chdir(outpath)
cpr.run_lda_user(outpath+"/ForLDA_"+str(lib_id)+".txt",str(lib_id)) # Run topic inferencer
inference_results = outpath+'/'+str(lib_id)+"LDA_composition_for_database.txt" # Identify the file name with the topic assignments

# Topics updated with inferencing
lib = pr.get_tpcs_lib(inference_results,lib)

#########
##### Filtering
#########

## 1. Filtering by top topics above a certain fraction 
corp = pr.shared_tpc_cutter_3(lib,corp,frac=0.5) # Fraction can be modified by adding a frac=n line; otherwise, default is 0.9
print "corp has length ",len(corp)
## 2. Word similarities filtering 

# 2.1 Word similarity function: this should return a word similarity matrix
'''for p in lib: # Create the file that Priya's function requires -- deprecate this as soon as possible, to avoid extra file IO
	pr.print_pub_text(p,str(u_id)+"_text_file_for_wordsim_calculation.txt"): dont need this as the ForLDA file is exactly that'''
	
#Calculate word similarity and return the similarity matrix
simmat = cpr.vocab_list_Similarity_calculator(pr.pmid_list(corp),outpath+"/ForLDA_"+str(lib_id)+".txt") 

#print "word_sim done"

# 2.2 Word similarity score aggregation - produces global scores for papers
corp_score_list = pr.word_sim_combined_score(simmat,lib,corp)
print "corp_score_list has length ", len(corp_score_list)
#########
##### Output
#########

# 1. Choose the best papers by their scores; we assume the corp_score_list is in the same order as the corpus
best_recs = pr.choose_best_from_corp(corp,corp_score_list)

# 2. Output them into Yonggan's format
pr.output_rec_by_lib(best_recs,libname=lib_id,user=lib_id,date = pr.todays_date())
subprocess.call(['/home/ubuntu/Scirec_scripts/aws_update_personalrecs_cronjob_cleanup.sh']) 