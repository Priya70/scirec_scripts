#!/bin/bash
#shell script to upload all new data to s3 and stop the instance
cd
aws s3  sync lda-data/ s3://lda-data 
aws s3  sync Scirec_scripts s3://scirec-scripts 
aws s3  sync Personalized_Recs/ s3://Personalized_Recs


export EC2_URL=http://ec2.us-west-1.amazonaws.com
#instance_id=`ec2-metadata -i | awk -F': ' '{print $2}'`
#now="$(date +'%m_%d_%Y')"
#new_name="${now}_scirec_compute"
#ec2-stop-instances $instance_id
#ec2-create-image instance_id $instance_id --name $new_name 
