#!/usr/bin/bash







## A python script to read in all the the LDA files into the database:
import MySQLdb
import os
import string
import warnings
import subprocess
import glob
import shutil


def write_lda_file(year):
 # Open database connection
    mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='SciRec_lda',local_infile = 1)
    newdir='/home/ubuntu/lda-data/'+str(year)
    os.chdir(newdir)
    filelist=glob.glob('Uniq*')
    for file_name in filelist:
        data=pd.read_csv(file_name,sep="\t",header=None,cols=[)
        
        full_filename='/home/ubuntu/lda-data/'+str(year)+'/'+file_name 
        sql_query="load data local infile '"+ full_filename+"'  into table basic_paper_info  FIELDS TERMINATED BY '\t'  LINES TERMINATED BY '\n' (pmid,DateCreated,ArticleTitle,ISSN,ISSNLinking,AbstractText);"
        cur=mysql_con.cursor()
        cur.execute(sql_query)
        
        
        
        From Yonggan-

+from scireader.settings import DATABASES
+import MySQLdb as mdb
+from pubmed.views import current_path
+
+def run():
+	con = mdb.Connect(DATABASES['default']['HOST'],DATABASES['default']['USER'],DATABASES['default']['PASSWORD'],DATABASES['default']['NAME'])
+	p = current_path()
+	pubmed = p + "ldaoutput/pubmed.txt"
+	abstract = p + "ldaoutput/abstract.txt"
+	keyword = p + "ldaoutput/keyword.txt"
+	with con:
+		cur = con.cursor()
+		cmd = "SELECT PMID,ArticleTitle INTO OUTFILE '" + pubmed + "' FIELDS TERMINATED BY '\t' LINES TERMINATED BY '\n' FROM  `pubmed_pubmed`"
+		cur.execute(cmd)
+		cmd = "SELECT PMID,AbstractText INTO OUTFILE '" + abstract + "' FIELDS TERMINATED BY '\t' LINES TERMINATED BY '\n' FROM  `pubmed_abstract`"
+		cur.execute(cmd)
+		cmd = "SELECT PMID,Keyword INTO OUTFILE '" + keyword + "' FIELDS TERMINATED BY '\t' LINES TERMINATED BY '\n' FROM  `pubmed_keyword`"
+		cur.execute(cmd)
+		print "done"
