#! /usr/bin/python

import argparse
import pubRecordsProduction as pr # Natalie's module
import Data_processing_scripts as dp # Priya's module
import shutil
import logging
date = pr.todays_date()
logging.basicConfig(filename='/home/ubuntu/Logging_info/'+str(date)+'basic_cronjobs.log',level=logging.DEBUG, filemode='w')
import subprocess

shutil.copy2('/home/ubuntu/Scirec_scripts/lda_scripts/all_20140221093827_for_inferencer_ORIG.mallet' ,'/home/ubuntu/Scirec_scripts/lda_scripts/all_20140221093827_for_inferencer.mallet')
shutil.copy2('/home/ubuntu/Scirec_scripts/Production_code/lda_scripts/all_20140221093827_for_inferencer_ORIG.mallet' ,'/home/ubuntu/Scirec_scripts/Production_code/lda_scripts/all_20140221093827_for_inferencer.mallet')

dp.update_new_papers()
#make the basic paper_sets for all eg LDA and Word Similiarity 
dp.create_basic_papers_dataset_for_tpcRec() 
dp.create_basic_papers_dataset_with_AllTpcProb()
dp.create_basic_papers_dataset_for_WordSim()
#dp.create_vocab_for_corpus()
dp.update_corpus_TermFreqVec()
dp.create_pytables_for_all_libraries()
#dp.update_pytables_for_libraries()