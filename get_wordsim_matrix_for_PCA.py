#! /user/bin/python

'''
cronjob to update all libraries for all users daily.
'''
import sys
import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
import os
import glob
import shutil
import numpy as np
import itertools
from bibtexparser.bparser import BibTexParser
import subprocess
import cPickle as pickle

## additional imports from me. 
import argparse
import pubRecords_1 as pr # natalie's module
import natalies_modified_working_Priya_module_realwordsim as cpr # priya's module
#import Data_processing_scripts_WORKING as cpr # priya's module

#make sure update_corpus_TermFreqVec() is run first!!
#first move the old rec file to loaded directory.
out = sys.argv[1]
u_id = sys.argv[2]
lib_publist = [i.strip().split()[0] for i in open(sys.argv[3],'r').readlines()]
dbname = sys.argv[4]
lib_list = sys.argv[5:len(sys.argv)]
date = pr.todays_date()
o = open(out,'a')

#corp_pubfile='/home/ubuntu/lda-data/Current_data/Current_basic_dataset_with_IF_and_LDA.csv'
corp_pubfile = '/home/ubuntu/Natalie/PCA_graphs/corpus.txt'

out_ind_ct=1
print "hello is anyone here"
for lib_id in lib_list:
    print "updating recs for library ", lib_id 
    libname=lib_id     
    outpath='/home/ubuntu/Personalized_Recs/cronjob_testing/Vocab_testing/'+str(lib_id)
    if not os.path.exists(outpath):
        os.makedirs(outpath)    
    lib,lib_publist =cpr.getting_usrpubRecs_andforLDA([int(i) for i in lib_publist],str(lib_id),outpath)    
    #tmp_corp = pr.get_corp_info(corp_pubfile,delim=",")
    tmp_corp = [pr.pubRecords(p=int(i)) for i in open(corp_pubfile,'r').readlines()]
    #tmp_corp = [pr.pubRecords(p=int(i)) for i in lib_publist]
    corp=[paper for paper in tmp_corp if paper.pmid not in lib_publist]
    os.chdir(outpath)
    #cpr.run_lda_user(outpath+"/ForLDA_"+str(lib_id)+".txt",str(lib_id)) # Run topic inferencer
    #print "done lda for ", lib_id
    inference_results = outpath+'/'+str(lib_id)+"LDA_composition_for_database.txt" # Identify the file name with the topic assignments
    lib = pr.get_tpcs_lib(inference_results,lib)
    #corp = pr.shared_tpc_cutter_3(lib,corp,frac=0.5) # Fraction can be modified by adding a frac=n line; otherwise, default is 0
    simmat,updated_corp_list= cpr.create_updated_termFreqVec_AND_calcSimilarity(pr.pmid_list(corp),outpath+"/ForLDA_"+str(lib_id)+".txt") 
    sfile = "wordsim_matrix_library_included.txt"
    s = open(sfile,'w')
    s.write("\t"+"\t".join([str(i) for i in pr.pmid_list(corp)])+"\n")
    for i in range(len(simmat[:,0])):
	to_write = str(lib[i].pmid)
	for j in range(len(simmat[i,:])):
	    to_write += "\t"+str(simmat[i,j])
	s.write(to_write+"\n")
    s.close()
    #no need to actually go further this time - NT
 #   simmat = cpr.vocab_list_Similarity_calculator(pr.pmid_list(corp),outpath+"/ForLDA_"+str(lib_id)+".txt") 
 #if any papers have been removed in the Vocab step because they did not have titles/etc make sure you update the corp:   
    new_corp=[paper for paper in corp if paper.pmid in updated_corp_list]
    print "word_sim done for lib ", lib_id
    corp_score_list = pr.word_sim_combined_score(simmat,lib,new_corp)
    corp_score_list = pr.metadata_weighter_tweet_binning(new_corp,corp_score_list,use=[1,1,1],dbgvar=0)
    best_recs = pr.choose_best_from_corp(new_corp,corp_score_list)
    outputerror = 0  #This is a little error flag -- you'll try to write to output, and if it fails, you'll return an error flag in the end 
    for i, pub in enumerate(best_recs): # For every single pub
	    output = str(out_ind_ct)+"\t"+"NULL"+"\t"+str(libname)+"\t"+dbname+"\t"+str(pub.pmid)+"\n"
	    out_ind_ct +=1
	    #print output
	    o.write(output)
o.close()	
#pr.get_merged_recs(out,out,reclen=500)

 
