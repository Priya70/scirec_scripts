#!/usr/bin/local/python
# usage: create_tpc_vector  'tpc_wds_Cell.txt'  20
# usage : create_tpc_vector  infile  num_tpcs 
#  infile, n_tpcs='tpc_wds_2013_150.txt' ,'150' 

import sys
import numpy as np
from sklearn.metrics import pairwise_distances
from scipy.spatial.distance import cosine
from hcluster import pdist, linkage, complete, dendrogram
from scipy.linalg import norm
from scipy.spatial.distance import euclidean
import scipy.cluster.hierarchy
import scipy

def create_tp_vector(infile,n_tpcs):    
    n_tpcs=int(n_tpcs)
    data=open(infile,'r').readlines()
    tpcs=[[] for x in xrange(n_tpcs)]   #make a list for every topic, so tpcs is  list of lists
    tpcs_norm= [[] for x in xrange(n_tpcs)] 
    wrd_cnt=len(data)/n_tpcs     #total number of words in the vocabulary
    for i in range(n_tpcs):
        tpcs[i]=[float(line.split()[2]) for line in data[0+i*wrd_cnt:wrd_cnt+i*wrd_cnt]]        
        #include the next 1 line if wrd cnt is 150039
        tpcs[i]=tpcs[i][0:-1]        
        tp_sum=sum([float(kk) for kk in tpcs[i]])
        tpcs_norm[i]=[(float(kk)/tp_sum) for kk in tpcs[i]]
    tpcs_norm_array=np.array(tpcs_norm)
    tpcs_array=np.array(tpcs)
    wrds_vocab=[line.split()[1] for line in data[0:wrd_cnt]]
    return tpcs_norm_array,tpcs_array,wrds_vocab
   
###To use tf-idf on the topic vectors, first convert the tpc vectors into the correct format,
## ie get rid of the smoothing 
def tf(tpc_vec,n_tpcs):
    corr_tpc_vec=[[] for x in xrange(n_tpcs)]
    tf=[[] for x in xrange(n_tpcs)]
    for i in range(n_tpcs):
        corr_tpc_vec[i]=[floor(term) for term in tpc_vec[i]]         
        tf[i]=corr_tpc_vec[i]/sum(corr_tpc_vec[i])  
    corr_tpc_vec=np.array(corr_tpc_vec)
    idf=np.zeros(len(tpc_vec[1]))
    tf=np.asarray(tf)
    for i in xrange(len(tpc_vec[1])):        
        idf[i]=math.log10( float(n_tpcs)/sum(1 for a in corr_tpc_vec[:,i] if a>0) )
    tf_idf=multiply(tf,idf) ##or tf*idf        
    return tf_idf

def create_CosineSim_matrix(tpcs_norm_arr):
    from sklearn.metrics import pairwise_distances
    from scipy.spatial.distance import cosine
    #shape(tpcs_norm_array)
    dist_out = 1- pairwise_distances(tpcs_norm_arr, metric="cosine")  
   # dist_out = pairwise_distances(tpcs_norm_arr, metric="cosine")
    return dist_out
    
    
    

_SQRT2 = np.sqrt(2)     # sqrt(2) with default precision np.float64 

def hellinger1(p, q):
    return norm(np.sqrt(p) - np.sqrt(q)) / _SQRT2
 
def hellinger2(p, q):
    return euclidean(np.sqrt(p), np.sqrt(q)) / _SQRT2
 
def hellinger3(p, q):
    return np.sqrt(np.sum((np.sqrt(p) - np.sqrt(q)) ** 2)) / _SQRT2

def create_hell_matrix(tpcs_norm_array,n_tpcs):
    _SQRT2 = np.sqrt(2)  
    hell_arr=np.zeros((n_tpcs,n_tpcs))    
    for i in range(n_tpcs):
        for j in range(n_tpcs):
            hell_arr[i,j]=hellinger3(tpcs_norm_array[i],tpcs_norm_array[j])
    return hell_arr
        

def main(args):
    if len(args) ==1:
        return
    else:
        infile=args[1]
        n_tps=args[2]  
        tpc_vec_norm,tpc_vec,words_vocab=create_tp_vector(infile,n_tpcs)
        tf_idf=tf(tpc_vec,int(n_tpcs))
        dist_tf_idf_cos=create_CosineSim_matrix(tf_idf)
        np.savetxt('dist_tf_idf_cosine.csv',dist_tf_idf_cos, delimiter=',') 
        #dist_tf_idf_cos=np.loadtxt('dist_tf_idf_cosine.csv',delimiter=',')
        dist_tpc_vec_norm_cos=create_CosineSim_matrix(tpc_vec_norm)
        np.savetxt('dist_tpc_norm_cosine.csv',dist_tpc_vec_norm_cos, delimiter=',') 
       #dist_tpc_vec_norm=np.genfromtxt('dist_tpc_norm_cosine.csv',delimiter=',')
        dist_tpc_vec_norm_Hell=create_hell_matrix(tpc_vec_norm,int(n_tpcs))
        np.savetxt('dist_tpc_vec_norm_Hell.csv',dist_tpc_vec_norm_Hell,delimiter=',')


##making heatmaps and dendrograms

from hcluster import pdist, linkage, complete, dendrogram
    dist_out=pdist(tpc_array,'cosine')
    Z = linkage(dist_out)
    #Z=complete(dist_out)
    dendrogram(Z,leaf_font_size=8,leaf_rotation=90)
    

        heatmap = plt.pcolor(dist_Cos, cmap=matplotlib.cm.Blues)
        _SQRT2 = np.sqrt(2)  
        hell_dist=create_hell_matrix(tpc_vec_norm,int(n_tpcs))
        Z=complete(hell_arr)
        dendrogram(Z,leaf_font_size=8,leaf_rotation=90)
##      savefig("hell.png")
        
import scipy.cluster.hierarchy
from scipy.cluster.hierarchy import linkage, dendrogram  
        Z=scipy.cluster.hierarchy.linkage(dist_tf_idf_cos,method='average', metric='euclidean')
        dendrogram(Z,leaf_font_size=8,leaf_rotation=90)
       # dendrogram(linkage(distanceMatrix, method='complete')
      
      
        #compute and plot dendrogram
        fig=pylab.figure()
        axdendro = fig.add_axes([0.09,0.1,0.2,0.8])
        AA=dendrogram(Z,orientation='right')
        axdendro.set_xticks([])
        axdendro.set_yticks([])
        # Plot distance matrix.
        axmatrix = fig.add_axes([0.3,0.1,0.6,0.8])
        index = AA['leaves']
        newD=dist_tf_idf_cos[index,:]
        newD=dist_tf_idf_cos[:,index]
        #plot the new matrix 
        im = axmatrix.matshow(newD, cmap=cm.autumn  ,aspect='auto', origin='lower')
        axmatrix.set_xticks([])
        axmatrix.set_yticks([])
        axcolor = fig.add_axes([0.91,0.1,0.02,0.8])
        pylab.colorbar(im, cax=axcolor) ##pick the colormap of your choice from  http://wiki.scipy.org/Cookbook/Matplotlib/Show_colormaps
        fig.show()

        
        
        
        axcolor = fig.add_axes([0.91,0.1,0.02,0.8])
        axmatrix.set_yticks([])
        im = axmatrix.matshow(newD,aspect='auto', cmap=cm.autumn, origin='lower')
        
        transformedData = data[leaves,:] # here data needs to be the original data not the distance matrix i think
        
        
if __name__ == "__main__":
    main(sys.argv)                
            
