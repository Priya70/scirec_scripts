#! /user/bin/python

'''
cronjob to update all libraries for all users daily.
'''
import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
import os
import glob
import shutil
import numpy as np
import itertools
from bibtexparser.bparser import BibTexParser
import pubRecords_1 as pr
import subprocess

## additional imports from me. 
import argparse
import pubRecords_1 as pr # natalie's module
import Data_processing_scripts_WORKING as cpr # priya's module

#first move the old rec file to loaded directory.
if glob.glob('/home/ubuntu/Personalized_Recs/cronjob_testing/new/*'):
    shutil.copy2(glob.glob('/home/ubuntu/Personalized_Recs/cronjob_testing/new/*')[0],'/home/ubuntu/Personalized_Recs/cronjob_testing/loaded/')
    os.remove(glob.glob('/home/ubuntu/Personalized_Recs/cronjob_testing/new/*')[0])
#then proceed
mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
q='select * from library_library_papers'
data=psql.frame_query(q,con=mysql_con)
lib_list=list(pd.unique(data.libid_id))
print lib_list
date = pr.todays_date()
out = '/home/ubuntu/Personalized_Recs/cronjob_testing/new/'+str(date)+"_recs.txt"
o = open(out,'a')

corp_pubfile='/home/ubuntu/lda-data/Current_data/Current_basic_dataset_with_IF_and_LDA.csv'
out_ind_ct=1
for lib_id in lib_list:
    print "updating recs for library ", lib_id 
    lib_publist = list(data.dbid[data.libid_id == lib_id])
    print lib_publist
    print len(lib_publist)
    u_id= str(list(data.userid_id[data.libid_id == lib_id])[0])
    date = pr.todays_date()
    libname=lib_id     
    dbname =str(list(data.dbname[data.libid_id == lib_id])[0]) 
    outpath='/home/ubuntu/Personalized_Recs/cronjob_testing/'+str(lib_id)
    if not os.path.exists(outpath):
        os.makedirs(outpath)    
    outpath='/home/ubuntu/Personalized_Recs/cronjob_testing/'+str(lib_id)
    lib,lib_publist =cpr.getting_usrpubRecs_andforLDA([int(i) for i in lib_publist],str(lib_id),outpath)    
    tmp_corp = pr.get_corp_info(corp_pubfile,delim=",")
    corp=[paper for paper in tmp_corp if paper.pmid not in lib_publist]
    os.chdir(outpath)
    cpr.run_lda_user(outpath+"/ForLDA_"+str(lib_id)+".txt",str(lib_id)) # Run topic inferencer
    print "done lda for ", lib_id
    inference_results = outpath+'/'+str(lib_id)+"LDA_composition_for_database.txt" # Identify the file name with the topic assignments
    lib = pr.get_tpcs_lib(inference_results,lib)
    corp = pr.shared_tpc_cutter_3(lib,corp,frac=0.5) # Fraction can be modified by adding a frac=n line; otherwise, default is 0.9
    words_dict, TermFreqMat,TermFreqMatPmid=get_old_termFreqVec()
 #  simmat = cpr.create_updated_termFreqVec_AND_calcSimilarity(pr.pmid_list(corp),outpath+"/ForLDA_"+str(lib_id)+".txt") 
    simmat = cpr.vocab_list_Similarity_calculator(pr.pmid_list(corp),outpath+"/ForLDA_"+str(lib_id)+".txt") 
    print "word_sim done for lib ", lib_id
    corp_score_list = pr.word_sim_combined_score(simmat,lib,corp)
    best_recs = pr.choose_best_from_corp(corp,corp_score_list)
    outputerror = 0  #This is a little error flag -- you'll try to write to output, and if it fails, you'll return an error flag in the end 
    for i, pub in enumerate(best_recs): # For every single pub
	    output = str(out_ind_ct)+"\t"+"NULL"+"\t"+str(libname)+"\t"+dbname+"\t"+str(pub.pmid)+"\n"
	    out_ind_ct +=1
	    #print output
	    o.write(output)
	    
subprocess.call(['/home/ubuntu/Scirec_scripts/aws_update_personalrecs_cronjob_cleanup.sh'])    

 