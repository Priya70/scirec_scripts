#/usr/bin/python

import pubRecordsProduction as pr # natalie's module
import glob
import logging
import os

date = pr.todays_date()
logging.basicConfig(filename='/home/ubuntu/Logging_info/'+str(date)+'overall_recs.log',level=logging.DEBUG, filemode='w')

out='/home/ubuntu/Personalized_Recs/cronjob_testing/new/'
os.chdir(out)
myfile=glob.glob('*personal*')[0]
pr.get_merged_recs(myfile,myfile,reclen=500)
