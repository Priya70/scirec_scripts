#! /user/bin/python

'''
cronjob to update recommendations for all libraries for all users daily. This is version3 and 
uses the incremental updates model- does not recalculate lda for all users as in the first version. 
'''
import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
import os
import glob
import shutil
import numpy as np
import itertools
from bibtexparser.bparser import BibTexParser
import pubRecords_1 as pr
import subprocess
import cPickle as pickle
from boto.s3.connection import S3Connection
from boto.s3.key import Key
import boto
## additional imports from me. 
import argparse
import pubRecords_priya as pr # natalie's module
import Data_processing_scripts_WORKING as cpr # priya's module


#make sure update_corpus_TermFreqVec() is run first!!
#make sure  update_pytables_for_libraries() is run next before running this

#first move the old rec file to loaded directory.
'''if glob.glob('/home/ubuntu/Personalized_Recs/cronjob_testing/new/*'):
    shutil.copy2(glob.glob('/home/ubuntu/Personalized_Recs/cronjob_testing/new/*')[0],'/home/ubuntu/Personalized_Recs/cronjob_testing/loaded/')
    os.remove(glob.glob('/home/ubuntu/Personalized_Recs/cronjob_testing/new/*')[0])
'''
#then proceed
mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
q='select * from library_library_papers'
data=psql.frame_query(q,con=mysql_con)
lib_list=list(pd.unique(data.libid_id))
print lib_list
date = pr.todays_date()
out = '/home/ubuntu/Personalized_Recs/cronjob_testing/'+str(date)+"_Personal_recs.txt"
o = open(out,'a')
corp_pubfile='/home/ubuntu/lda-data/Current_data/Current_basic_dataset_with_IF_and_LDA.csv'
out_ind_ct=1
for lib_id in lib_list:
    print "updating recs for library ", lib_id 
    lib_publist = list(data.dbid[data.libid_id == lib_id])
    #print lib_publist
    print len(lib_publist)
    u_id= str(list(data.userid_id[data.libid_id == lib_id])[0])
    date = pr.todays_date()
    libname=lib_id     
    dbname =str(list(data.dbname[data.libid_id == lib_id])[0]) 
    outpath='/home/ubuntu/usrlibrary-data/libid_'+str(lib_id)
    if not os.path.exists(outpath):
        os.makedirs(outpath)  
    os.chdir(outpath)
    if glob.glob('*.h5'):
        os.remove(glob.glob('*.h5')[0])   
    cpr.get_olddata_from_s3(lib_id)  
    #read in the data and assign appropriate values
    if not glob.glob('*.h5'):
        #get out of the loop - nothing for this library-pick it up tomorrow
        break      
    ##reading in big corpus data   
    tmp_corp = pr.get_corp_info(corp_pubfile,delim=",")
    corp=[paper for paper in tmp_corp if paper.pmid not in lib_publist]
    #note now the users libraries are uodated so just read them in and go    
    lib,lib_publist =cpr.getting_users_paper_info_lda_pmid_uniq(lib_id,outpath)
    
 #   lib,lib_publist =cpr.getting_usrpubRecs_andforLDA([int(i) for i in lib_publist],str(lib_id),outpath)    
    corp = pr.shared_tpc_cutter_3(lib,corp,frac=0.5) # Fraction can be modified by adding a frac=n line; otherwise, default is 0
    if len(corp)>0:
        simmat,updated_corp_list= cpr.create_updated_termFreqVec_and_calcSimilarity2(pr.pmid_list(corp),"lib_"+str(lib_id)+".h5") 
#  simmat,updated_corp_list= cpr.create_updated_termFreqVec_AND_calcSimilarity(pr.pmid_list(corp),outpath+"/ForLDA_"+str(lib_id)+".txt") 
 #   simmat = cpr.vocab_list_Similarity_calculator(pr.pmid_list(corp),outpath+"/ForLDA_"+str(lib_id)+".txt") 
 #if any papers have been removed in the Vocab step because they did not have titles/etc make sure you update the corp:   
        new_corp=[paper for paper in corp if paper.pmid in updated_corp_list]
        print "word_sim done for lib ", lib_id
        corp_score_list = pr.word_sim_combined_score(simmat,lib,new_corp)
        corp_score_list = pr.metadata_weighter(new_corp,corp_score_list,use=[0.1,1,1],dbgvar=0)
        best_recs = pr.choose_best_from_corp(new_corp,corp_score_list)
        outputerror = 0  #This is a little error flag -- you'll try to write to output, and if it fails, you'll return an error flag in the end 
        for i, pub in enumerate(best_recs): # For every single pub
	   # output = str(out_ind_ct)+"\t"+"NULL"+"\t"+str(libname)+"\t"+dbname+"\t"+str(pub.pmid)+"\n"
	        output = str(out_ind_ct)+"\t"+str(u_id)+"\t"+str(libname)+"\t"+dbname+"\t"+str(pub.pmid)+"\n"
	        out_ind_ct +=1
	  # print output
	        o.write(output)
	
	              
o.close()	
pr.get_merged_recs(out,out,reclen=500)
#subprocess.call(['/home/ubuntu/Scirec_scripts/aws_update_personalrecs_cronjob_cleanup.sh'])    

 