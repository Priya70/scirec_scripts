#!/usr/bin/env python
##### Merged recommendation creater -- this creates a personalized, merged recommendation list
import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
import os
import glob
import shutil
import numpy as np
import itertools
from bibtexparser.bparser import BibTexParser
import pubRecords_1 as pr
import subprocess

## additional imports from me. 
import argparse
import pubRecords_1 as pr # natalie's module
import Data_processing_scripts_WORKING as cpr # priya's module
import recMerge as rm

######### Argument parsing 
#### Argument parsing
parser = argparse.ArgumentParser() ## Create an argument parser

# Arguments...
parser.add_argument('--u', help = "user ID") # user ID -- can run on an individual user_ID if necessary
parser.add_argument('--rf', help = "today's rec file", default = pr.todays_date()+"_recs.txt") # The recommendation file to merge
parser.add_argument('--rl', help = "length of recommendations to provide",default=0) # Length of recs to provide
parser.add_argument('--of', help = "output file to append recommendations to", default=pr.todays_date()+"_recs.txt") # Where to output recs
# Now parse everything - these lines are required to create a list of args, l_args, to read
args = parser.parse_args()
l_args = vars(args) # Dictionary with argument names -> values 
arglist = l_args.values() # Values of all the arguments, in order
# Assign to variables
u_id = l_args["u"] #user ID, if looking at a specific user
recfile = l_args["rf"] # rec file, either default or actually entered
reclen = int(l_args["rl"]) # rc list length
outfile = l_args["of"]

### Merge recommendations 
pr.get_merged_recs(recfile,outfile,reclen=reclen)
