#!/usr/bin/env python
### Composite pipeline for regular recommendation updates. 
## pipeline edited for TEN FOLD VALIDATION scheme, edits marked '10'
##################
##### Imports
##################

## this import section lifted directly from Priya's code -- unsure of what dependencies her functions have so I am taking it all just in case 
'''import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
from datetime import datetime,timedelta 
#import h5py
import subprocess
import os
from dateutil import rrule
import glob
import shutil
import numpy as np
import itertools
from bibtexparser.bparser import BibTexParser'''
import numpy as np
import os
## additional imports from me. 
import argparse
import pubRecords_1 as pr # my module
import natalie_modified_Priya_module as cpr # Priya's module with my edits
#for the 10fold
import random
##################
##### Argument processing and set-up
##################

#### Argument parsing
parser = argparse.ArgumentParser() ## Create an argument parser

# Arguments...
parser.add_argument('--cf', help = "config file with all settings") # config file
parser.add_argument('--u', help = "user ID") # user ID
parser.add_argument('--l', help = "library ID") # library ID
parser.add_argument('--pf', help = "PMID file list") # a file with PMIDs in it
parser.add_argument('--ci', help = "Corpus info file") # A file with all of the corpus info
parser.add_argument('--ct', help = "Corpus text info file -- e.g., includes titles and abstracts") # A file with all of the corpus info for word similarity
parser.add_argument('--pl', help = "PMID list", nargs = "+") # list of PMIDs for manual entry -- REDUNDANT with the PMID file list

# Now parse everything - these lines are required to create a list of args, l_args, to read
args = parser.parse_args()
l_args = vars(args) # Dictionary with argument names -> values 
arglist = l_args.values() # Values of all the arguments, in order
# Assign to variables
config = l_args["cf"] #config file
## If not using the config file, other args -- these args appear in the config file in this exact order
u_id = l_args["u"] #user ID
lib_id = l_args["l"] #library ID
lib_publist = l_args["pl"] #PMID list
lib_pubfile = l_args["pf"] #filename with PMID list stored within
corp_pubfile = l_args["ci"] #corpus info for first filtration steps
corp_txt = l_args["ct"] #larger file with corpus 

##################
#####  MAIN  #####
##################

#########
##### File processing 
#########
 
## 1: Assembling complete record information for each of the papers in the library, not including topics
if lib_publist == None: # If a list of PMIDs hasn't been loaded in
	# Then load it yourself
	lib_publist = np.loadtxt(lib_pubfile)
##10 inferencing moved here, so it's only done once....
outpath='/home/ubuntu/Personalized_Recs/cronjob_testing/'+str(u_id) # define an outpath 
lib =cpr.getting_usrpubRecs_andforLDA([int(i) for i in lib_publist],u_id,outpath) # grab the user library
os.chdir(outpath) #lib = pr.get_lib_db_info([ str(int(i)) for i in lib_publist]) # Recast the numpy.float64s as ints to remove the .0, then as strings to enable joining 
#cpr.run_lda_user(outpath+"/ForLDA_"+str(u_id)+".txt",u_id) # Run topic inferencer
inference_results = outpath+'/'+str(u_id)+"LDA_composition_for_database.txt" # Identify the file name with the topic assignments
# Topics updated with inferencing
# Fixing the tuple thing....
newlib = []
for i in range(len(lib[0])):
	lib[0][i].pmid=lib[1][i]
	newlib.append(lib[0][i])
lib = newlib
lib_cut = pr.get_tpcs_lib(inference_results,lib)

## 2: Assembling complete record information for each of the papers in the corpus, INCLUDING topics
print "Finished with LDA!" #TPL
corp_cut = pr.get_corp_info(corp_pubfile,delim=",")
print "Finsihed assembling corpus!" #TPL
## 1. Filtering by top topics above a certain fraction 
corp_cut = pr.shared_tpc_cutter_3(lib,corp_cut,frac=0.5) # Fraction can be modified by adding a frac=n line; otherwise, default is 0.9
print "corp has length ",len(corp_cut)
## 2. Word similarities filtering 

#Calculate word similarity and return the similarity matrix
simmat = cpr.vocab_list_Similarity_calculator(pr.pmid_list(corp_cut),outpath+"/ForLDA_"+str(u_id)+".txt",corp_cut,[str(int(i)) for i in pr.pmid_list(lib_cut)])
simfile = "wordsim_matrix.txt"
s = open(simfile,'w')
s.write("\t"+"\t".join([str(i) for i in pr.pmid_list(corp_cut)])+"\n")
for i in range(len(simmat[:,0])):
	to_write = str(lib_cut[i].pmid)
	for j in range(len(simmat[i,:])):
		to_write += "\t"+str(simmat[i,j])
	s.write(to_write+"\n")
s.close()

#print simmat[0,:] # TPL
#print simmat[:,0] # TPL
print "word_sim done for lib ", lib_id
corp_score_list = pr.word_sim_combined_score(simmat,lib_cut,corp_cut,alpha=1000)
print "corp_score_list has length ", len(corp_score_list)
#best_recs = pr.choose_best_from_corp(corp_cut,corp_score_list)	
#corp_score_list = pr.metadata_weighter(corp_cut,corp_score_list,dbgvar=0,use=[1,1,1])
#########
##### Output
#########

### The only thing to output is the actual wordsim matrix...
# 1. Choose the best papers by their scores; we assume the corp_score_list is in the same order as the corpus
#best_recs,best_scores = pr.modified_choose_best_from_corp(corp_cut,corp_score_list,reclen=500)
#best_scores = pr.metadata_weighter(best_recs,best_scores)
#best_recs,best_scores = pr.modified_choose_best_from_corp(corp_cut,corp_score_list,reclen=len(best_recs))
# 2. Output them into Yonggan's format
### Output the test library as well as the test corpus
#pr.modified_output_rec_by_lib(best_recs,[str(1) for i in range(len(best_recs))],libname=lib_id,user=u_id,date = "not-a-real-date")
