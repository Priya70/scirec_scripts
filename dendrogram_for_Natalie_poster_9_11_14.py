##to create a dendrogram using the cosine similarity as the metric for natalie:

infile='/home/ubuntu/mallet/mallet-2.0.7/for_database/make_inferencer_20140221093827/wrd_tpcall_20140221093827_150_9603.txt'
n_topics=150

def create_tp_vector(infile,n_tpcs):    
    n_tpcs=int(n_tpcs)
    data=open(infile,'r').readlines()
    tpcs=[[0]*len(data) for x in xrange(n_tpcs)]  
    word_vocab=[]
    ct=0
    for line in data:
        word_vocab.append(line.split()[1])   
        aa=line.split()[2:]
        for tt in aa:
            tpcs[int(tt.split(':')[0])][ct]=int(tt.split(':')[1])            
        ct=ct+1   
    tpcs.insert(0,word_vocab)      
    tpcs_df=pd.DataFrame(tpcs)           
    return tpcs_df
    
tpcs_df=create_tp_vector(infile ,'150')
tmp=tpcs_df.drop(tpcs_df.index[[0]])
##to get it to the format I had before...(in create_tpc_vector.py)
tpcs=np.asarray(tmp)
for i in range(n_tpcs):
    tp_sum=sum([float(kk) for kk in tpcs[i]])
    tpcs_norm[i]=[(float(kk)/tp_sum) for kk in tpcs[i]]

def tf(tpc_vec,n_tpcs):
    corr_tpc_vec=[[] for x in xrange(n_tpcs)]
    tf=[[] for x in xrange(n_tpcs)]
    for i in range(n_tpcs):
        corr_tpc_vec[i]=[floor(term) for term in tpc_vec[i]]         
        tf[i]=corr_tpc_vec[i]/sum(corr_tpc_vec[i])  
    corr_tpc_vec=np.array(corr_tpc_vec)
    idf=np.zeros(len(tpc_vec[1]))
    tf=np.asarray(tf)
    for i in xrange(len(tpc_vec[1])):        
        idf[i]=math.log10( float(n_tpcs)/sum(1 for a in corr_tpc_vec[:,i] if a>0) )
    tf_idf=multiply(tf,idf) ##or tf*idf        
    return tf_idf

tpcs_array=np.array(tpcs)
tf_idf=tf(tpcs_array,int(n_tpcs))
testing=pd.DataFrame(tf_idf)
dist_tf_idf_cos=create_CosineSim_matrix(tf_idf)

    