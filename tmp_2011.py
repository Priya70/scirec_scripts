import os  
import sys 
import Data_processing_scripts as dp
import subprocess
import glob
import shutil

filelist=glob.glob('ForLDA*csv')
print filelist
for file in filelist[7:]:        
        lda_script_path='/data/priya/Topic_modelling/mallet-2.0.7/for_database/Calculating_Rec_Scores/JP_test_set/'
        print "running LDA for ", file 
        subprocess.call([lda_script_path+'run_LDA.sh',file])
        shutil.copyfile('output_file_tpcs_composition_for_database.txt',file.split('csv')[0]+'LDA_composition.txt')
        os.remove('output_file_tpcs_composition.txt')
        os.remove('output_file_tpcs_composition_for_database.txt')  
