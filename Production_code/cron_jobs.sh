#!/bin/bash
##script for cron jobs

echo "running basic jobs on "$(date) >>/home/ubuntu/update_rec.txt
python /home/ubuntu/Scirec_scripts/Production_code/basic_cronjobs.py 
python /home/ubuntu/Scirec_scripts/Production_code/chk_if_pytable_exists.py
echo "Starting update of Tpcs at "$(date)>>/home/ubuntu/update_rec.txt 
python /home/ubuntu/Scirec_scripts/Production_code/Update_tpc_recommendations.py 
echo "Starting update of personal libraries on "$(date) >>/home/ubuntu/update_rec.txt
python /home/ubuntu/Scirec_scripts/Production_code/twitter_recs.py
python /home/ubuntu/Scirec_scripts/Production_code/Personal_recs2.py
#python /home/ubuntu/Scirec_scripts/Production_code/blank_recs.py
python /home/ubuntu/Scirec_scripts/Production_code/Overall_recs.py
python /home/ubuntu/Scirec_scripts/Production_code/combine_recs.py
echo "Done updating personal libraries on "$(date) >>/home/ubuntu/update_rec.txt
#/home/ubuntu/Scirec_scripts/Production_code/aws_update_personalrecs_cronjob_cleanup.sh 
echo "Done uploading files to S3 on"$(date) >>/home/ubuntu/update_rec.txt
cd /home/ubuntu/
aws s3 rsync /home/ubuntu/lda-data  s3://lda-data/ --recursive
 