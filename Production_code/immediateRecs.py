#!/usr/bin/env python
__all__ = ['immediateRecs']

### Purpose of this package
# this package enables immediate recommendation 
# based on any given subset of user data. Given 
# user_topics, keywords that users like, and
# authors theyr'e interested in following,
# depending on what you get, identify which papers
# would be interesting to the user for a first round
# of recommendations. Then, return this list. 

######
# Imports
######

import pubRecordsProduction as pr # use the pubRecords data structures
import MySQLdb # use this for getting information from the pubmed database
import subprocess # use this to get the information about the topics via grepping. grepping is so fast :) 
from random import shuffle # all I actually use is the shuffle function, so I'll import it
import numpy as np
import datetime
###############
### Information processing
###############

## Returns the numerical topics a user is interested in
def get_topics(u_id): 
	q = "SELECT userid_id,subcatid_id FROM feeds_mytopics WHERE userid_id ="+str(u_id)+";" # Select all of the topics
        mysql_con = MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
        c=mysql_con.cursor()
        c.execute(q)
        # Obtain your queried information
        topicdata = c.fetchall()
        topicdata = np.asarray(topicdata) # who doesn't love NParrays

        # For each user, identify which topics are relevant, and get papers for those.
        user_topics = set() # This dictionary will hold user topics and papers with those topics
        for i in range(len(topicdata[:,0])):
		try: # Try to actually go over the list of user_topics.
	                user_topics = user_topics.union(set([int(topicdata[i,1])]))
		except:
			pass
	return [i for i in user_topics]

## Returns the names of authors a user is interested in
def get_auths(user):
	auths = []
	return auths

## Returns the keywords a user is interested in
def get_keywords(u_id):
	# Then get all of the keywords of interest for that user
        q = "SELECT userid_id,interest FROM accounts_interests WHERE userid_id ="+str(u_id)+";" # Select all of the topics
        mysql_con = MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
        c=mysql_con.cursor()
        c.execute(q)
        # Obtain your queried information
        kwdata = c.fetchall() # recall that this returns a tuple.
	user_kwords = []
	for i in range(len(kwdata)):
                user_kwords.append(kwdata[i][1])
	#print user_kwords
	return [i.upper() for i in user_kwords]

###############
### Filtering by each category
###############

def find_papers_with_topic(file,topics):
	# The command will look like: grep ",15," /home/ubuntu/lda-data/Current_data/Current_basic_dataset_with_IF_and_LDA.csv
	# this example is for topic 15
	# Use subprocess for each topic
	papers = []
	for t in topics:
		#g = subprocess.Popen(['grep',","+str(t)+",", file]) # get the grep 
		#papers = subprocess.check_output(('cut', '-d = \",\"', '-f1,1'), stdin=g.stdout).split("\n") 
		tstr = "\","+str(t-1)+",\"" # topic string
		#print tstr
		command = "cut -f1,2,3 -d \",\" "+str(file)+" | grep "+tstr
		#print command
		#papers = subprocess.check_output(('cut', '-f1,2', '-d=','\",\"', file, '| grep', tstr), shell=True).split(",")[0]
		res = subprocess.check_output(command, shell=True)
		#papstrings = res.split("\n")
		papers += [i.strip().split(",")[0] for i in res.split("\n")]
		papers = papers[0:len(papers)-1] # don't return the last blank element
	return [pr.pubRecords(p=i) for i in papers]

### THis function identifies papers whose abstracts or titles include keywords		
def find_papers_with_keyword(papers,kwords):
	# Identify if this paper has any words in common with my keyword SET. This should be passed as a SET. 
	# GEt the abstracts from the database -- no need to knit them together as you're just looking for word incidence
	papers_pmids = ",".join([str(i) for i in (pr.pmid_list(papers))])
        q = "SELECT PMID_id, AbstractText FROM pubmed_abstract WHERE PMID_id IN ("+papers_pmids+");"
        mysql_con = MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
        c=mysql_con.cursor()
        c.execute(q)    
        # Obtain your queried information
        data = c.fetchall()
        data = np.asarray(data) # who doesn't love NParrays
	#print data
	newpapers = [] # Here's a list of the new papers you're recommending. 
	kwords=set(k.upper() for k in kwords)
	# For each paper, see if any of the words in it overlap with the keywords
	for i in range(len(data[:,0])): # For every line of the data
		# If this paper isn't already added in newpapers... [this checks if it's in newpapers]
		if data[i,0] in newpapers:
			continue #ignore it if it's added into newpapers
		else: ## Otherwise, identify whether it belongs there -- does it match any keywords?
			text = set([j.upper() for j in data[i,1].strip().split()])
			if len(kwords.intersection(text)) != 0: # If there's keyword overlap
				newpapers.append(data[i,0]) # Append the PMID to new papers
	return newpapers
	
	
## Returns a list of pubRecords objects based on the topics, authors, and keywords
def get_first_recs(topics,topicfile,auths,kwords,use=[1,1,1]): # By default, use all of them; you can set this otherwise though
	# First, identify papers which belong in those topics. 
    papers = find_papers_with_topic(topicfile,topics)
	# Then identify which papers match the correct keywords
	# papers = find_papers_with_keyword(papers,set(kwords)) # This uses set intersect to identify intersection, ergo the set conversion
	# Then, get the most tweeted ones
    shuffle(papers)
    # priya-commented out -papers is already a list of pubRecords so cannot call pubRecords(i)	
    # papers = [pr.pubRecords(p=i) for i in papers[0:min(500,len(papers))]]
    papers=[i for i in papers[0:min(500,len(papers))]]
    #print len(papers)
    scores =  pr.metadata_weighter(papers,[1 for i in range(len(papers))],dbgvar=0,use = [1,0,0])
    # Test print.
    #for i,p in enumerate(papers):
	    #print p.pmid, scores[i]
	# Choose the first papers
    recs = pr.choose_best_from_corp(papers,scores,reclen=min(500,len(papers)))
	#print(len(recs))
    return recs

######## Recs for a specific user, upon finding the user's library is empty. 
def blank_library_recs(u_id,file,use=[1,0,1]): # authors still in progress. 
	# Assemble information for the user        
	## Topics are use case 0
	if use[0] > 0: # Then get all of the topics of interest for that user
		topics = get_topics(u_id)
	else:
		topics = [] # make sure the topics list exists but is empty
	## Authors are use case 1
       	if use[1] > 0:
		print "Sorry, author partitioning does not yet exist"
	else:
		auths = [] # otherwise just make sure the author list EXISTS but is empty
	## Keywords are use case 2
        if use[2] > 0:
		kwords = get_keywords(u_id) 
	else:
		kwords = [] # just make sure the list exists but is empty

	## Now that you've assembled information, make recommendations... the use statement is the length of the lists from above
        recs = [i.pmid for i in get_first_recs([i for i in topics],file,auths,kwords,use=[len(topics),len(auths),len(kwords)])]        
	return recs
            
######## TWEET WEIGHTING SPECIFIC FUNCTIONS
def find_papers_with_topic_dateweight(file,topics):
        # The command will look like: grep ",15," /home/ubuntu/lda-data/Current_data/Current_basic_dataset_with_IF_and_LDA.csv
        # this example is for topic 15
        # Use subprocess for each topic
        papers = []
        for t in topics:
		# Set up a command to grab ALL The papers in a particular topic. 
                tstr = "\","+str(t)+",\"" # topic string
                command = "cut -f1,2,3,8 -d \",\" "+str(file)+" | grep "+tstr # Take the 500 most recent only. 
                res = subprocess.check_output(command, shell=True)
		for line in res.split("\n"): # For every line in those results, grab only the recent papers.
			# Is this a recent paper?
			parts = line.strip().split(",")
			if len(parts) == 4: # If this is an actual paper line,
				date = [int(i) for i in parts[3].split("-")]
				paper_date = datetime.date(date[0],date[1],date[2])
				print paper_date
				if (datetime.date.today() - paper_date).days < 7: # If it's a recent paper
	        		        papers.append(pr.pubRecords(p=parts[0],d=date))
	return papers

