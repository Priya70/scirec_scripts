#!/user/bin/python

'''
script to read in the 3 files: twitter, liked and personal recs and then concatenate them.
'''

import pandas as pd
import glob
import pubRecordsProduction  as pr 
import os
import subprocess
import shutil
import logging
date = pr.todays_date()
logging.basicConfig(filename='/home/ubuntu/Logging_info/'+str(date)+'combine_recs.log',level=logging.DEBUG, filemode='w')


outpath='/home/ubuntu/Personalized_Recs/cronjob_testing/new/'
os.chdir(outpath)

twitter_file=glob.glob('*twitter*')[0]
personal_file=glob.glob('*personal*')[0]

twit_data=pd.read_csv(twitter_file, sep='\t', header=None, dtype={1:str})
personal_data=pd.read_csv(personal_file, sep='\t', header=None, dtype={1:str})


data=pd.concat([twit_data,personal_data],ignore_index=True)
data.index=data.index+1
data.drop([0],axis=1,inplace=True)
date = pr.todays_date()
data.to_csv('/home/ubuntu/Personalized_Recs/cronjob_testing/new/'+str(date)+"_recs.txt",sep='\t',na_rep='NULL',index=True,header=True)


shutil.copy2(twitter_file,'/home/ubuntu/Personalized_Recs/cronjob_testing/loaded/')   
shutil.copy2(personal_file,'/home/ubuntu/Personalized_Recs/cronjob_testing/loaded/') 
os.remove(twitter_file)
os.remove(personal_file)
os.chdir('/home/ubuntu')


subprocess.call(['/home/ubuntu/Scirec_scripts/Production_code/aws_Update_personalrecs_cronjob_cleanup.sh'])    

