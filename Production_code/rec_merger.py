#!/usr/bin/env python
##### Merged recommendation creater -- this creates a personalized, merged recommendation list
import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
import os
import glob
import shutil
import numpy as np
import itertools
from bibtexparser.bparser import BibTexParser
import pubRecordsProduction as pr
import subprocess

## additional imports from me. 
import argparse
import pubRecords_1 as pr # natalie's module
import Data_processing_scripts_WORKING as cpr # priya's module
import recMerge as rm

######### Argument parsing 
#### Argument parsing
parser = argparse.ArgumentParser() ## Create an argument parser

# Arguments...
parser.add_argument('--u', help = "user ID") # user ID -- can run on an individual user_ID if necessary
parser.add_argument('--rf', help = "today's rec file", default = pr.todays_date()+"_recs.txt") # The recommendation file to merge
parser.add_argument('--rl', help = "length of recommendations to provide") # Length of recs to provide
parser.add_argument('--of', help = "output file to append recommendations to", default=pr.todays_date()+"_recs.txt") # Where to output recs
# Now parse everything - these lines are required to create a list of args, l_args, to read
args = parser.parse_args()
l_args = vars(args) # Dictionary with argument names -> values 
arglist = l_args.values() # Values of all the arguments, in order
# Assign to variables
u_id = l_args["u"] #user ID, if looking at a specific user
recfile = l_args["rf"] # rec file, either default or actually entered
reclen = int(l_args["rl"]) # rc list length
outfile = l_args["of"]
######### Identifying user-library groupings. 
# First, move the old recommendation file.
'''if glob.glob('/home/ubuntu/Personalized_Recs/cronjob_testing/new/*'):
    shutil.copy2(glob.glob('/home/ubuntu/Personalized_Recs/cronjob_testing/new/*')[0],'/home/ubuntu/Personalized_Recs/cronjob_testing/loaded/')
    os.remove(glob.glob('/home/ubuntu/Personalized_Recs/cronjob_testing/new/*')[0])
'''
# Then, identify the user-library-paper pairings and store them as a dictionary.
# First, identify which users are associated with which libraries via mysql query. 
if u_id == None: # If there is a user-id specified
	mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
	q = 'SELECT userid_id,id FROM library_libraries'
	c=mysql_con.cursor()
	c.execute(q)
	data = c.fetchall() # This returns a tuple of this format: userid,libid,dbmane,dbID
	c.close()
else: # Otherwise, scrape the whole library
	mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
	q = 'SELECT userid_id,id FROM library_libraries WHERE userid_id='+str(u_id)
	c=mysql_con.cursor()
	c.execute(q)
	data = c.fetchall() # This returns a tuple of this format: userid,libid,dbmane,dbID
	c.close()

# Given the data, create a dictionary with user IDs linked to dictionaries with the library IDs as keys
user_libsets = dict()
for i in range(len(data)): # For every line...
	try: # Can you create a blank list to store the list of papers later?
		user_libsets[int(data[i][0])][int(data[i][1])] = []
		#print int(data[i][0]), int(data[i][1])
	except: # If not, set up the dictionary in the first place.
		user_libsets[int(data[i][0])] = dict()
		user_libsets[int(data[i][0])][int(data[i][1])] = []
		#print int(data[i][0]), int(data[i][1])

##### Now, add the recommendation data into the user -> library -> [list of rec'd papers]
# Load in the recommendation data...
data = [line.strip().split() for line in (open(recfile,'r').readlines())]
#print data
# File format is: 
#recnum	user	lib	db	dbID
#1	NULL	3	pubmed	25164749

# Create the dictionary described above
for i in range(len(data)): # For every line of the libdata...
	# Identify the correct user
	this_user = 0
	while this_user == 0: # Until you find which user it is
		for user in user_libsets.keys(): # For all of the users, 
			#print user, user_libsets[user].keys(), int(data[i][2]) #TPL
			if int(data[i][2]) in user_libsets[user].keys(): # if this library is in that user's library list...
				this_user = user # Go ahead and set that as the user
				#print "Assigning library", data[i][2], "to user", user
	try: # add the library and its corresponding paper to the dictionary of dictionaries
		user_libsets[int(this_user)][int(data[i][2])].append(pr.pubRecords(p=int(data[i][4])))
		#print "success"
	except: # IF that doesn't work out, create it
		#print "failure"
		try: # Try to create an addition to the dictionary of dictionaries		
			user_libsets[int(this_user)][int(data[i][2])] = [pr.pubRecords(p=data[i][4])] # Each library's linked to a list of pubRecords of papers
		except: # If that doesn't work you haven't even got your dictionary of dictionaries together! Get that together. 
			user_libsets[int(this_user)] = dict()
			user_libsets[int(this_user)][int(data[i][2])] = [pr.pubRecords(p=int(data[i][4]))]
	#print data[i], len(user_libsets[int(this_user)][int(data[i][2])])
	#print user_libsets[this_user].keys()

######### Merging user-library groupings into single recommendation lists
# This will be done using the recMerge package (see imports)
for user in user_libsets.keys(): # For each user
	# Obtain the new recommendation list
	#recs = rm.by_lib_size(user_libsets[user],reclen=reclen) -- this option is still under construction
	recs = rm.basic(user_libsets[user],reclen=reclen)
	#for i in recs:
		#i.pRprint()
	pr.output_rec_by_lib(recs,libname="NULL",dbname="pubmed",user=str(int(user)),date=pr.todays_date(),outf = outfile)
