#! /usr/local/bin/python
#usage python match_inference_topics_to_pmids_NO_reorder.py  'pubmed_1_14_2014_topics.txt' 'pubmed_for_inferencer.txt'  'pubmed_1_14_2014_topics_for_database.txt'
import sys    

def main(args):
  if len(args) < 3:
    print "# of arguments incorrect!"
    return
  else:  
    tpc_file, txt_file, out_file=args[1:4]
    topic_data=open(tpc_file).readlines()
    txt_data=open(txt_file).readlines()
    
    txt_pmid=[]
    for line in txt_data:
        txt_pmid.append(line.split()[0])
        
    out=open(out_file,'w')
    count=0
    for line in topic_data[1:]:
        aa=line.split(' ')[2:302]
        aa.insert(0,str(txt_pmid[count]))
        outstr='\t'.join(aa)  
        out.write(outstr+'\n') 
        count=count+1 
   
    out.close()

if __name__ == "__main__":
    main(sys.argv)