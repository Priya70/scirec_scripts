#!/bin/bash
#shell script to call from python to run lda after we have abstract data.
input_txt=$1 
echo "input.txt is ", $input_txt 
python /home/ubuntu/Scirec_scripts/Production_code/lda_scripts/clean.py  $input_txt >> input_file_for_inferencer.txt
mallet_path='/home/ubuntu/mallet/mallet-2.0.7/bin'
$mallet_path/mallet   import-file    --input  input_file_for_inferencer.txt    --keep-sequence  --remove-stopwords   --output output_file.mallet    --use-pipe-from  /home/ubuntu/Scirec_scripts/Production_code/lda_scripts/all_20140221093827_for_inferencer.mallet 
$mallet_path/mallet   infer-topics    --input  output_file.mallet     --inferencer  ~/Scirec_scripts/lda_scripts/all_20140221093827inferencer_150_9603    --output-doc-topics    output_file_tpcs_composition.txt   --num-iterations 150
python /home/ubuntu/Scirec_scripts/Production_code/lda_scripts/match_inference_topics_to_pmids_NO_reorder.py   output_file_tpcs_composition.txt    input_file_for_inferencer.txt   output_file_tpcs_composition_with_pmid.txt
python /home/ubuntu/Scirec_scripts/Production_code/lda_scripts/match_inference_topics_to_pmids_reordered.py    output_file_tpcs_composition.txt   input_file_for_inferencer.txt   output_file_tpcs_composition_for_database.txt
pwd
rm input_file_for_inferencer.txt
rm output_file_tpcs_composition.txt  
rm output_file.mallet          
