#! /usr/local/bin/python
#usage python match_inference_topics_to_pmids_reordered.py  'pubmed_1_14_2014_topics.txt' 'pubmed_for_inferencer.txt'  'pubmed_1_14_2014_topics_for_database.txt'
import sys    

def main(args):
  if len(args) < 3:
    print "# of arguments incorrect!"
    return
  else:  
    tpc_file, txt_file, out_file=args[1:4]
    topic_data=open(tpc_file).readlines()
    txt_data=open(txt_file).readlines()
    
    txt_pmid=[]
    for line in txt_data:
        txt_pmid.append(line.split()[0])
        
    nn=(len(topic_data[1].split())-2)/2  #nn is num_topics
    print nn
    out=open(out_file,'w')
    count=0
    for line in topic_data[1:]:
        aa=line.split()
        topic_list=[int(a) for a in aa[2::2]]
        map=[(topic_list.index(i)*2)+2 for i in range(nn)] 
        topic_pmid=txt_pmid[count]   
        out_str=topic_pmid  
        for k in map:
            out_str=out_str+'\t'+aa[k+1]
        out.write(out_str+'\n') 
        print out_str
        count=count+1 
   
    out.close()

if __name__ == "__main__":
    main(sys.argv)                
            

     
