#! /usr/local/bin/python
'''
script to update the most recent data; and get the topic recommendations out..
python /home/ubuntu/Scriec_scripts/Update_tpc_recommendations.py 
'''
import pandas.io.sql as psql
import MySQLdb
import pandas as pd
import numpy as np
from datetime import datetime,timedelta
import Data_processing_scripts as dp
import os  
import sys 
import glob
import shutil
from dateutil import rrule
import pubRecordsProduction  as pr
import subprocess
import logging
date = pr.todays_date()
logging.basicConfig(filename='/home/ubuntu/Logging_info/'+str(date)+'Update_tpc_recs.log',level=logging.DEBUG, filemode='w')


#Expecting that all the relevant data for 2014 is already there in lda-data/2014.
#update paper data -or should that have already been done by cron job??
#/home/ubuntu/Scirec_scripts/script_to_getMysqldata_and_create_usable_files.py  2014 2015

#update the backend cronjob log"
'''
mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
cursor=mysql_con.cursor()

insert_q='insert into backend_cronjoblog (description, dtstart ,dtend,count,app,success,mem ) values ( %s,%s,%s,%s,%s,%s,%s)'
desc_str=""" "topic recs cronjob" """
app_str=""" "lda topic" """
insert_q=insert_q%(desc_str, 'now()', '0', '0' , app_str, '0', 'NULL')
try:
    cursor.execute(insert_q)
    mysql_con.commit()
except:
    pass
'''    
os.chdir('/home/ubuntu/lda-data/Current_data/')
sample_dataset=pd.read_csv('Current_basic_dataset_with_IF_and_LDA.csv')
#This data has cols=pmid,150 tpc probs,pmid,DateCreated,ISSN,ISSNLinking 
sample_dataset['DateCreated']=pd.to_datetime(sample_dataset['DateCreated'])


#define the datefactor function
def datefunc1(x):
    now=datetime.now()
    delta=now.date()- ((x.DateCreated).date())
    if delta.days>90:
       datefctr=0
       return datefctr   
    if 90>= delta.days >60:
       datefctr=(.5)**(delta.days-60.0)  
       return datefctr
    elif 60.0 >= delta.days >=  15:
       datefctr=4.0-((1.5/45)*delta.days)  
       return datefctr
    else:
       datefctr=5
       return datefctr
       
sample_dataset['datefactor']=sample_dataset.apply(datefunc1,axis=1)

#define Impactfactor function
def impactfactorfctr(x):
    im=x.IF
    if im >=30.0:
        im_fctr=8.0
        return im_fctr
    if (30.0>im) & (im>=20.0):
        im_fctr=5.0
        return im_fctr
    if (20.0>im) &  (im>=10.0):
        im_fctr=3.0   
        return im_fctr    
    if (10.0>im) & (im>=4.0):  
        im_fctr=1.0   
        return im_fctr 
    else:     
        im_fctr=0.1        
        return im_fctr      

sample_dataset['impact_factor_fctr']=sample_dataset.apply(impactfactorfctr,axis=1)

sample_dataset['tpcRec_Score']=sample_dataset['tpcprob1']*8.0+sample_dataset['datefactor']+(sample_dataset['impact_factor_fctr'])*0.5
sample_dataset['tpcRec_Score'][sample_dataset.datefactor == 0]=0

#Making topic lists
for i in xrange(150):
    tmpdf = sample_dataset[sample_dataset.tpcno1 == i].sort('tpcRec_Score',ascending=False)  
    tmpdf=tmpdf.ix[:,['tpcno1','pmid','tpcRec_Score']]
    tmpdf=tmpdf.reset_index()
    tmpdf.drop(['index'],axis=1,inplace=True)
    tmpdf.to_csv('/home/ubuntu/lda-data/Current_data/TopicRecs/'+str(i)+"_sorted_"+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t',index_label='Index')
out=open('/home/ubuntu/lda-data/Current_data/TopicRecs/All_tpids_sorted'+datetime.now().strftime('%m_%d_%y')+'.txt','w')
linecnt=1 
for i in range(150):
    print i
    if i == 0:
         list=[0,41]
         tmp0=pd.read_csv('/home/ubuntu/lda-data/Current_data/TopicRecs/'+str(list[0])+'_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t')
         tmp1=pd.read_csv('/home/ubuntu/lda-data/Current_data/TopicRecs/'+str(list[1])+'_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t')
         new= pd.concat([tmp0,tmp1]).sort('tpcRec_Score',ascending=False).head(500)
         new.to_csv('/home/ubuntu/lda-data/Current_data/TopicRecs/'+str(i)+"_edit_sorted_"+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t',index=False)         
         tmp=open('/home/ubuntu/lda-data/Current_data/TopicRecs/'+str(i)+'_edit_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt').readlines()
         print "i = " + str(i)
         for line in tmp[1:501]:   
          #  out.write(str(linecnt)+'\n')            
            tt=line.split('\t')
           # print  str(linecnt)+"\tpubmed\t"+str(int(i)+1)+'\t'+tt[2]+'\t'+tt[3]
            out.write(str(linecnt)+'\t'+str(int(i)+1)+"\tpubmed\t"+tt[2]+'\t'+tt[3])
            linecnt=linecnt+1 
         print linecnt       
    elif i == 3:
         list=[3,9]
         tmp0=pd.read_csv('/home/ubuntu/lda-data/Current_data/TopicRecs/'+str(list[0])+'_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t')
         tmp1=pd.read_csv('/home/ubuntu/lda-data/Current_data/TopicRecs/'+str(list[1])+'_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t')
         new= pd.concat([tmp0,tmp1]).sort('tpcRec_Score',ascending=False).head(500)
         new.to_csv('/home/ubuntu/lda-data/Current_data/TopicRecs/'+str(i)+"_edit_sorted_"+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t',index=False)
         print "i = " + str(i)
         tmp=open('/home/ubuntu/lda-data/Current_data/TopicRecs/'+str(i)+'_edit_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt').readlines()
         for line in tmp[1:501]:   
          #  out.write(str(linecnt)+'\n')
            tt=line.split('\t')
           # print  str(linecnt)+"\tpubmed\t"+str(int(i)+1)+'\t'+tt[2]+'\t'+tt[3]
            out.write(str(linecnt)+'\t'+str(int(i)+1)+"\tpubmed\t"+tt[2]+'\t'+tt[3])
            linecnt=linecnt+1    
         print linecnt        
    elif i == 8:
         list=[8,146]
         tmp0=pd.read_csv('/home/ubuntu/lda-data/Current_data/TopicRecs/'+str(list[0])+'_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t')
         tmp1=pd.read_csv('/home/ubuntu/lda-data/Current_data/TopicRecs/'+str(list[1])+'_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t')
         new= pd.concat([tmp0,tmp1]).sort('tpcRec_Score',ascending=False).head(500)
         new.to_csv('/home/ubuntu/lda-data/Current_data/TopicRecs/'+str(i)+"_edit_sorted_"+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t',index=False)
         print "i = " + str(i)
         tmp=open('/home/ubuntu/lda-data/Current_data/TopicRecs/'+str(i)+'_edit_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt').readlines()
         for line in tmp[1:501]:              
            tt=line.split('\t')
          #  print  str(linecnt)+"\tpubmed\t"+str(int(i)+1)+'\t'+tt[2]+'\t'+tt[3]
            out.write(str(linecnt)+'\t'+str(int(i)+1)+"\tpubmed\t"+tt[2]+'\t'+tt[3])
            linecnt=linecnt+1    
         print linecnt      
    elif i == 134:
         list=[134,138]
         tmp0=pd.read_csv('/home/ubuntu/lda-data/Current_data/TopicRecs/'+str(list[0])+'_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t')
         tmp1=pd.read_csv('/home/ubuntu/lda-data/Current_data/TopicRecs/'+str(list[1])+'_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t')
         new= pd.concat([tmp0,tmp1]).sort('tpcRec_Score',ascending=False).head(500)
         new.to_csv('/home/ubuntu/lda-data/Current_data/TopicRecs/'+str(i)+"_edit_sorted_"+datetime.now().strftime('%m_%d_%y')+'.txt', sep='\t',index=False)
         print "i = " + str(i)
         tmp=open('/home/ubuntu/lda-data/Current_data/TopicRecs/'+str(i)+'_edit_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt').readlines()
         for line in tmp[1:501]:   
            tt=line.split('\t')
            out.write(str(linecnt)+'\t'+str(int(i)+1)+"\tpubmed\t"+tt[2]+'\t'+tt[3])
            linecnt=linecnt+1
         print linecnt          
    else:  
         print "getting into else loop"   
         tmp=open('/home/ubuntu/lda-data/Current_data/TopicRecs/'+str(i)+'_sorted_'+datetime.now().strftime('%m_%d_%y')+'.txt').readlines()
         print "i = " + str(i)
         for line in tmp[1:501]:  
            tt=line.split('\t')
          #  print  str(linecnt)+"\tpubmed\t"+str(int(i)+1)+'\t'+tt[2]+'\t'+tt[3]
            out.write(str(linecnt)+'\t'+str(int(i)+1)+"\tpubmed\t"+tt[2]+'\t'+tt[3])
            linecnt=linecnt+1
         print linecnt      
out.close()      
# Now I need to delete some of the topic ids:
data=pd.read_csv('/home/ubuntu/lda-data/Current_data/TopicRecs/All_tpids_sorted'+datetime.now().strftime('%m_%d_%y')+'.txt',sep='\t',header=None,names=['id','subcatid','dbname','pmid','prob'])     
#to test if data is ok:
# for i in range(1,151,1):
#             print shape(data[data.subcatid == i])

del_list=[10,11,14,21,29,42,48,49,56,60,65,73,103,104,112,113,121,124,127,139,147,149]
for i in del_list:
    data=data[data.subcatid != i]
data.drop_duplicates(cols='pmid',inplace=True)    
data.id=range(1,len(data)+1)
data.to_csv('/home/ubuntu/lda-data/Current_data/TopicRecs/All_tpids_sorted'+datetime.now().strftime('%m_%d_%y')+'.txt',sep='\t',index=False,header=False)
shutil.copy2(glob.glob('/home/ubuntu/Final_Tpc_Recs/new/*')[0],'/home/ubuntu/Final_Tpc_Recs/loaded/')
os.remove(glob.glob('/home/ubuntu/Final_Tpc_Recs/new/*')[0])
data.to_csv('/home/ubuntu/Final_Tpc_Recs/new/All_tpids_sorted'+datetime.now().strftime('%m_%d_%y')+'.txt',sep='\t',index=False,header=False)
subprocess.call(['/home/ubuntu/Scirec_scripts/Production_code/aws_Update_tpcs_cleanup.sh'])
#send email
aa=str(datetime.now().strftime('%m_%d_%y')+'.txt')
tpcs_there=glob.glob('/home/ubuntu/Final_Tpc_Recs/new/'+"*"+aa+"*")
if len(tpcs_there)>0:
    subprocess.call("python /home/ubuntu/Scirec_scripts/Production_code/send_Topics_pass.py",shell=True)
else:
    subprocess.call("python /home/ubuntu/Scirec_scripts/Production_code/send_Topics_fail.py",shell=True)
 

'''
update_q='update backend_cronjoblog SET dtend=%s,count=%s,success=%s where description=%s'  
numtops= len(pd.unique(data.subcatid))
if numtops==128:
    succ=1
else:
    succ=0    
update_q=update_q%('now()',str(numtops), str(succ), desc_str)
try:
    cursor.execute(update_q)
    mysql_con.commit()
except:
    pass
'''    