#! /user/bin/python

'''
cronjob to update recommendations for all libraries for all users daily. This is version3 and 
uses the incremental updates model- does not recalculate lda for all users as in the first version. 
'''
import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
import os
import glob
import shutil
import numpy as np
import itertools
from bibtexparser.bparser import BibTexParser
import subprocess
import cPickle as pickle
from boto.s3.connection import S3Connection
from boto.s3.key import Key
import boto
## additional imports from me. 
import argparse
import pubRecordsProduction  as pr # natalie's module
import Data_processing_scripts as cpr # priya's module
import logging
date = pr.todays_date()
logging.basicConfig(filename='/home/ubuntu/Logging_info/'+str(date)+'personal_recs.log',level=logging.DEBUG, filemode='w')
#make sure update_corpus_TermFreqVec() is run first!!
#make sure  update_pytables_for_libraries() is run next before running this script

'''
#update the backend cronjob log"
mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
cursor=mysql_con.cursor()

insert_q='insert into backend_cronjoblog (description, dtstart ,dtend,count,app,success,mem ) values ( %s,%s,%s,%s,%s,%s,%s)'
desc_str=""" "Personalized library recs cronjob" """
app_str=""" "lda recommendation" """
insert_q=insert_q%(desc_str, 'now()', '0', '0' , app_str, '0', 'NULL')
try:
    cursor.execute(insert_q)
    mysql_con.commit()
except:
    pass
'''

#then proceed
mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
q='select * from library_library_papers'
data=psql.frame_query(q,con=mysql_con)
lib_list=list(pd.unique(data.libid_id))
print lib_list
date = pr.todays_date()
out = '/home/ubuntu/Personalized_Recs/cronjob_testing/new/'+str(date)+"_personal_recs.txt"
o = open(out,'a')
corp_pubfile='/home/ubuntu/lda-data/Current_data/Current_basic_dataset_with_IF_and_LDA.csv'
out_ind_ct=1
for lib_id in lib_list:
    print "updating recs for library ", lib_id 
    lib_publist = list(data.dbid[data.libid_id == lib_id])
    #print lib_publist
    print len(lib_publist)
    u_id= str(list(data.userid_id[data.libid_id == lib_id])[0])
    date = pr.todays_date()
    libname=lib_id     
    dbname =str(list(data.dbname[data.libid_id == lib_id])[0]) 
    outpath='/home/ubuntu/usrlibrary-data/libid_'+str(lib_id)
    if not os.path.exists(outpath):
        os.makedirs(outpath)  
    os.chdir(outpath)
    if glob.glob('*.h5'):
        os.remove(glob.glob('*.h5')[0])   
    isthere = cpr.get_olddata_from_s3(lib_id)  #see if file is actually there
    if isthere == 0:  
        print "no file in s3"
        logging.debug(str(lib_id)+' doesnt have an h5 library yet.' )
        continue
    #read in the data and assign appropriate values
    ##reading in big corpus data   
    else:
        tmp_corp = pr.get_corp_info(corp_pubfile,delim=",")
        corp=[paper for paper in tmp_corp if paper.pmid not in lib_publist]
        #note now the users libraries are updated so just read them in and go    
        lib,lib_publist,error_flag =cpr.getting_users_paper_info_lda_pmid_uniq(lib_id,outpath)
        if error_flag>0:
            continue
        #lib,lib_publist =cpr.getting_usrpubRecs_andforLDA([int(i) for i in lib_publist],str(lib_id),outpath)    
        #now check how many papers are there in the users library- if too large only use most recent 1000
        if len(lib_publist)>500:
            uniq_data=pd.read_hdf("lib_"+str(lib_id)+".h5","/lib"+str(lib_id)+"_uniqAll")
            newuniq=uniq_data.sort([1],ascending=False)[0:500]            
            newind=list(newuniq.index)
            lib=[lib[i] for i in newind]
        corp = pr.shared_tpc_cutter_3(lib,corp,frac=0.5) # Fraction can be modified by adding a frac=n line; otherwise, default is 0
        if len(corp)>0:
            try:
                if len(lib_publist)>500:
                    simmat,updated_corp_list= cpr.create_updated_termFreqVec_and_calcSimilarity3(pr.pmid_list(corp),"lib_"+str(lib_id)+".h5",newind) 
                else:
                    simmat,updated_corp_list= cpr.create_updated_termFreqVec_and_calcSimilarity2(pr.pmid_list(corp),"lib_"+str(lib_id)+".h5") 
  
                #if any papers have been removed in the Vocab step because they did not have titles/etc make sure you update the corp:   
                new_corp=[paper for paper in corp if paper.pmid in updated_corp_list]
                print "word_sim done for lib ", lib_id
                corp_score_list = pr.word_sim_combined_score(simmat,lib,new_corp)
                corp_score_list = pr.metadata_weighter(new_corp,corp_score_list,use=[0,1,1],dbgvar=0)
                best_recs = pr.choose_best_from_corp(new_corp,corp_score_list)
                outputerror = 0  #This is a little error flag -- you'll try to write to output, and if it fails, you'll return an error flag in the end 
                for i, pub in enumerate(best_recs): # For every single pub
	                output = str(out_ind_ct)+"\t"+str(u_id)+"\t"+str(libname)+"\t"+dbname+"\t"+str(pub.pmid)+"\n"
	                out_ind_ct +=1
	                o.write(output)	              
            except:
	            continue        
o.close()
	
#send email

outpath='/home/ubuntu/Personalized_Recs/cronjob_testing/new/'
os.chdir(outpath)
personal_file=glob.glob('*personal*')
if len(personal_file)>0:
    personal_data=pd.read_csv(personal_file[0], sep='\t', header=None, dtype={1:str})
    num_personal_recs=len(pd.unique(personal_data[2])) -1  
    if num_personal_recs>0:
        subprocess.call("python /home/ubuntu/Scirec_scripts/Production_code/send_personal_passed.py  'number of Personal recs created='" + str(num_personal_recs) ,shell=True)
else:
    subprocess.call("python /home/ubuntu/Scirec_scripts/Production_code/send_personal_failed.py",shell=True)






'''
#write out to the backend log file
outpath='/home/ubuntu/Personalized_Recs/cronjob_testing/new/'
os.chdir(outpath)
personal_file=glob.glob('*personal*')[0]
personal_data=pd.read_csv(personal_file, sep='\t', header=None, dtype={1:str})
num_lib_recs=len(pd.unique(list(personal_data[2])))-1
update_q='update backend_cronjoblog SET dtend=%s,count=%s,success=%s where description=%s'  
if num_lib_recs>0:
    success=1
else:
    success=0 
update_q=update_q%('now()',str(num_lib_recs), str(success),desc_str)
try:
    cursor.execute(update_q)
    mysql_con.commit()        
except:
    pass
'''    