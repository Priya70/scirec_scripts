#!/usr/bin/env python
__all__ = ['pubRecordsProduction']

###########
## Imports
###########

# Minimal dependencies: MySQLdb, bibtexparser
import MySQLdb # used everywhere!!
from bibtexparser.bparser import BibTexParser # deprecated
import numpy as np # I love numpy, and it loads files so quickly when they're all-numerical!
import time # For today's date
from datetime import date,datetime,timedelta # for date upweighting
import sys # this is just necessary sometimes
import recMerge as rm # for making the merged recs
import subprocess as sp # useful for shell things which I need to do word counts and such
from scipy import stats
##################
##### Classes
##################

######## pubRecords:
### This class stores publication record information.
## Here are the pieces of info available - these are their exact key titles:
# [year,month,date]
# title
# pmid
# journal
# doi
# [author_a,author_b...author_n] ### AUTHORS CURRENTLY DISABLED. 
# abstract
# issn
# UPDATED: tpcs is a dict() with tpc -> p_tppc. 
##########

class pubRecords:
	## def __init__(self,t="",d=[0,0],p=0,j="",DOI=0,a=[],abst="",i=0,il=0,tp=set(0,0,0),p_tp=set(0,0,0)): ## INIT DEFINITION *WITH* AUTHORS
	def __init__(self,t="\"title not found\"",d=[0,0,0],p=0,abst="",i=0,il=0,tp=[],p_tp = [],imf=1,a=[]): # All of these sets are fake / impossible
		self.title = t
		self.date = d 
		#print self.date , "before"
		for i in range(len(self.date)):
			self.date[i] = int(self.date[i]) # make sure the date is an integer
		#print self.date , "after"
		self.pmid = int(p)
		#self.journal = j
		#self.doi = DOI 
		self.auths = a 
		self.abstract = abst 
		self.issn = i 
		self.issnl = il
		self.IF = imf 
		# Define the topic dictionary
		self.tpcs = dict()
		for i in range(len(tp)):
			if not str(tp[i]).isdigit(): # Do not allow instantiation of non-numeric topics
				print "Sorry -- topic ", tp[i], "was non-numeric. Please only use numeric topic values."
				exit()
			try:
				float(p_tp[i])
			except: # Do not allow instantiation of non-numeric topic probabilities
				print "Sorry -- topic probability ", p_tp[i], "was non-numeric. Please only use numeric topic values."
				exit()
			#print i
			self.tpcs[int(tp[i])] = float(p_tp[i])

	def pRprint(self): # Print method
		try:
			print self.title, "published on", "-".join(str(self.date))
		except: 
			print "Title or date missing"
		try:
			print self.pmid
		except:
			print "PMID missing"
		#print ",".join(self.auths)
		try:
			print (self.tpcs.keys())
		except:
			print "Topics missing"
		try:
			print (self.tpcs.values())
		except: 
			print "Topic probabilities missing"
		print "--"
		return
	# This function adds a new topic in an appropriate way.
        ## IF there are already 10 topics catalogued, don't include any more
        def addtpc(self,tpc,prob):
                if (len(self.tpcs.keys()) >= 10 and tpc not in self.tpcs.keys()): # If there are already 10 keys and this one is N$
                        return 0
                else: # Otherwise, feel free to add another key and topic!
                        self.tpcs[tpc] = prob
		return 

	def check(self): # Checks whether or not a record is actually completed correctly
		try: # try to do something with the title; does it actually exist
                        self.title+",".join(self.date)
                except: # if not, there's an error
                        return 1
                try: # does the PMID exist?
                        self.pmid * 1
                except: # if not, there's an error
                        return 1
                try: # do the topic keys exist?
                        sum(self.tpcs.keys())
                except:# if not, there's an error
                        return 1
                try: # do the values exist?
                        sum(self.tpcs.values())
                except:# if not, there's an error
			return 1
                # If no error has caused a return yet, return a 0
		return 0

################
### Functions
################

### all-purpose:
def dbg(dbgvar,str): # This is a debugger function - it prints the dbgvar message if dbgvar is set to 1.
	if (dbgvar == 1):
		print str
	return

def err(str): # This prints an error message and exits
	print str
	sys.exit()

###########################
######### Section 1:	  #
## Recommendation filters #
#########				  #
###########################

###################################
######## FILTER 1: BY TOP 3 TOPICS
###################################

## Defines a shared union from a list of paper objects
def shared_tpc_set(list,dbgvar):
	# Define a set of all of the topics in the paper list
	tops = set()
	for p in list:
		dbg(dbgvar,"Creating a shared topic list from "+str(p.pmid))
		tops = tops.union(set(p.tpcs.keys()))
	return tops

## Returns a 0 if the paper (second arg, P) should be cut
## and a 1 if it should remain in the list of similar papers
def shared_tpc(tops,P,dbgvar):
	#print "Comparing..." #TPL
	if len(set(P.tpcs.keys()).intersection(tops)) != 0:
		dbg(dbgvar,"Shared topic found "+str(P.pmid))
		return 1 # If the paper P has membership in a topic in the top list, return it
	dbg(dbgvar, "No shared topic found for "+str(P.pmid)) # dbgvar statement		
	return 0 # Otherwise, return 0 - you should cut this paper

## The actual cutter filter - given a library list and a corpus
## which papers should remain in the corpus? It returns the corp.

########## THE ACTUAL FUNCTION
def shared_tpc_cutter_1(lib,corp,dbgvar=0):
	tops = shared_tpc_set(lib,dbgvar) # Get the list of top topics
	newcorp = [] # Make a list of papers in the new and abridged corpus
	for paper in corp: # If a paper is in the corpus
		if shared_tpc(tops,paper,dbgvar): # and has a shared top topic
			dbg(dbgvar,"Adding a new paper to the corpus"+str(paper.pmid))
			newcorp.append(paper) # then add it to the corpus. 
		else:	 # Otherwise, well, don't!
			dbg(dbgvar,"Removing "+str(paper.pmid)+" from the corpus")
			next	
	return newcorp # Return the new corpus

###################################
######## FILTER 2: BY TOPICS FROM THE TOP 3, WHICH COVER N% OF THE PAPERS IN THE LIBRARY
###################################

## should we substitute this with a general distance function between topics?
def get_tpc_dict(lib,dbgvar): # Get a dictionary of topics to papers associated. 
	tops = shared_tpc_set(lib)
	# For each of the topics, identify which papers have that topic, then hash it... 
	tpcs_to_papers = dict()
	for paper in lib:
		if len(tops.intersection(set(paper.tpcs.keys()))) > 0:
			for tpc in tops.intersection(set(paper.tpcs.keys())):
				dbg(dbgvar,"Adding a topic to the topic-paper dictionary: "+str(tpc))
				try: # Try to add the paper to a set of papers which have that topic affiliation...
					tpcs_to_papers[tpc] = tpcs_to_papers[tpc].union(set([paper.pmid]))
				except:
					tpcs_to_papers[tpc] = set([paper.pmid])
	return tpcs_to_papers

def get_biggest_tpc(tpcs_to_papers,dbgvar): # Gets the largest topic...
	maxlen = 0
	bigtpc = 0
	for tpc in tpcs_to_papers.keys():
		if len(tpcs_to_papers[tpc]) > maxlen:
			maxlen = len(tpcs_to_papers[tpc])
			bigtpc = tpc
			dbg(dbgvar,"Current largest topic is:"+str(tpc))
	dbg(dbgvar,"Found the largest topic: it's "+str(bigtpc))
	return bigtpc 

def get_cover_set(lib,frac,dbgvar): # Create a set of topics which >frac% of the papers in the library have top membership in
	t_to_p = get_tpc_dict(lib)
	# Now that you have a hash, identify your covering set.... 
	tops = shared_tpc_set(lib)
	# Now, while the fraction of papers covered is smaller than frac, append covering topics ...
	papers_covered = set() # Here's a set of papers...
	cover = []
	while((len(papers_covered) / len(lib)) < frac):
		# Choose the topic with the largest list
		dbg(dbgvar,"Finding the largest topic...")
		tpc = get_biggest_tpc(t_to_p)
		cover.append(tpc)
		dbg(dbgvar,"Adding "+str(tpc)+" to the cover set...")
		papers_covered = papers_covered.union(t_to_p[tpc])
		# Now, remove all of those papers from all the other topics...
		for key in t_to_p:
			t_to_p[key] = t_to_p[key] - t_to_p[tpc]
	return set(cover)

########## THE ACTUAL FUNCTION
def shared_tpc_cutter_2(lib,corp,frac,dbgvar=0):
	# Get the covering set
	cover = get_cover_set(lib,frac,dbgvar)
	newcorp = []
	# Then filter by membership with topics in the cover set or not....
	for paper in corp:
		if shared_tpc(cover,paper,dbgvar) == 1:
			dbg(dbgvar,"Adding a new paper to the corpus")
			newcorp.append(paper)
	return newcorp


###################################
######## FILTER 3: BY TOPICS WHICH DESCRIBE >FRAC % OF AVG TOPIC MEMBERSHIP
## E.g., given topic probabilities (over 150 topics) for all of the papers,
## construct a single topic probability vector encompassing the whole lib.
## Then filter out anything which doesn't overlap with substantial components
## of that topic probability vector. 
###################################

###### THIS FUNCTION IS DEPRECATED AS THIS STRUCTURE IS NOT ROBUST TO 150 TOPICS.
# Get the overlapping topic vector from the whole library
def get_overlapping_topic_vec_DEPRECATED(lib,dbgvar):
	# I'm going to assume here that instead of just getting the top THREE topics, 
	# we have the WHOLE 150 vector. We do need to decide whether this is how we'll 
	# actually do things in the end. 
	vec =  [] # This vector will be the average topic vector for the library. 
	for paper in lib:
		# For every topic, try to add it to the vector list
		for i, key in enumerate(paper.tpcs.keys()):
			try: 
				vec[i] += paper.tpcs[key]
			except: # If that particular vector slot hasn't been incremented at all
				vec.append(paper.tpcs[key]) # then create it
	for i,freq in enumerate(vec): # Then, for each frequency, divide it by the library size
		vec[i] = freq / len(lib)
	return vec

# Get the overlapping topic vector from the whole library
def get_overlapping_topic_vec(lib,dbgvar):
        # I'm going to assume here that instead of just getting the top THREE topics, 
        # we have the WHOLE 150 vector. We do need to decide whether this is how we'll 
        # actually do things in the end. 
        freqs = dict() # This vector will be the average topic vector for the library. 
        for paper in lib:
                # For every topic, try to add it to the vector list
                for i, key in enumerate(paper.tpcs.keys()):
                        try:
                                freqs[key] += paper.tpcs[key]
                        except: # If that particular vector slot hasn't been incremented at all
                                freqs[key] = (paper.tpcs[key]) # then create it
	# Once you have a dictionary of all of the summed probabilities, stick it into a list to get the averaged vector
	avg_vec = [] # averaged vector
        for i in range(150): # For every possible topic, try to get the average assignment to that topic
                try: # Try to get an average
			avg_vec.append(freqs[i] / float(len(lib)))
			#print "tried"
		except: # Otherwise, assign zero
			avg_vec.append(0)
			#print "excepted"
        return avg_vec


# Gets the largest frequency in the overlapping topic vector
def get_biggest_freq(ind_vec,dbgvar): # Gets the largest frequency..
	maxlen = 0
	bigtpc = 0
	for tpc in ind_vec.keys():
		if ind_vec[tpc] > maxlen:
			maxlen = ind_vec[tpc]
			bigtpc = tpc
	return bigtpc 

# Identifies top-3 topics in the corpus shared with the list of topics, then see if there's overlap
def shared_top3_tpc(tops,P,dbgvar):
	#print "Comparing..." #TPL
	top3 = []
	# Copy the paper...
	newpub = P
	while (len(top3) < 3):
		bigtpc = get_biggest_freq(newpub.tpcs)
		newpub.tpcs[bigtpc] = 0 # Make sure you don't find that one again
		top3.append(bigtpc)
	print top3
	if len(set(top3).intersection(tops)) != 0:
		return 1 # If the paper P has membership in a topic in the top list, return it
	return 0 # Otherwise, return 0 - you should cut this paper

# Get the top topics from that overlapping topic vector
def get_cover_set_from_topic_vec(tvec,frac,dbgvar):
	# Make a dictionary of indices to values
	# Then return a cover set of the indices whose fractions sum up to frac....
	ind_vec = dict()
	for i in range(150):
		# Create the ind_vec dictionary
		ind_vec[i] = tvec[i]
	#print ind_vec.values()
	# Now, create the cover set
	cover = []
	coverfrac = 0
	while coverfrac < frac: # While less is covered....
		bigtpc = get_biggest_freq(ind_vec,dbgvar)
		#print bigtpc
		dbg(dbgvar,"Found a new big topic:"+str(bigtpc))
		cover.append(bigtpc)
		coverfrac += ind_vec[bigtpc]
		#print coverfrac
		ind_vec[bigtpc] = 0 # Make sure you don't just find the *same* one again....
	return cover

######### THE ACTUAL FUNCTION
def shared_tpc_cutter_3(lib,corp,frac=0.9,dbgvar=0):
	vec = get_overlapping_topic_vec(lib,dbgvar)
	#print vec
	cover = get_cover_set_from_topic_vec(vec,frac,dbgvar)
	#print cover
	newcorp = []
	for paper in corp:
		#if shared_top3_tpc(cover,paper): ### Complex computation -- calculates top topics
		if shared_tpc(cover,paper,dbgvar): ### Assumes only top topics are included in the tpc list
			newcorp.append(paper)
	return newcorp

###################################
######## FILTER 4: Overlap requirements
## This overlapping filter is identical to filter 3, 
## except that in order for it to consider topic overlap
## to have occured, it requires at least greater than
## some frac% probability annotated. 
###################################

######### This shared topic cutter requires overlap to be greater than a certan fraction, 
## overfrac. It doesn't assign overlap to a paper othwerise.
def shared_tpc_overfrac(tops,P,overfrac,dbgvar):
        #print "Comparing..." #TPL
	# Is there an intersection?
	intersect = set(P.tpcs.keys()).intersection(tops)
        if len(set(P.tpcs.keys()).intersection(tops)) != 0:
                dbg(dbgvar,"Shared topic found "+str(P.pmid))
		# Is the intersection big enough?
		for tpc in intersect: # Check each one
			if P.tpcs[tpc] >= overfrac: # If it's big enough, return the yes-flag
				return 1
        dbg(dbgvar, "No shared topic found for "+str(P.pmid)) # dbgvar statement                
        return 0 # Otherwise, return 0 - you should not include this paper


######### THE ACTUAL FUNCTION
def shared_tpc_cutter_4(lib,corp,libfrac=0.9,overfrac=0.1,dbgvar=0):
        vec = get_overlapping_topic_vec(lib,dbgvar)
        cover = get_cover_set_from_topic_vec(vec,libfrac,dbgvar)
        newcorp = []
        for paper in corp:
                #if shared_top3_tpc(cover,paper): ### Complex computation -- calculates top topics
                if shared_tpc_overfrac(cover,paper,overfrac,dbgvar): ### Assumes only top topics are included in the tpc list
                        newcorp.append(paper)
        return newcorp

###################################
########  FILTER 5: Coauthorship degree
## E.g., given a network of author-coauthorship, 
## construct a score for 'authorship' proximity
## between two papers. For simplicity, look only
## at the first and last n authors for coauths. 
###################################

### Creating a coauthorship matrix

# Given a particular author, identifies coauthors of the nth degree based on the n-1th degree coauthorship matrix
def find_degree(coauth_mat_1,auth_data,auth_index):
	# First identify which authors have n-1th degree coauthorship
	#coauth_mat_1[]
	# Then identify what primary coauthors those coauthors have from the 1' coauthorship matrix...
	return degrees

# Get the first degree of authorship
def get_first_degree(lib, corp, coauth_mat):
	# This will require looking at the actual database, which you'll do here...

	# Return the updated matrix
	return coauth_mat
# Get the second degree of authorship
def get_second_degree(lib, corp, coauth_mat): 
	coauth_mat_2 = np.zeros((len(lib)+len(corp),len(lib)+len(corp)))
	# Return the overlaid 2' matrix
	return coauth_mat_2

# Creating the coauthorship matrix
def get_coauth_mat(lib,corp):
	# By default, there's no coauthorship between anyone
	coauth_mat = np.zeros((len(lib)+len(corp),len(lib)+len(corp)))

	coauth_mat = get_degree(lib,corp,coauth_mat,deg=1)
	
	return coauth_mat
##########
## Word similarity score computation
##########

# Creates a score for a paper based on the day it was published.
def sim_datescore(paper,dbgvar):
	# What does priya use for her date function?
	score = 1 # Right now, no date score implemented.... 
	return score 

# Given a column in a word similarity matrix, return a similarity score for that index.
def get_combo_sim(lib,simlist,alpha,dbgvar): # Alpha is a constant to which everything is raised
	score = 0
	for i in range(len(simlist)):
		#s = datescore(lib[i],dbgvar) * (float(simlist[i]) ** (alpha) )
		s = sim_datescore(lib[i],dbgvar) * ((alpha) ** float(simlist[i]) )
		score += s
	score /= len(simlist)
	dbg(dbgvar,"The similarity score is "+str(score))
	return score 

# Given a word similarity matrix, for each column, return an overall word similarity score. 
def word_sim_combined_score(simmat,lib,corp,alpha=float(2),dbgvar=0): # I assume I'm given the corpus of papers too 
	scorelist = []
	#Calculate scores iteratively, by column
	for i in range(len(simmat[0,:])): 
	#	print "Iteration, length of each simmat side...", i, len(simmat[0,:]), len(simmat[:,0])
		dbg(dbgvar,"Calculating summed word similarity score")
		scorelist.append(get_combo_sim(lib,simmat[:,i],alpha,dbgvar))
	return scorelist


#########
## Re-weighting paper orderings based on date and impact factor
#########

# This function weights papers by impact factor given a list of scores for each of the 5 brackets
# of impact factor score possible. 
def if_weighter(paper,scores=[9.0,5.0,3.0,1.0,0.1],use=1):
	# If you don't want to use this function, then use will be set to 0 -- just go ahead and don't use it then
	if use == 0:
		return 1
    	# Given the paper's impact factor, identify what IF score it should get by placing it in a bracket.
	if paper.IF >=30.0: # bracket 1 corresponds to the 1st score
	       	return scores[0]
	if (30.0>paper.IF) & (paper.IF>=20.0): # bracket 2 = score 2
	        return scores[1]
	if (20.0>paper.IF) &  (paper.IF>=10.0): # and so on and so forth
		return scores[2]
	if (10.0>paper.IF) & (paper.IF>=4.0):  # until you reach the last bracket
		return scores[3]
	else: # after that, everything is assigned an embarassing score that's low :)
		return scores[4]
# This function weights papers by date, given a list of scores for each of the date brackets
# very similarly to the function above.
def date_weighter(paper,scores=[5.0,4.0,0.5,0],use=1):
	# If you don't want to use this function, then use will be set to 0 -- just go ahead and don't use it then
	if use == 0:
		return 1
	now=datetime.now() # Identify what today's date is
#   commenting out print statements
#	paper.pRprint() #TPL
	delta= now.date() - date(paper.date[0],paper.date[1],paper.date[2]) # Then identify the difference between today's date and the paper's date -- 
	if delta.days > 90.0:
		return scores[3] # This function returns a 0 for any paper which is too old
	if 90.0 >= delta.days > 60.0:
		return((scores[2])**(delta.days-60.0))
	elif 60.0 >= delta.days >=  15:
		return(scores[1]-((1.5/45)*delta.days))
	else:
	        return(scores[0])

# This function is a tweet_weighter() which returns a score, ala impact factor, 
# depending on how much a paper has been tweeted
def tweet_weighter(papers,weight_tweet,weight_rt,default=0.1,use=1):
	if use == 0: # If we aren't using this function just return straight away
		return [1 for i in range(len(papers))] # return a list of scores "1" for each tweet, to not affect weighting
	## Obtain the tweets
	# create a structure to link the tweets
	papers_tweets = dict() # I love dictionaries....
	papers_rts = dict() # I love dictionaries so much.# Set-up the query and make it to the server

	# Set up the query to get tweet information	
	papers_pmids = ",".join([str(i) for i in (pmid_list(papers))])
	q = "SELECT dbid, retweet_count FROM twitter_tweets WHERE dbid IN ("+papers_pmids+");"
	mysql_con = MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
	c=mysql_con.cursor()
	c.execute(q)

	# Obtain your queried information
	data = c.fetchall()
	data = np.asarray(data) # who doesn't love NParrays
	
	# Now, link the tweets
	for i in range(len(data)): # For every row
	        # Try to add another tweet to that particular paper's counts
	        try:
	                papers_tweets[int(data[i,0])] += 1
	        except:
	                papers_tweets[int(data[i,0])] = 1
	        # Try to add another retweet amount to that paper's retweet counts
	        try:
	                papers_rts[int(data[i,0])] += int(data[i,1])
	        except:
	                papers_rts[int(data[i,0])] = int(data[i,1])
	
	# Now, create a combined score for each paper based on how many tweets they have
	scores = []
	for p in papers: # For every paper, create a tweet score
	        score = default #(miniscule if there are no tweets associated with it)
	        try: # try to add in the tweets
	                score += papers_tweets[p.pmid] * weight_tweet
	        except: # if there are none, don't.
	                score += 0
	        try: # try to add in the retweets
	                score += papers_rts[p.pmid] * weight_rt
	        except: # if there are none, don't
	                score += 0
	        scores.append(score) # append the score
	# Then, return the scores
	return scores

### This function is a tweet_weighter which also normalizes the tweet scores afterwards and assigns each tweet its z-score, rather
## than its combined tweets+ retrweets score.
# This function is a tweet_weighter() which returns a score, ala impact factor, 
# depending on how much a paper has been tweeted
def tweet_weighter_normalized(papers,weight_tweet,weight_rt,default=0.1,use=1):
        if use == 0: # If we aren't using this function just return straight away
                return [1 for i in range(len(papers))] # return a list of scores "1" for each tweet, to not affect weighting
        ## Obtain the tweets
        # create a structure to link the tweets
        papers_tweets = dict() # I love dictionaries....
        papers_rts = dict() # I love dictionaries so much.# Set-up the query and make it to the server

        # Set up the query to get tweet information     
        papers_pmids = ",".join([str(i) for i in (pmid_list(papers))])
        q = "SELECT dbid, retweet_count FROM twitter_tweets WHERE dbid IN ("+papers_pmids+");"
        mysql_con = MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
        c=mysql_con.cursor()
        c.execute(q)

        # Obtain your queried information
        data = c.fetchall()
        data = np.asarray(data) # who doesn't love NParrays

        # Now, link the tweets
        for i in range(len(data)): # For every row
                # Try to add another tweet to that particular paper's counts
                try:
                        papers_tweets[int(data[i,0])] += 1
                except:
                        papers_tweets[int(data[i,0])] = 1
                # Try to add another retweet amount to that paper's retweet counts
                try:
                        papers_rts[int(data[i,0])] += int(data[i,1])
                except:
                        papers_rts[int(data[i,0])] = int(data[i,1])

        # Now, create a combined score for each paper based on how many tweets they have
        scores = np.zeros(len(papers))
        for i, p in enumerate(papers): # For every paper, create a tweet score
                scores[i] = default #(miniscule if there are no tweets associated with it)
                try: # try to add in the tweets
                        scores[i] += papers_tweets[p.pmid] * weight_tweet
                except: # if there are none, don't.
                        scores[i] += 0
                try: # try to add in the retweets
                        scores[i] += papers_rts[p.pmid] * weight_rt
                except: # if there are none, don't
                        scores[i] += 0
        # Then, return the scores, normalized
	return [i for i in stats.mstats.zscore(np.log10(scores))] # return the z-scores as a list, to ensure they're compatible with later steps for which numpy doesn't have equivalent functionality


## This function is a tweet_binner -- although it calculates the tweet score like its cousin above, what it does is processes
## the scores and assigns another score from a few bins specified in the command.
def tweet_binner(papers,weight_tweet,weight_rt,default=0.1,scores=[1.0,2.0,4.0,8.0],use=1):
        if use == 0: # If we aren't using this function just return straight away
                return [1 for i in range(len(papers))] # return a list of scores "1" for each tweet, to not affect weighting
        ## Obtain the tweets
        # create a structure to link the tweets
        papers_tweets = dict() # I love dictionaries....
        papers_rts = dict() # I love dictionaries so much.# Set-up the query and make it to the server

        # Set up the query to get tweet information     
        papers_pmids = ",".join([str(i) for i in (pmid_list(papers))])
        q = "SELECT dbid, retweet_count FROM twitter_tweets WHERE dbid IN ("+papers_pmids+");"
        mysql_con = MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
        c=mysql_con.cursor()
        c.execute(q)

        # Obtain your queried information
        data = c.fetchall()
        data = np.asarray(data) # who doesn't love NParrays

        # Now, link the tweets
        for i in range(len(data)): # For every row
                # Try to add another tweet to that particular paper's counts
                try:
                        papers_tweets[int(data[i,0])] += 1
                except:
                        papers_tweets[int(data[i,0])] = 1
                # Try to add another retweet amount to that paper's retweet counts
                try:
                        papers_rts[int(data[i,0])] += int(data[i,1])
                except:
                        papers_rts[int(data[i,0])] = int(data[i,1])

        # Now, create a combined score for each paper based on how many tweets they have
        tscores = []
        for p in papers: # For every paper, create a tweet score
                score = default #(miniscule if there are no tweets associated with it)
                try: # try to add in the tweets
                        score += papers_tweets[p.pmid] * weight_tweet
                except: # if there are none, don't.
                        score += 0
                try: # try to add in the retweets
                        score += papers_rts[p.pmid] * weight_rt
                except: # if there are none, don't
                        score += 0
                # Then, bin the score
		if score >= 0 and score < 10:
			tscores.append(scores[0])
		elif score >= 10 and score < 100:
			tscores.append(scores[1])
		elif score >= 100 and score < 1000:
			tscores.append(scores[2])
		else:
			tscores.append(scores[3])
        
        return tscores

## This function takes the weighters above and re-weights a list of scores with them. It takes a list of which filters to use (use) which correspond as follows:
## use[0] = usetweets or not? use[1] = use dates or not? use[2] = use IF or not?

def metadata_weighter(recs,scores,dbgvar=0,use = [1,1,1]):
	reweighted = [] # this will be the new list of scores
	#print use 
	# For every paper, reweight the scores by the various weighter functions
	# Calculate the tweet scores separately -- this speeds up the process, as there are many queries to make otherwise
	tweet_scores = tweet_weighter(recs,weight_tweet = 1, weight_rt = 0.5, use=use[0])
	for i,p in enumerate(recs): 
		dbg("Currently reweighting paper"+str(i),dbgvar) # debug statement 
		score = scores[i]
		if use[0] != 0: #If the use isn't 0, use tweets to upweight
			score = score * tweet_scores[i] * use[0] 
		if use[1] != 0: # If the use isn't 1, use the dates to upweight
			score = score * date_weighter(p,use=use[1]) * use[1]
		if use[2] != 0: # If we're using IFs at all, use the the IFs to upweight
			score = score * if_weighter(p,use=use[2]) * use[2]
		# Append the new score
		reweighted.append(score)
	# Return the new scores
	return reweighted 
##### This metadata weighter has tweet normalization. It's also under construction and not to be used in production
# code (yet); it replaces tweet scoring and binning with the tweet score being the z-score of the actual tweet score.
def metadata_weighter_tweet_normalizing(recs,scores,dbgvar=0,use = [1,1,1]):
        reweighted = [] # this will be the new list of scores
        #print use
        # For every paper, reweight the scores by the various weighter functions
        # Calculate the tweet scores separately -- this speeds up the process, as there are many queries to make otherwise
        tweet_scores = tweet_weighter_normalized(recs,weight_tweet = 1, weight_rt = 0.5, use=use[0])
        for i,p in enumerate(recs):
                dbg("Currently reweighting paper"+str(i),dbgvar) # debug statement 
                if use[0] != 0: #If the use isn't 0, use tweets to upweight
                        score = scores[i] * tweet_scores[i] * use[0]
                if use[1] != 0: # If the use isn't 1, use the dates to upweight
                        score = score * date_weighter(p,use=use[1]) * use[1]
                if use[2] != 0: # If we're using IFs at all, use the the IFs to upweight
                        score = score * if_weighter(p,use=use[2]) * use[2]
                # Append the new score
                reweighted.append(score)
        # Return the new scores
        return reweighted
##### This metadata weighter uses normalized tweets, but also uses tweets / IFs interchangeably. It chooses
# whichever score is higher and simply uses that one. 
def metadata_weighter_tweet_IF_interchange(recs,scores,dbgvar=0,use = [1,1,1]):
        reweighted = [] # this will be the new list of scores
        #print use
        # For every paper, reweight the scores by the various weighter functions
        # Calculate the tweet scores separately -- this speeds up the process, as there are many queries to make otherwise
        tweet_scores = tweet_weighter_normalized(recs,weight_tweet = 1, weight_rt = 0.5, use=use[0])
        for i,p in enumerate(recs):
                dbg("Currently reweighting paper"+str(i),dbgvar) # debug statement 
                if use[1] != 0: # If the use isn't 0, use the dates to upweight
                        score = score * date_weighter(p,use=use[1]) * use[1]
		# If you're using both tweets and IF, choose the highest, and upweight with that
		if use[2] != 0 and use[0] != 0: # If you're using both scores...
			if_score = if_weighter(p,use=use[2])
			max = max(tweet_scores[i],if_score)
			if max == tweet_scores[i]: # If the tweet score is biggest, weight by tweet score
				score = score * tweet_scores[i] * use[0]
			else: # Otherwise, weight by the IF score
				score = score * if_score * use[2]
		# Otherwise, use one or the other -- whichever one you said you were using... 
                if use[0] != 0 and use[2] == 0: #If the use isn't 0, use tweets to upweight
                        score = scores[i] * tweet_scores[i] * use[0]

		# Otherwise, use one or the other -- whichever one you said you were using... 
                if use[2] != 0 and use[0] == 0: # If we're using IFs at all, use the the IFs to upweight
                        score = score * if_weighter(p,use=use[2]) * use[2]
                # Append the new score
                reweighted.append(score)
        # Return the new scores
        return reweighted

##### This metadata with tweet binning is just an under-construction option; not to be used in production code (yet)
# it replaces tweet scoring with tweet score binning as per tweet_binner(). 
def metadata_weighter_tweet_binning(recs,scores,dbgvar=0,use = [1,1,1]):
        reweighted = [] # this will be the new list of scores
        #print use
        # For every paper, reweight the scores by the various weighter functions
        # Calculate the tweet scores separately -- this speeds up the process, as there are many queries to make otherwise
        tweet_scores = tweet_binner(recs,weight_tweet = 1, weight_rt = 0.5, scores=[1,2,4,8], use=use[0])
        for i,p in enumerate(recs):
                dbg("Currently reweighting paper"+str(i),dbgvar) # debug statement 
                if use[0] != 0: #If the use isn't 0, use tweets to upweight
                        score = scores[i] * tweet_scores[i] * use[0]
                if use[1] != 0: # If the use isn't 1, use the dates to upweight
                        score = score * date_weighter(p,use=use[1]) * use[1]
                if use[2] != 0: # If we're using IFs at all, use the the IFs to upweight
                        score = score * if_weighter(p,use=use[2]) * use[2]
                # Append the new score
                reweighted.append(score)
        # Return the new scores
        return reweighted

##########
## Creating a list of merged recommendations
##########
# this function helps create the user->libraries->papers dictionary for matching users with recommended papers
# organized by libraries belonging to that user.
# It starts with a dictionary of users to libraries, and populates those library subdictionaries with associated
# papers. 
def create_user_lib_dict(user_libsets,recfile):
	# Load in the recommendation data...
	data = [line.strip().split() for line in (open(recfile,'r').readlines())]
        # File format of the data being read in is:
        #recnum user    lib     db      dbID
        #1      NULL    3       pubmed  25164749
	#print data[0] # test print line
        # Associate recommended papers with libraries they're recommended for
        for i in range(len(data)): # For every line of the recommendation data...
                # Identify the correct user this library rec matches with
                this_user = 0
                while this_user == 0: # Until you find which user it is
                        for user in user_libsets.keys(): # For all of the users, 
                                if int(data[i][2]) in user_libsets[user].keys(): # if this library is in that user's library list...
                                        this_user = user # Go ahead and set that as the user
                try: # add the library and its corresponding paper to the dictionary of dictionaries
                        user_libsets[int(this_user)][int(data[i][2])].append(pubRecords(p=int(data[i][4])))
                except: # IF that doesn't work out, create it
                        try: # Try to create an addition to the dictionary of dictionaries             
				user_libsets[int(this_user)][int(data[i][2])] = [pubRecords(p=data[i][4])] # Each library's linked to a list of pubRecords of papers
                        except: # If that doesn't work you haven't even got your dictionary of dictionaries together! Get that together. 
                                user_libsets[int(this_user)] = dict()
                                user_libsets[int(this_user)][int(data[i][2])] = [pubRecords(p=int(data[i][4]))]
	return user_libsets # Return the dictionary

# This function creates a list of merged recommendations by using whichever merger is specified.
# The merging functions are defined in the separate package recMerge. 
def get_merged_recs(recfile,outfile,reclen=0,dbgvar=0):
	### Step 1: linking users and libraries together
	# First, identify which users are associated with which libraries via mysql query. 
        mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
        q = 'SELECT userid_id,id FROM library_libraries'
        c=mysql_con.cursor()
        c.execute(q)
        data = c.fetchall() # This returns a tuple of this format: userid,libid,dbmane,dbID
        c.close()
	
	# Given the data, create a dictionary with user IDs linked to dictionaries with the library IDs as keys
	# First, link users with associated libraries
	user_libsets = dict()
	dbg(dbgvar,"Getting user library sets")
	for i in range(len(data)): # For every line...
		dbg(dbgvar,"Trying to parse line "+str(i))
	        try: # Can you create a blank list to store the list of papers later?
	                user_libsets[int(data[i][0])][int(data[i][1])] = []
	                #print int(data[i][0]), int(data[i][1])
	        except: # If not, set up the dictionary in the first place.
	                user_libsets[int(data[i][0])] = dict()
	                user_libsets[int(data[i][0])][int(data[i][1])] = []
	                #print int(data[i][0]), int(data[i][1])
	
	# Now, connect individual users' libraries with the recommendations for those libraries	
	user_libsets = create_user_lib_dict(user_libsets,recfile)
	index = int(sp.check_output("wc -l "+str(recfile), shell = True).split()[0])
	##Step 2: Merging user-library groupings into single recommendation lists
	# Identify the index to start at for outputting
	for user in user_libsets.keys(): # For each user
		dbg(dbgvar,"Getting overall recs for "+str(user))
	        # Obtain the new recommendation list
	        #recs = rm.by_lib_size(user_libsets[user],reclen=reclen) -- this option is still under construction
	        recs = rm.basic(user_libsets[user],reclen=reclen)
	        #newrecs=[p for p in unique(recs)]
	        output_rec_by_lib(recs,libname=str(0),dbname="pubmed",user=str(int(user)),date=todays_date(),outf = outfile,index=index)
		# Update the index so you keep writing from the right place
		index += len(recs) 
	return #success! now go back to your main function

###########################
######### Section 2:	  #
## Assembling paper info  #
#########		  #
###########################

##########
## Create complete library info
##########

## These methods help to complete the information needed for a complete record
## This method returns a list of PMIDs for a list of pubRecords. 
def pmid_list(publist):
	pmidlist = []	
	for p in publist:
		pmidlist.append(p.pmid)
	return pmidlist

## This particular method determines whether info is missing from a record -- DEPRECATED
def needs_db_update(pubrec):
	#### DEPRECATED WARNING
	print "DEPRECATED"    #
	return 0              #
	#######################
	if pubrec.pmid == "nonefound":
		return 1 
	elif pubrec.title == "nonefound" or pubrec.abstract == "nonefound":
		return 1
	elif pubrec.issn == "nonefound" or pubrec.doi == "nonefound":
		return 1
	elif pubrec.date == ["nonefound","nonefound"]:
		return 1
	#elif "nonefound" in pubrec.auths:
	#	return 1
	#return 0 # Otherwise the record is as complete as the MySQL db can make it

### Along these lines, we also need to get the issn linking number...  -- DEPRECATED
def get_issn_linking_num(publist): # This function returns an updated publist

	############ DEPRECATED
	print "DEPRECATED"	  #
	return 0			  #
	#######################

	
	pmids = ",".join(pmid_list(publist))
	q = "SELECT ISSNLinking from pubmed_pubmed WHERE pubmed_pubmed.PMID IN("+pmids+");"
	mysql_con = MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
	data=psql.frame_query(q,con=mysql_con)
	for i in range(len(publist)):
		publist[i].issn = data[i]
	return publist 

## This particular method updates the info in a pubrec
def pubrec_db_update(pmid,data): # where data is one row of the MySQL join results
	### The data is in this order in the data array:
	# "t1.PMID","t1.DateCreated","t1.ISSN","t1.ISSNLinking","t1.ArticleTitle","t2.AbstractText"
	###### Updating...
	paper = pubRecords(p = pmid, d = str(data[1]).split("-")[0:2], i = data[2], t = data[4], abst = data[5])
	print data[1] #TPL
	print paper.date #TPL
	#paper.pRprint() #TPL
	return paper

## This particular method gets a table from the MySQL db, then creates a list of LIBRARY
## papers in the pubRecords format. 
def get_lib_db_info(pmidlist,dbgvar=0):
	# It doesn't make sense to waste time on MANY large joins... 
	# Rather, we should do one big one, then go paper by paper and fill in the info

	# Connect to the database
	fields = ",".join(["t1.PMID","t1.DateCreated","t1.ISSN","t1.ISSNLinking","t1.ArticleTitle","t2.AbstractText"]) # list of fields to update
	pmids = ",".join(pmidlist)
	# Create your query and evaluate it
	dbg(dbgvar, "Querying...")
	q= "SELECT "+fields+" FROM pubmed_pubmed t1, pubmed_abstract t2 WHERE t1.PMID IN ("+pmids+") AND t2.PMID_id IN ("+pmids+");"
	mysql_con = MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
	c=mysql_con.cursor()
	c.execute(q)
	# Obtain your queried information
	data = c.fetchall() 
	#mysql_con.commit() # remove the closing statements for now
	#mysql_con.close()
	#c.close()
	# For each publication, update it and insert it into the newpublist
	newpublist = []
	for i, pmid in enumerate(pmidlist):
		dbg(dbgvar,"Creating the "+str(i)+"th record")
		# format the info into 
		newpublist.append(pubrec_db_update(pmid,data[i]))
	return newpublist

# This method obtains topic information for library papers from LDA inferencer results.
def get_tpcs_lib(lda_results,lib): 
	lda = open(lda_results,'r')
	for i,line in enumerate(lda): # for every line
		# Update that paper with the topics from that line.
		parts = line.strip().split()
		if int(lib[i].pmid) != int(parts[0]): # Sanity check - make sure you're assigning topics to the correct paper
			err("Sorry, this isn't a correct paper match: "+str(lib[i].pmid)+"is not"+str(parts[0]))
		# Otherwise, update the topic dictionary...
		j = 1 # Begin indexing at 1, as parts[0] is the pmid
		while j < len(parts): # Until you get to the end...
			lib[i].addtpc(int(parts[j]),float(parts[j+1])) # Assign the 0 through 149th topic '''and make sure that its of type float'''
			# Move on to the next topic
			j += 2
	return lib

###########
# Create corpus info
###########

# Creates an individual pubrecord from a particular line of a 2d numpy array.
def construct_pubRec_from_line(data_list):
	#print data_list[7], "is the date we're converting"
	newpaper = pubRecords(p=data_list[0],tp = [data_list[1],data_list[3],data_list[5]],p_tp = [data_list[2],data_list[4],data_list[6]], d = data_list[7].split("-"),il=data_list[8],imf=data_list[9])
	return newpaper # Return the paper

# This method obtains topic information for corpus papers (more incomplete than library papers).
def get_corp_info(file,delim,dbgvar=0):
	# File name: Current_basic_dataset_with_IF_and_LDA.csv
	corpus = [] 
	f = open(file,'r') # open up the file, then create the corpus line by line.
	# Formatting: pmid,tpcno1,tpcprob1,tpcno2,tpcprob2,tpcno3,tpcprob3,DateCreated,ISSNLinking,IF
	f.readline() # Skip the header, of course
	for line in f:
		dbg(dbgvar,"Constructing corpus paper...")
		paper = construct_pubRec_from_line(line.strip().split(",")) # Construct the corp record from each file line
		corpus.append(paper) # add that paper to the corpus
		#print "Corpus paper date", paper.date
		#paper.pRprint()
		if (paper.check()):
			dbg(dbgvar,"An error was found!")
	return corpus

#########
## Creation of pubRecord from a bibtex -- deprecated for the most part...
#########

## Parses in a bibtex dictionary while creating a pubRecord. -- DEPRECATED
def get_pubrec(dicts):
	#### DEPRECATED WARNING
	print "DEPRECATED"    #
	return 0              #
	#######################

	try:
		title = dicts["title"]
	except:
		title = "nonefound"  # Except to the default value
	try:
		pmid = dicts["pmid"]
	except:
		pmid = "nonefound"  # Except to the default value
	try:
		year = dicts["year"]
	except:
		year = "nonefound"  # Except to the default value
	try:
		month = dicts["month"]
	except:
		month = "nonefound"  # Except to the default value
	try:
		journal = dicts["journal"]
	except:
		journal = "nonefound"  # Except to the default value
	#try:
	#	authors = dicts[key]["author"]
	#except:
	#	authors = "nonefound"  # Except to the default value
	try:
		doi = dicts["doi"]
	except:
		doi = "nonefound"  # Except to the default value
	try:
		issn= dicts["issn"]
	except:
		issn = "nonefound"  # Except to the default value
	try:
		abstract = dicts["abstract"]
	except:
		abstract = "nonefound"  # Except to the default value

	# Create correct formatting for several of them
	date = [year,month]
	#authors = authors.split(" and ")

	# Create and return the record object
	#pubrec = pubRecords(t=title,d=date,p=pmid,i=issn,j=journal,a=authors,DOI=doi,abst=abstract) ### WITH AUTHORS
	pubrec = pubRecords(t=title,d=date,p=pmid,i=issn,j=journal,DOI=doi,abst=abstract)
	return pubrec 
 
## Removes duplicate PMIDs from a publist
def remove_duplicate_pubs(publist):
	# Create a dictionary -- pmid to object
	# Return the values
	pubdict = dict()
	for pubrec in publist:
		pubdict[pubrec.pmid] = pubrec 
	return pubdict.values() # This returns the values of the now-PMID-unique hash

## Parses in a full bibtex file, to create a list of pubRecords. 
def get_publist(file): # Returns the list of pubRecord documents
	#### DEPRECATED WARNING
	print "DEPRECATED"    #
	return 0              #
	#######################
	# Parse the bibtex
	bibfile =  open(file, 'r') # Open the bibtex for reading
	bp = BibTexParser(bibfile.read()) # Use the parser to read it in
	bibfile.close() # Make sure and close your files for chrissake!
	# This creates a structure which has dictionaries corresponding to specific bibtex entry types.
	dicts = bp.get_entry_dict() # Get dictionaries which represent each bibtex article entry
	publist = []
	for key in dicts.keys(): #For every single paper in the dicts(keys), read it in as a pubRecord
		pubdict = dicts[key]
		pubrec = get_pubrec(pubdict)
	return remove_duplicate_pubs(publist)

###########
# Output functions
###########

# A simple function to get today's date...
def todays_date():
	return "_".join([time.strftime("%m"),time.strftime("%d"),time.strftime("%y")])

# Choose (n) of the best papers to output
def choose_best_from_corp(corp,scores,reclen=500,dbgvar=0): # Assumes the score list and corp are ordered the same way
	# Create a dictionary with scores for papers -> all papers which have that score
	scores_papers = dict()
	for i in range(len(corp)):
		try: 
			scores_papers[scores[i]] += ","+str(i)
		except:
			scores_papers[scores[i]] = str(i)
	# Now, sort the scores, and choose the papers until you reach a pre-canned length
	#print scores
	scores.sort() # sort from least to greatest score
	scores.reverse() # reverse -- now you have greatest to least! yay!

	papers = [] # This is the list of papers you'll recommend
	for s in scores:
		good_papers = scores_papers[s].split(",") # Get the list of good papers
		for p in good_papers:
			papers.append(corp[int(p)])
		# Stop when you've recommended enough papers
		if len(papers) >= reclen:
			return(papers[0:reclen])
        ###priya -adding code -need to return something if reclen<500	
        else:
            return(papers)		

# Outputs recommendations by library. The index to start at is a default place to start counting from -- if you output different libraries, you want to make sure indexing is consistent throughout the file
def output_rec_by_lib(publist,libname="15",dbname="pubmed",user="NULL",date="01_14_1993",outf="thisisntarealfile",index=0): #i like my birthday as a default :D also, unreasonable values for defaults will make sure we clue in to errors immediately
	out = outf # Set the outfile to what the user specifies -- unless they specify nothing
	if out == "thisisnotarealfile":
		out = date+"_recs.txt"
	o = open(out,'a')
	outputerror = 0 # This is a little error flag -- you'll try to write to output, and if it fails, you'll return an error flag in the end
	for i, pub in enumerate(publist): # For every single pub
		i += index
		#print i
		output = str(i+1)+"\t"+user+"\t"+str(libname)+"\t"+dbname+"\t"+str(pub.pmid)+"\n"
		try:
			o.write(output)
		except:
			outputerror = 1
	return outputerror

# Outputs personal recommendations (e.g., recommendations based on combined knowledge of the libraries in there)
def output_personal_combo_rec(publist,username,date):
	outputerror = 0
	return outputerror

###########
# Miscellaneous useful functions...
###########

# A quick function which writes out things that we definitely don't want to use long term .... 
#it's just necessary for us to do this right now because we have a lot of things writing out to files. 
def print_pub_text(pub,file_name,dbgvar=0): # prints out all text data
	f = open(file_name,'a') # Open the file for appending
	# Output the text for that particular paper to this file
	try:
		f.write(str(pub.pmid)+"\t"+str(pub.title)+"\t"+str(pub.abstract)+"\n")
		dbg(dbgvar,"Wrote one line to the file "+str(file_name))
		f.close()
		return 1
	except:
		# If the write doesn't work for some reason, then return a 0 -- this way you can alarm upon any problems...
		dbg(dbgvar,"Unable to write a line to file "+str(file_name))
		f.close()
		return 0 

######## For validation purposes -- chooses best papers and the best scores. 
# Choose (n) of the best papers to output
def modified_choose_best_from_corp(corp,scores,reclen=500,dbgvar=0): # Assumes the score list and corp are ordered the same way
        # Create a dictionary with scores for papers -> all papers which have that score
        scores_papers = dict()
	print len(corp), len(scores)
        for i in range(min(len(corp),len(scores))):
                try:
                        scores_papers[scores[i]] += ","+str(i)
                except:
                        scores_papers[scores[i]] = str(i)
        # Now, sort the scores, and choose the papers until you reach a pre-canned length
        #print scores
        scores.sort() # sort from least to greatest score
        scores.reverse() # reverse -- now you have greatest to least! yay!

        papers = [] # This is the list of papers you'll recommend
        for s in scores:
                good_papers = scores_papers[s].split(",") # Get the list of good papers
                for p in good_papers:
                        papers.append(corp[int(p)])
                # Stop when you've recommended enough papers
                if len(papers) >= reclen:
                        return(papers[0:reclen],scores[0:reclen])
############ Outputs recommendations by library along with the score for that recommendation
def modified_output_rec_by_lib(publist,scorelist,libname="15",dbname="pubmed",user="NULL",date="01_14_1993"): # i like my birthday as a default :D also, unreasonable values for defaults will make sure we clue in to errors immediately
        out = str(date)+"_recs.txt"
        o = open(out,'a')
        outputerror = 0 # This is a little error flag -- you'll try to write to output, and if it fails, you'll return an error flag in the end
        for i, pub in enumerate(publist): # For every single pub
                output = str(i+1)+"\t"+user+"\t"+str(libname)+"\t"+dbname+"\t"+str(pub.pmid)+"\t"+str(scorelist[i])+"\n"
                try:
                        o.write(output)
                except:
                        outputerror = 1
        return outputerror

############# Also gets author info for the corpus.
def modified_get_corp_info(file,delim,dbgvar=0):
        # File name: Current_basic_dataset_with_IF_and_LDA.csv
        corpus = []
        f = open(file,'r') # open up the file, then create the corpus line by line.
        # Formatting: pmid,tpcno1,tpcprob1,tpcno2,tpcprob2,tpcno3,tpcprob3,DateCreated,ISSNLinking,IF
        f.readline() # Skip the header, of course
        for line in f:
                dbg(dbgvar,"Constructing corpus paper...")
                paper = construct_pubRec_from_line(line.strip().split(",")) # Construct the corp record from each file line
                corpus.append(paper) # add that paper to the corpus
                #paper.pRprint()
                if (paper.check()):
                        dbg(dbgvar,"An error was found!")
	# Now, add the authors in for the corpus
	# Create a query to grab the authors
	fields = ",".join(["PMID_id","ForeName","LastName","Initials"])
        pmids = ",".join([str(i) for i in pmid_list(corpus)])
        # Create your query and evaluate it
        dbg(dbgvar, "Querying...")
        q= "SELECT "+fields+" FROM pubmed_author WHERE PMID_id IN ("+pmids+");"
        mysql_con = MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
        c=mysql_con.cursor()
        c.execute(q)
        # Obtain your queried information
        data = c.fetchall()
        #mysql_con.commit()
	#mysql_con.close()
	# For every paper in the corpus, assign the information. 
	for i in range(len(data)):
		for p in corpus: # For every paper
			# If the PMID matches, add the author
			if p.pmid == data[i][0]:
				p.auths.append("".join(data[i][1:3]))
	return corpus
