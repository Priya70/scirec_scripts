import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
import os
import glob
import Data_processing_scripts as cpr

mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
q='select * from library_library_papers'
data=psql.frame_query(q,con=mysql_con)
lib_list=list(pd.unique(data.libid_id))
print lib_list
lib_with_no_pylab=[]
lib_with_no_papers=[]
for lib_id in lib_list:
    lib_publist = list(data.dbid[data.libid_id == lib_id])
    if len(lib_publist)==0:
        lib_with_no_papers.append(lib_id)
    outpath='/home/ubuntu/usrlibrary-data/libid_'+str(lib_id)
    if not os.path.exists(outpath):
        os.makedirs(outpath)  
    os.chdir(outpath)
    if glob.glob('*.h5'):
        os.remove(glob.glob('*.h5')[0])   
    isthere = cpr.get_olddata_from_s3(lib_id)  #see if file is actually there
    if isthere == 0:  
        lib_with_no_pylab.append(lib_id)      

if len(lib_with_no_pylab)>0:
    qq='select * from library_libraries where id in (%s)'
    str_list=','.join(['%s']*len(lib_with_no_pylab))
    q=qq % str_list
    lib_data=psql.frame_query(q,params = lib_with_no_pylab,con=mysql_con)
    for lib in lib_with_no_pylab:
        cpr.create_pytable_for_single_library(lib)
else:
    pass
        
    
