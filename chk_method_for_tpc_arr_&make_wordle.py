#!/usr/bin/local/python
# usage: create_tpc_vector  'wrd_tpcall_20140221093827_150_9603.txt ' 150 
# usage : create_tpc_vector  infile  num_tpcs 
#  infile, n_tpcs='wrd_tpcall_20140221093827_150_9603.txt' ,'150' 

import sys
import pandas as pd
import numpy as np
from datetime import datetime


def create_tp_vector(infile,n_tpcs):    
    n_tpcs=int(n_tpcs)
    data=open(infile,'r').readlines()
    tpcs=[[0]*len(data) for x in xrange(n_tpcs)]  
    word_vocab=[]
    ct=0
    for line in data:
        word_vocab.append(line.split()[1])   
        aa=line.split()[2:]
        for tt in aa:
            tpcs[int(tt.split(':')[0])][ct]=int(tt.split(':')[1])            
        ct=ct+1   
    tpcs.insert(0,word_vocab)      
    tpcs_df=pd.DataFrame(tpcs)           
    tpcs_df=tpcs_df.T
    return tpcs_df
    
tpcs_df=create_tp_vector('/data/priya/Topic_modelling/mallet-2.0.7/for_database/make_inferencer_20140221093827/wrd_tpcall_20140221093827_150_9603.txt' ,'150')
        df=pd.DataFrame([['NA','NA','NA']],columns=['topicID','word','wrd_prob'])
    for i in range(150):    
        print "Topic # ",i    
        test=pd.DataFrame(tpcs_df,columns=[0,i+1])
        test.columns=['word','wrd_prob']
        test['wrd_prob']=test['wrd_prob']/test.wrd_prob.sum()
        tmp=test.sort('wrd_prob',ascending=False).head(50)
        tmp['topicID']=np.zeros(50)+i
        df=pd.concat([df,tmp])
        #need to filter out tuberculosis/mycobacterium from tpc 54. Do this manually
        # on visual inspection these correspond to indices:
#          11205      54   tuberculosis   0.04168841
#          11127      54             tb     0.038503
#          11204      54  mycobacterium   0.01449453
#          11605      54            bcg  0.005149851
#          5062       54  mycobacterial   0.003622626
#          11610      54   mycobacteria   0.00321362
    df.drop([11205,11127,11204,11605,5062,11610],inplace=True)           
    #    tmp.index=list(xrange(50)+i*50+i)
    df.index =list(xrange(len(df.wrd_prob)))
    df=df[df.word != 'NA']  #dont use df.index == 0, as there are multiple tpcs where 0th wrd has high prob.
    tmp=df.topicID+1
    df.topicID=tmp
    del_list=[10,11,14,21,29,42,48,49,56,60,65,73,103,104,112,113,121,124,127,139,147,149]
    for i in del_list:
         df=df[df.topicID != i]
    df.drop(['index'],axis=1,inplace=True)  
    aa=range(len(df.topicID)+1)[1:]   
    df.index=aa

    test= pd.DataFrame(index=df.index, columns=['topicID','word','wrd_prob','col5','col6','col7','col8','col9','col10'])
    test=test.fillna('NULL')
    test.topicID=df.topicID
    test.word=df.word
    test.wrd_prob=df.wrd_prob
    test.topicID=test['topicID'].apply(int) 
    test.to_csv('/data/priya/Topic_modelling/mallet-2.0.7/for_database/Tpc_names+wordles/For_Wordle'+datetime.now().strftime('%m_%d_%y')+'.txt',sep='\t',header=False)
    test.to_csv('/data/priya/For_Yonggan/Tpc_lists+Wordles/For_Wordle'+datetime.now().strftime('%m_%d_%y')+'.txt',sep='\t',header=False)
  
        
   
   del_list=[10,11,14,21,29,42,48,49,56,60,65,73,103,104,112,113,121,124,127,139,147,149]
for i in del_list:
    data=data[data.subcatid != i]
data.drop(['id'],axis=1,inplace=True)   
aa=range(len(data.subcatid)+1)[1:]   
data.id=aa
