#! /usr/local/bin/python
#This script run LDA for a filelist of files that have been prepared for LDA .- so dont need to do everything again 

#usage:python /home/ubuntu/Scirec_scripts/given_filelist_run_LDA.py  (no arguments) but MUST be run in the correct directory 

import os  
import sys 
import Data_processing_scripts_WORKING as dp
#reload(dp) 
import subprocess
import glob
import shutil


def main(args):
    filelist=glob.glob('ForLDA*.csv')
    print filelist
    for file in filelist:     
        if len(open(file).readlines())>5:   
#lda_runscript_path='/data/priya/Topic_modelling/mallet-2.0.7/for_database/Calculating_Rec_Scores/JP_test_set/'
            print "running LDA for ", file 
            subprocess.call(['/home/ubuntu/Scirec_scripts/lda_scripts/run_LDA.sh',file])
            shutil.copyfile('output_file_tpcs_composition_for_database.txt',file.split('csv')[0]+'LDA_composition_for_database.txt')
            shutil.copyfile('output_file_tpcs_composition.txt',file.split('csv')[0]+'LDA_composition_with_pmid.txt')      
            os.remove('output_file_tpcs_composition.txt')
            os.remove('output_file_tpcs_composition_for_database.txt')  
         
        else:
            print "file is short. It has length ", len(open(file).readlines()) 
    subprocess.call(['/home/ubuntu/Scirec_scripts/aws_cleanup.sh'])

    


if __name__ == "__main__":
    main(sys.argv)     