#! /usr/local/bin/python
# usage python getArticleDataFromXML.py filename.xml
# This takes in an XML file, queries the PMID of each, and writes the data to some output

import sys
import urllib
import re
import xml.etree.cElementTree as ET
import codecs
sys.stdout = codecs.getwriter('utf8')(sys.stdout)

def getArticleDataFromFile(f):
    pmid = None
    count = None
    helperStructure = None
    year = None
    for event, elem in ET.iterparse(f, events=("start","end"):
        if event == 'start':
            if elem.tag == 'PubmedArticle':
                pmid, count, year = None, None, None
        elif event == 'end':
            if elem.tag == 'PubmedArticle':
                yield pmid
            elif elem.tag =='article-id pub-id-type="pmid"' or elem.tag == 'pub-id pub-id-type="pmid"':
                helperStructure = createHelperStructure(elem.text)
                for p,y,c,f in helperStructure:
                    pmid = p
                    year = y
                    count = c
                    #write this data to output file
                    pmid, year, count = None, None, None #reset the file
    f.close()

def createHelperStructure(pmid_input):
    pmid = pmid_input
    year = None
    count = None
    #open in new file
    filehandle = urllib.urlretrieve("http://www.ebi.ac.uk/europepmc/webservices/rest/search/query="+pmid)[0]
    f = open(filehandle, 'r')
    for event,elem in ET.iterparse(f, events="start","end")):
        if event == 'start':
            if elem.tag == 'PubmedArticle': #not sure if this is correct usage?
                pmid, year, count = None, None, None
        elif event == 'end':
            if elem.tag == 'PubmedArticle'
                yield count,pmid,year
            if elem.tag == 'citedByCount':
                count = elem.text
            if elem.tag == 'pmid':
                pmid = elem.text
            if elem.tag == 'pubYear':
                year = elem.text
    f.close()

def main(args):
    if(len(args) != 2) return;
    filename = sys.argv[1] #xml filename is the second argument
    article = getArticleDataFromFile(filename)

