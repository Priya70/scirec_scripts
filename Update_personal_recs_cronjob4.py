#!/user/bin/python

###### NATALIE'S NOTE
###### THIS CRONJOB HAS BEEN MODIFIED TO ALSO INCLUDE THE LIKED PAPERS.
###### IT IS CURRENTLY VERY MUCH EXPERIMENTAL AND SHOULD NOT BE CONSIDERED PRODUCTION CODE!
###### I SIMPLY WANTED TO GENERATE SOME RECS FROM IT TO SHOW JONATHAN TOMORROW.

'''
cronjob to update all libraries for all users daily.
'''
import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
import os
import glob
import shutil
import numpy as np
import itertools
from bibtexparser.bparser import BibTexParser


import subprocess
import cPickle as pickle
import immediateRecs as ir
## additional imports from me. 
import argparse
import pubRecordsProduction as pr # natalie's module
import Data_processing_scripts as cpr # priya's module
import random

###############################
#### Argument parsing
###############################
### Filenames
# 1. Filename with topic information
# 2. Old rec file (default -- yesterday's date)
# 3. New rec file (default -- today's date)
# 4. 

### 
#
#
#
#

#first move the old rec file to loaded directory.
if glob.glob('/home/ubuntu/Personalized_Recs/cronjob_testing/new/*'):
    shutil.copy2(glob.glob('/home/ubuntu/Personalized_Recs/cronjob_testing/new/*')[0],'/home/ubuntu/Personalized_Recs/cronjob_testing/loaded/')
    os.remove(glob.glob('/home/ubuntu/Personalized_Recs/cronjob_testing/new/*')[0])


### Know which index you're outputting at...
out_ind_ct=0

####### Quick beginning step: create the twitter recommendations. 
### This variable SHOULD BE assigned via command line args, but I can't restructure all htis code right now, so I won't. 
topicfile = "/home/ubuntu/lda-data/Current_data/Current_basic_dataset_with_IF_and_LDA.csv"
# Grab the existing papers from twitter
##### 1. Query for the existing papers.
q = "SELECT dbid,count FROM twitter_tweetspapers;"
mysql_con = MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
c=mysql_con.cursor()
c.execute(q)
# Obtain your queried information
tweetpaps = c.fetchall()
tweetpaps = np.asarray(tweetpaps) # who doesn't love NParrays

# Sort it by score into a hashmap
gen_tweets_list = [] # remember, you'll need to output a general tweets list
tweet_scores = dict() # make a map of the scores to papers....
for i in range(len(tweetpaps[:,0])):
	# Append to the general list...
	gen_tweets_list.append(pr.pubRecords(p=int(tweetpaps[i,0])))
        try: # Try to make the map, but if you fail, just 
                tweet_scores[tweetpaps[i,1]].append(pr.pubRecords(p=int(tweetpaps[i,0])))
        except: # start the list
                tweet_scores[tweetpaps[i,1]] = [pr.pubRecords(p=int(tweetpaps[i,0]))]

# Then get all of the topics of interest
q = "SELECT userid_id,subcatid_id FROM feeds_mytopics;" # Select all of the topics
mysql_con = MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
c=mysql_con.cursor()
c.execute(q)
# Obtain your queried information
topicdata = c.fetchall()
topicdata = np.asarray(topicdata) # who doesn't love NParrays

##### 2. Find the top tweeted papers for these topics.
# For each user, identify which topics are relevant, and get papers for those.
user_topics = dict() # This dictionary will hold user topics and papers with those topics
for i in range(len(topicdata[:,0])):
        # Try to add these papers into a dictionary that already exists
        try:
                user_topics[topicdata[i,0]].values() # is there a dictionary here?
                # If so, go ahead and try to update it. 
                try:
                        user_topics[topicdata[i,0]][topicdata[i,1]].append(ir.find_papers_with_topic(topicfile,[topicdata[i,1]-1]))
                # If that doesn't work out, then start from scratch:
                except:
                        user_topics[topicdata[i,0]][topicdata[i,1]] = (ir.find_papers_with_topic(topicfile,[topicdata[i,1]-1]))
        # If there isn't a dictionary in the first place, create it and add
        except:
                user_topics[topicdata[i,0]] = dict()
                user_topics[topicdata[i,0]][topicdata[i,1]] = (ir.find_papers_with_topic(topicfile,[topicdata[i,1]-1]))

# Now, for each paper, calculate the top-tweeted list of all the papers, and mix them in at the top. 
for n,user in enumerate(user_topics.keys()):
        # Get the top tweeted papers from those topics
        # prepare a list of paper objects...
        papers = []
        for lib in user_topics[user].values():
                for p in lib:
                        papers.append(p)
        # Re-weight the papers, but only with the tweet scores (use = 1,0,0)
        scores =  pr.metadata_weighter(papers,[1 for i in range(len(papers))],dbgvar=0,use = [1,0,0])
        # Choose the first papers
        recs = pr.choose_best_from_corp(papers,scores,reclen=500)
        # Mix them in preferentially at the top
        best_tweeted = random.sample(recs[0:20],10) # get 10 of the best 20 for the top
        less_tweeted = random.sample(recs[20:200],90) # grab 90 of the next best 180 for the top
        # Choose the top papers to mix in...
        scores_list = tweet_scores.keys()
        scores_list.sort()
        scores_list.reverse()

        # Now, choose from the best thereof to populate the rest...
        best_corp = []
        less_corp = []

        # Make sure and choose nicely matched sets.
        index = 0
        while len(best_corp) < 10:
                for p in tweet_scores[scores_list[index]]:
                        best_corp.append(p)
                index += 1
        index = 0
        # Now add the less tweeted papers in there.
        while len(less_corp) < 90:
                for p in tweet_scores[scores_list[index]]:
                        less_corp.append(p)
        # Now create two shuffled lists.
        best_list = best_corp + best_tweeted
        less_list = less_corp + less_tweeted
        random.shuffle(best_list)
        random.shuffle(less_list)
        final_list = best_list + less_list
        # Make sure your paper sets are unique, and create your final set...
        tweet_mix = []
        final_set = set(final_list)

        # For every paper, add it to the tweet mix if it's unique; if not, choose another paper to put in its stead...
        for p in final_list:
                attempt_counter = 0 # Never get stuck in a loop - maybe you're all tried out
                while p in tweet_mix and attempt_counter <= 10:
                        p = random.sample(recs,1)
                        attempt_counter += 1
                tweet_mix.append(p)
        # Write it out into the rec file. 
        pr.output_rec_by_lib(final_list,libname="-2",dbname="pubmed",user=str(int(user)),outf="/home/ubuntu/Personalized_Recs/cronjob_testing/new/"+str(pr.todays_date())+"_recs.txt",index=out_ind_ct)
	# Update the output counter
	print out_ind_ct
	out_ind_ct += len(final_list)
# Remember to output a general list....
pr.output_rec_by_lib(gen_tweets_list,libname="-2",dbname="pubmed",user=str("NULL"),outf="/home/ubuntu/Personalized_Recs/cronjob_testing/new/"+str(pr.todays_date())+"_recs.txt",index=len(final_list)*n)

#### Make recommendations with the usual process for users' liked libraries.
'''
mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')

'''
q='SELECT * FROM metrics_likes'
# Store it as a pandas data frame
data=psql.frame_query(q,con=mysql_con)
# Here, the list of libraries is ACTUALLY a list of users. 
lib_list=list(pd.unique(data.userid_id))
print lib_list

date = pr.todays_date()
out = '/home/ubuntu/Personalized_Recs/cronjob_testing/new/'+str(date)+"_recs.txt"
#out = '/home/ubuntu/Personalized_Recs/cronjob_testing/new/'+str(date)+"_recs.txt"
o = open(out,'a')

# Given the libraries organized, use each one in successive recommendations. 
corp_pubfile='/home/ubuntu/lda-data/Current_data/Current_basic_dataset_with_IF_and_LDA.csv'
for lib_id in lib_list:
    print "updating recs for library ", lib_id
    lib_publist = list(data.dbid[data.userid_id == lib_id])
    print lib_publist
    print len(lib_publist)
    u_id= str(list(data.userid_id[data.userid_id == lib_id])[0])
    date = pr.todays_date()
    libname=lib_id
    dbname =str(list(data.dbname[data.userid_id == lib_id])[0])
    outpath='/home/ubuntu/Personalized_Recs/cronjob_testing/Vocab_testing/'+str(lib_id)
    if not os.path.exists(outpath):
        os.makedirs(outpath)
    outpath='/home/ubuntu/Personalized_Recs/cronjob_testing/Vocab_testing/'+str(lib_id)
    lib,lib_publist =cpr.getting_usrpubRecs_andforLDA([int(i) for i in lib_publist],str(lib_id),outpath)
    tmp_corp = pr.get_corp_info(corp_pubfile,delim=",")
    corp=[paper for paper in tmp_corp if paper.pmid not in lib_publist]
    os.chdir(outpath)
    cpr.run_lda_user(outpath+"/ForLDA_"+str(lib_id)+".txt",str(lib_id)) # Run topic inferencer
    print "done lda for ", lib_id
    inference_results = outpath+'/'+str(lib_id)+"LDA_composition_with_pmid.txt" # Identify the file name with the topic assignments
    lib = pr.get_tpcs_lib(inference_results,lib)
    corp = pr.shared_tpc_cutter_3(lib,corp,frac=0.5) # Fraction can be modified by adding a frac=n line; otherwise, default is 0
    simmat,updated_corp_list= cpr.create_updated_termFreqVec_AND_calcSimilarity(pr.pmid_list(corp),outpath+"/ForLDA_"+str(lib_id)+".txt")
 #   simmat = cpr.vocab_list_Similarity_calculator(pr.pmid_list(corp),outpath+"/ForLDA_"+str(lib_id)+".txt") 
 #if any papers have been removed in the Vocab step because they did not have titles/etc make sure you update the corp:   
    new_corp=[paper for paper in corp if paper.pmid in updated_corp_list]
    print "word_sim done for lib ", lib_id
    corp_score_list = pr.word_sim_combined_score(simmat,lib,new_corp)
    corp_score_list = pr.metadata_weighter(new_corp,corp_score_list,use=[.1,1,1],dbgvar=0)
    best_recs = pr.choose_best_from_corp(new_corp,corp_score_list)
    outputerror = 0  #This is a little error flag -- you'll try to write to output, and if it fails, you'll return an error flag in the end 
    for i, pub in enumerate(best_recs): # For every single pub 
           output = str(out_ind_ct)+"\t"+str(libname)+"\t"+str(-1)+"\t"+dbname+"\t"+str(pub.pmid)+"\n"
           out_ind_ct +=1
           #print output
           o.write(output)

### First, identify recommendations for each individual user library.
# Get all of the papers for all of the libraries
mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
q='select * from library_library_papers'
data=psql.frame_query(q,con=mysql_con)
# Identify how many libraries there are
lib_list=list(pd.unique(data.libid_id))
print lib_list
date = pr.todays_date()
out = '/home/ubuntu/Personalized_Recs/cronjob_testing/new/'+str(date)+"_recs.txt"
#out = '/home/ubuntu/Personalized_Recs/cronjob_testing/new/'+str(date)+"_recs.txt"
o = open(out,'a')

# Given all that data, use the corpus to get recs for every successive library. 
corp_pubfile='/home/ubuntu/lda-data/Current_data/Current_basic_dataset_with_IF_and_LDA.csv'
for lib_id in lib_list:
    print "updating recs for library ", lib_id 
    lib_publist = list(data.dbid[data.libid_id == lib_id])
    print lib_publist
    print len(lib_publist)
    u_id= str(list(data.userid_id[data.libid_id == lib_id])[0])
    date = pr.todays_date()
    libname=lib_id     
    dbname =str(list(data.dbname[data.libid_id == lib_id])[0]) 
    outpath='/home/ubuntu/Personalized_Recs/cronjob_testing/Vocab_testing/'+str(lib_id)
    if not os.path.exists(outpath):
        os.makedirs(outpath)    
    outpath='/home/ubuntu/Personalized_Recs/cronjob_testing/Vocab_testing/'+str(lib_id)
    lib,lib_publist =cpr.getting_usrpubRecs_andforLDA([int(i) for i in lib_publist],str(lib_id),outpath)    
    tmp_corp = pr.get_corp_info(corp_pubfile,delim=",")
    corp=[paper for paper in tmp_corp if paper.pmid not in lib_publist]
    os.chdir(outpath)
    cpr.run_lda_user(outpath+"/ForLDA_"+str(lib_id)+".txt",str(lib_id)) # Run topic inferencer
    print "done lda for ", lib_id
    inference_results = outpath+'/'+str(lib_id)+"LDA_composition_with_pmid.txt" # Identify the file name with the topic assignments
    lib = pr.get_tpcs_lib(inference_results,lib)
    corp = pr.shared_tpc_cutter_3(lib,corp,frac=0.5) # Fraction can be modified by adding a frac=n line; otherwise, default is 0
    #corp = pr.shared_tpc_cutter_4(lib,corp,libfrac=0.9,overfrac=0.1) # Fraction can be modified by adding a frac=n line; otherwise, default is 0
    simmat,updated_corp_list= cpr.create_updated_termFreqVec_AND_calcSimilarity(pr.pmid_list(corp),outpath+"/ForLDA_"+str(lib_id)+".txt") 
 #   simmat = cpr.vocab_list_Similarity_calculator(pr.pmid_list(corp),outpath+"/ForLDA_"+str(lib_id)+".txt") 
 #if any papers have been removed in the Vocab step because they did not have titles/etc make sure you update the corp:   
    new_corp=[paper for paper in corp if paper.pmid in updated_corp_list]
    print "word_sim done for lib ", lib_id
    corp_score_list = pr.word_sim_combined_score(simmat,lib,new_corp)
    corp_score_list = pr.metadata_weighter(new_corp,corp_score_list,use=[.1,1,1],dbgvar=0)
    best_recs = pr.choose_best_from_corp(new_corp,corp_score_list)
    outputerror = 0  #This is a little error flag -- you'll try to write to output, and if it fails, you'll return an error flag in the end 
    for i, pub in enumerate(best_recs): # For every single pub
	    output = str(out_ind_ct)+"\t"+str(u_id)+"\t"+str(libname)+"\t"+dbname+"\t"+str(pub.pmid)+"\n"
	    out_ind_ct +=1
	    #print output
	    o.write(output)
o.close()
pr.get_merged_recs(out,out,reclen=500)
subprocess.call(['/home/ubuntu/Scirec_scripts/aws_update_personalrecs_cronjob_cleanup.sh'])

