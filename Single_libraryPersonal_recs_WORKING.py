#! /user/bin/python

'''
script to create a new library's recommendations
'''
import pandas as pd
import pandas.io.sql as psql
import MySQLdb
import numpy as np
import os
import glob
import shutil
import numpy as np
import itertools
import subprocess
import cPickle as pickle
from boto.s3.connection import S3Connection
from boto.s3.key import Key
import boto
## additional imports from me. 
import argparse
import pubRecordsProduction_WORKING  as pr # natalie's module
import Data_processing_scripts_WORKING as cpr # priya's module
date = pr.todays_date()
import sys
import subprocess

#get the unique library id
lib_id=int(sys.argv[1])
pathname= '/home/ubuntu/scireader/tmp/'
if not os.path.exists(pathname):
    os.makedirs(pathname)  
os.chdir(pathname)
#clean up the directory:
for file in glob.glob('*'):
    os.remove(file)
#create the pytable     
cpr.create_pytable_for_single_library_standalone(lib_id,pathname)
#then proceed

mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='s83B7J3JQhSNKJ', db='scireader')
q='select * from library_library_papers'
data=psql.frame_query(q,con=mysql_con)
date = pr.todays_date()
out = pathname+str(lib_id)+"_personal_recs.txt"
#out = '/home/ubuntu/scireader/tmp/'str(lib_id)+"_personal_recs.txt"
o = open(out,'a')
#need to download current file from s3
subprocess.call("aws s3 cp s3://lda-data/Current_data/Current_basic_dataset_with_IF_and_LDA.csv    Current_basic_dataset.csv ",shell=True)    
corp_pubfile='Current_basic_dataset.csv'
out_ind_ct=1
lib_publist = list(data.dbid[data.libid_id == lib_id])
#print lib_publist
print len(lib_publist)
u_id= str(list(data.userid_id[data.libid_id == lib_id])[0])
date = pr.todays_date()
libname=lib_id     
dbname =str(list(data.dbname[data.libid_id == lib_id])[0]) 

##reading in big corpus data   
tmp_corp = pr.get_corp_info(corp_pubfile,delim=",")
corp=[paper for paper in tmp_corp if paper.pmid not in lib_publist]
#note now the users libraries are updated so just read them in and go    
lib,lib_publist,error_flag =cpr.getting_users_paper_info_lda_pmid_uniq(lib_id,pathname)
if error_flag>0:
    print "error- check library input??"
else:
    corp = pr.shared_tpc_cutter_3(lib,corp,frac=0.5) # Fraction can be modified by adding a frac=n line; otherwise, default is 0
    if len(corp)>0:
        simmat,updated_corp_list= cpr.create_updated_termFreqVec_and_calcSimilarity_single_library_standalone(pr.pmid_list(corp),"lib_"+str(lib_id)+".h5") 
        #if any papers have been removed in the Vocab step because they did not have titles/etc make sure you update the corp:   
        new_corp=[paper for paper in corp if paper.pmid in updated_corp_list]
        print "word_sim done for lib ", lib_id
        corp_score_list = pr.word_sim_combined_score(simmat,lib,new_corp)
        corp_score_list = pr.metadata_weighter(new_corp,corp_score_list,use=[0,1,1],dbgvar=0)
        best_recs = pr.choose_best_from_corp(new_corp,corp_score_list)
        outputerror = 0  #This is a little error flag -- you'll try to write to output, and if it fails, you'll return an error flag in the end 
        for i, pub in enumerate(best_recs): # For every single pub
	        # output = str(out_ind_ct)+"\t"+"NULL"+"\t"+str(libname)+"\t"+dbname+"\t"+str(pub.pmid)+"\n"
	        output = str(out_ind_ct)+"\t"+str(u_id)+"\t"+str(libname)+"\t"+dbname+"\t"+str(pub.pmid)+"\n"
	        out_ind_ct +=1
	        print str(pub.pmid)+"\n"
	        o.write(output)	              
o.close()

#subprocess.call("cut  -f5 *personal*")
#subprocess.call("rm -rif *)
 