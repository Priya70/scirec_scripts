#! /usr/local/bin/python
#This script will call functions in Data_processing_scripts.py.  So import that first. Also this script needs to be run from 
# python script_to_getMysqldata_and_create_usable_files.py
#usage:python /home/ubuntu/Scirec_scripts/script_to_getMysqldata_and_create_usable_files.py  1990 1994

import os  
import sys 
import Data_processing_scripts as dp
#reload(dp) 
import subprocess
import glob
import shutil

def main(args):
    beginyear=args[1] #year for which we want to get the mysql data in monthly format
    endyear=args[2]
    for year in xrange(int(beginyear),int(endyear)): 
        startyear=year   
        print "running script for", startyear
        newdir='/home/ubuntu/lda-data/'+str(startyear)
        os.mkdir(newdir)
        os.chdir(newdir)
        print os.getcwd()
        dp.get_MysqlData(startyear) #This should create the tmp files in Scireader_Data/tmp which need to be sorted so all the abstracts for each pmid are together
        filelist=os.listdir('./')
        for file in filelist:
            print file  
            dp.MysqlData_to_OneRecPerLineFmt(file,newdir) #resorts the file into format with one record per line as well as make the txt file for LDA 
        os.chdir(newdir)    
        filelist=glob.glob('ForLDA*.csv')
        print filelist
        for file in filelist:     
            if len(open(file).readlines())>5:   
                print "running LDA for ", file 
                subprocess.call(['/home/ubuntu/Scirec_scripts/lda_scripts/run_LDA.sh',file])
                shutil.copyfile('output_file_tpcs_composition_for_database.txt',file.split('csv')[0]+'LDA_composition_for_database.txt')
                shutil.copyfile('output_file_tpcs_composition_with_pmid.txt',file.split('csv')[0]+'LDA_composition_with_pmid.txt')                
                os.remove('output_file_tpcs_composition_for_database.txt')  
                os.remove('output_file_tpcs_composition.txt')
                os.remove('output_file_tpcs_composition_with_pmid.txt')
            else:
                print "file is short. It has length ", len(open(file).readlines()) 
    subprocess.call(['/home/ubuntu/Scirec_scripts/aws_cleanup.sh'])
            
if __name__ == "__main__":
    main(sys.argv)     