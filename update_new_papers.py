#! /usr/local/bin/python
#This script will call functions in Data_processing_scripts.py.  So import that first. 
#usage:python /home/ubuntu/Scirec_scripts/update_new_papers.py  
import os  
import sys 
import Data_processing_scripts as dp
#reload(dp) 
import subprocess
import glob
import shutil
from datetime import datetime,timedelta 
import MySQLdb
import pandas as pd
import pandas.io.sql as psql


def main():
    '''
    This function should do the following:
    - update the papers in lda-data with papers downloaded into the Scireader database since the last download/yesterday? 
    - convert into files for LDA processing and Unique data
    Put it in a cron job  Never shut this instance down?         
    '''    
    do = True
    while do == True:						# put everything in an endless loop
	  now = datetime.now()
	  if now.hour == 1 and now.minute == 50:	# if it is 6:00, run the jobs
        curr_mon=datetime.now().month 
        curr_year=datetime.now().year
        path='/home/ubuntu/lda-data/'+str(curr_year)
        print path
        os.chdir(path)
        fd=str(curr_year)+'-'+str(curr_mon)+"-1"
        ld=str(curr_year)+'-'+str(curr_mon)+"-31"
        mysql_con= MySQLdb.connect(host='scireader.ckyekccfx4xz.us-west-1.rds.amazonaws.com', user='scireader', passwd='&s83B7J3JQhSNKJ', db='scireader')
        q='select p.pmid, p.DateCreated,p.ArticleTitle, p.JournalTitle,p.ISSN,p.ISSNLinking ,a.AbstractText, a.AbstractOrder from pubmed_pubmed as p inner join  pubmed_abstract as a on p.pmid = a.pmid where (p.DateCreated between %s and %s)'        
        data=psql.frame_query(q,params=(fd,ld),con=mysql_con)
        nrow,ncol=data.shape
        if nrow>5:  #dont bother saving if its a Month in the future or a month without much data        
            tt=(datetime.strptime(fd,"%Y-%m-%d")).strftime("%b")
            data.to_csv(tt+'_'+str(curr_year)+'_data.csv',index=False)
            # re-sort the file into format with one record per line as well as make the txt file for LDA    
        dp.MysqlData_to_OneRecPerLineFmt(tt+'_'+str(curr_year)+'_data.csv',path)
        filelist=glob.glob('ForLDA_'+tt+'*data.csv')
        print "filename is ", filelist 
        for file in filelist:
            if len(open(file).readlines())>5:   
                print "running LDA for ", file 
                subprocess.call(['/home/ubuntu/Scirec_scripts/lda_scripts/run_LDA.sh',file])
                shutil.copyfile('output_file_tpcs_composition_for_database.txt',file.split('csv')[0]+'LDA_composition_for_database.txt')
                shutil.copyfile('output_file_tpcs_composition.txt',file.split('csv')[0]+'LDA_composition_with_pmid.txt')      
                os.remove('output_file_tpcs_composition_for_database.txt')  
                os.remove('output_file_tpcs_composition.txt')
                os.remove('output_file_tpcs_composition_with_pmid.txt')
                
            else:
                print "file is short. It has length ", len(open(file).readlines()) 
                
   # subprocess.call(['/home/ubuntu/Scirec_scripts/aws_cleanup.sh'])
                
if __name__ == "__main__":
    main()                     