#!/bin/bash
#shell script to upload all new data to s3 and stop the instance
cd /home/ubuntu/
export EC2_URL=http://ec2.us-west-1.amazonaws.com
aws s3  sync lda-data/ s3://lda-data  delete
aws s3  sync Scirec_scripts/  s3://scirec-scripts/  --delete
aws s3  sync Final_Tpc_Recs/new/  s3://scireader-data/topics/new/ --delete

aws s3  sync Final_Tpc_Recs/loaded/ s3://scireader-data/topics/loaded/

